<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    /**
     * Nom de la table associée au model
     * @var string
     */
	protected $table = 'feedbacks';

    /**
     * Proporiétés autorisées pour l'envoie de données de masse
     * @var array
     */
    protected $fillable = ['entreprise', 'descrption_entreprise','contenu', 'created_by','recrutement_feedback','isOuvert','valide'];

    public function etudiant(){
       return $this->belongsTo('App\Etudiant','created_by');
    }

}
