<?php

namespace App\Repositories;


interface ResponsableRepository
{
    /** cherche les responsables par le id de filiere
     * @param $id
     * @return mixed
     */
    public function findByFiliereId($id);

    /**
     * get la liste de responsables avec le filiere
     * @return mixed
     */
    public function allWithFiliere();

    /**
     * recherche le responsable par ID
     * @param $id
     * @return mixed
     */
    public function findById($id);

}