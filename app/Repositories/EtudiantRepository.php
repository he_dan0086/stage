<?php namespace App\Repositories;


interface EtudiantRepository {


    /**
     * get tous les etudiants avec la filiere
     * @return mixed
     */
    public function allWithFiliereCV();

    /**
     * cherche etudaint par id
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * recherche les etudiant par le id de filiere
     * @param $filiere_id
     * @return mixed
     */
    public function listEtudiantIdByFiliereId($filiere_id);

    /**
     * retour la liste des etudiants
     *
     * @return mixed
     */
    public function all();


}
