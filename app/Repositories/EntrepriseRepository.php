<?php

namespace App\Repositories;


interface EntrepriseRepository
{

    /**
     * get la liste des entreprise
     * @return mixed
     */
    public function all();

}