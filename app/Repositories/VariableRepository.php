<?php

namespace App\Repositories;


interface VariableRepository
{
    /**
     * recherche toutes les variables qui appartienne à l'utilisateur current
     *
     * @return mixed
     */
    public function findByUserCurrent();

}