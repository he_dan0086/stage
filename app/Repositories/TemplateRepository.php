<?php

namespace App\Repositories;


interface TemplateRepository
{

    /**
     * retour la liste de templates;
     * @return mixed
     */
    public function allDownloads();
    /**
     * recheche le template par le langage
     * @param $lang
     * @return mixed
     */
    public function findDownloadByLang($lang);


    /**
     * recherche la template par la filiere
     * @param $filiere
     * @return mixed
     */
    public function findTemplateByFiliere($filiere);

}