<?php

namespace App\Repositories;


interface MailRepository
{


    /**
     * envoyer un email pour confirmer la publcaition de l'offre de stage
     * @param $expediteur
     * @param $expediteur_mail
     * @param $destination
     * @param $destination_mail
     * @param $file
     * @return mixed
     */
    public function envoyerMail($expediteur,$expediteur_mail,$destination,$destination_mail,$file,$route,$variable,$subject);


}