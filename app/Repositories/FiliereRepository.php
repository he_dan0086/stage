<?php

namespace App\Repositories;


interface FiliereRepository
{

    /**
     * retour tous les récord de filieres
     * @return mixed
     */
    public function all();

    /**
     * recherche la filiere par nom
     * @param $nom
     * @return mixed
     */
    public function findByNom($nom);


    /**
     * recherche la filiere par id
     * @param $id
     * @return mixed
     */
    public function findById($id);

}