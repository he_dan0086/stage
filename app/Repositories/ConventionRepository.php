<?php
namespace App\Repositories;


interface ConventionRepository
{
    /**
     * retour tous les conventions dans la DB
     * @return mixed
     */
    public function all();


    /**
     * cherche une convention par id
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function findById($id);

    /**
     * chercher les conventions par la filiere
     * @param $filiere
     * @return mixed
     */
    public function findByFiliere($filiere);

}