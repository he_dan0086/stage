<?php

namespace App\Repositories;


interface UserRepository
{
    /**
     * cherche le utilisateur par le type et id
     * @param $type
     * @return mixed
     */
    public function findByUserTypeEtId($type, $id);

    /**
     * retour tous les users avec responsables et ses filieres
     * @return mixed
     */
    public function findUserWithResponsable();

    /**
     * retour l'utilisateur authentifié
     * @return mixed
     */
    public function getUserCurrect();

    /**
     * retour l'utilisateur de role
     * @return mixed
     */
    public function getRoleCurrent();

}