<?php

namespace App\Repositories\Impl;


use App\Entreprise;
use App\Repositories\EntrepriseRepository;

class EntrepriseRepositoryImpl implements EntrepriseRepository
{

    /**
     * get la liste des entreprise
     * @return mixed
     */
    public function all()
    {
        return Entreprise::with('user')->get();
    }
}