<?php

namespace App\Repositories\Impl;


use App\Filiere;
use App\Repositories\Filiererepository;

class FiliereRepositoryImpl implements Filiererepository
{

    /**
     * retour tous les récord de filieres
     * @return mixed
     */
    public function all()
    {
        return Filiere::all();
    }

    /**
     * recherche la filiere par nom
     * @param $nom
     * @return mixed
     */
    public function findByNom($nom)
    {
        return Filiere::whereFiliereNom($nom)->first();
    }

    /**
     * recherche la filiere par id
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Filiere::whereId($id)->first();
    }
}