<?php
namespace App\Repositories\Impl;


use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserRepositoryImpl implements UserRepository
{


    /**
     * cherche le utilisateur par le type
     * @param $type
     * @return mixed
     */
    public function findByUserTypeEtId($type, $id)
    {
        return User::whereUserType($type)->whereUserId($id)->first();
    }

    /**
     * retour tous les users avec responsables et ses filieres
     * @return mixed
     */
    public function findUserWithResponsable()
    {
//        $users=User::whereUserType('App\Responsable')
//            ->with('user')
//            ->get();
//        foreach($users as $user)
//            $user->user->with('filiere')->get();
//        dd($users);

    }

    /**
     * retour l'utilisateur authentifié
     * @return mixed
     */
    public function getUserCurrect()
    {
        return Auth::user();
    }

    /**
     * retour l'utilisateur de role
     * @return mixed
     */
    public function getRoleCurrent()
    {
        return Auth::user()->user;
    }
}