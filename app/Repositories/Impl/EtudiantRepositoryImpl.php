<?php namespace App\Repositories\Impl;



use App\Etudiant;
use App\Repositories\EtudiantRepository;

class EtudiantRepositoryImpl implements EtudiantRepository {


    /**
     * get tous les etudiants avec la filiere
     * @return mixed
     */
    public function allWithFiliereCV()
    {
        return Etudiant::whereNotNull('cv')->with('filiere')->get();
    }

    /**
     * cherche etudaint par id
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Etudiant::whereId($id)->with('filiere')->first();
    }


    /**
     * recherche les etudiant par le id de filiere
     * @param $filiere_id
     * @return mixed
     */
    public function listEtudiantIdByFiliereId($filiere_id)
    {
        return Etudiant::whereFiliereId($filiere_id)->lists('id');
    }

    /**
     * retour la liste des etudiants
     *
     * @return mixed
     */
    public function all()
    {
        return Etudiant::with('filiere')->with('user')->get();
    }
}