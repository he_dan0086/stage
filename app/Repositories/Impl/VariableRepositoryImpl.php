<?php

namespace App\Repositories\Impl;


use App\Repositories\VariableRepository;
use App\Variable;
use Illuminate\Support\Facades\Auth;

class VariableRepositoryImpl implements VariableRepository
{

    /**
     * recherche toutes les variables qui appartienne à l'utilisateur current
     *
     * @return mixed
     */
    public function findByUserCurrent()
    {
        $variables=Variable::all();
        foreach ($variables as $v)
        {
            $name=$v->key;
            $n=explode('+',$name);
            if(trim($n[0]) == Auth::user()->email)
                $array[$n[1]]=$v->value;
        }
        if(isset($array)){
            return $array;
        }
        return null;
    }
}