<?php
namespace App\Repositories\Impl;


use App\Repositories\MailRepository;
use Illuminate\Support\Facades\Mail;

class MailRepositoryImpl implements MailRepository
{

    /**
     * envoyer un email pour confirmer la publcaition de l'offre de stage
     * @param $expediteur
     * @param $expediteur_mail
     * @param $destination
     * @param $destination_mail
     * @param $file
     * @return mixed
     */
    public function envoyerMail($expediteur,$expediteur_mail,$destination,$destination_mail,$files,$route,$variable,$subject){

    Mail::send(
        $route,
        $variable,
        function ($message)
        use ($expediteur,$expediteur_mail,$destination,$destination_mail,$files,$subject)
        {
            $message->from($expediteur_mail, $expediteur)
                ->to($destination_mail)
                ->subject($subject);
            $size = sizeOf($files);
            for($i=0;$i<$size;$i++){
                $message->attach($files[$i]);
            }
        },true);
    }
}