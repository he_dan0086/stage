<?php namespace App\Repositories\Impl;


use App\Etudiant;
use App\OffreStage;
use App\Repositories\OffreStageRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class OffreStageRepositoryImpl implements OffreStageRepository{


    /**
     * Retourne toutes les offres de stages
     *
     * @return mixed
     */
    public function all()
    {
        return OffreStage::orderBy('valide')
            ->orderBy('updated_at', 'desc')
            ->with('competences')
            ->with('niveaux')
            ->with('departements')
            ->get();
    }

    /**
     * cherche l'offre de stage par id
     * @param $id
     * @return mixed
     */
    public function findbyId($id)
    {
        return OffreStage::whereId($id)->first();
    }


    /**
     * cherche l'offre de satge par le createur
     * @param $createor
     * @return mixed
     */
    public function findByCreator($createor)
    {
        return OffreStage::whereCreatedBy($createor)->get();
    }

    /**
     * cherche l'offre de stage par le nom de l'entreprise
     * @param $entreprise_nom
     * @return mixed
     */
    public function findByEntrepriseNom($entreprise_nom)
    {
       return OffreStage::whereNomEntreprise($entreprise_nom)
           ->orderBy('updated_at', 'desc')
           ->orderBy('nombre_vu', 'desc')
           ->get();
    }

    /**
     * Retourne true si l'offre est validée, false sinon
     *
     * @param $id
     * @return bool
     */
    public function isValide($id)
    {
        $offre = OffreStage::find($id)->valide;
        return ($offre == 1);
    }

    /**
     * Retourne la liste du nombre de candidatures pour chaque offre.
     *
     * @return mixed
     */
    public function getNbCandidats()
    {
        return DB::table('etudiant_offre_stage')
            ->select('offre_stage_id', DB::raw('count(*) as total'))
            ->groupBy('offre_stage_id')
            ->lists('total', 'offre_stage_id');
    }

    /**
     * ajouter la relation de l'offre de stage et du departement
     * @param $departements
     * @param $offre
     * @return mixed
     */
    public function insertOffreDepartements($departements,$offre){
            $array=[];
            if($departements!=null) {
                foreach ($departements as $departement) {
                    array_push($array,$departement);
                }
            }
        return $offre->departements()->sync($array);
        }


    /**
     * ajouter la relationde l'offre de stage et du niveau
     * @param $niveaux
     * @param $offre
     * @return mixed
     */
    public function insertOffreNiveaux($niveaux,$offre){
        $array=[];
        if($niveaux!=null) {
            foreach ($niveaux as $niveau) {
                array_push($array,$niveau);
            }
        }
        return $offre->niveaux()->sync($array);
    }

    /**
     * upload un fichier dans le repertoire /public/upload/entreirse/..
     * @param $file
     * @param $offre
     * @return null
     */
    public function uploadFile($file,$offre){
        if($file!=null) {
            if ($file->isValid()) {
                $exten = $file->getClientOriginalExtension();
                if ($exten == 'pdf' || $exten == 'PDF' || $exten == 'doc' || $exten == 'docx' || $exten == 'odt') {
                    $nom = $offre->nom_entreprise .'_'.$offre->titre. '.'.$exten ;
                    $path_local = 'uploads/entreprises/' . md5($offre->nom_entreprise);
                    $file->move($path_local, $nom);
                    $offre->ficher_detail = $path_local . '/' . $nom;
                    return $offre->save();
                }
            }
        }
        return null;
    }


    /**
     * envoyer un email pour confirmer la publcaition de l'offre de stage
     * @param $expediteur
     * @param $expediteur_mail
     * @param $destination
     * @param $destination_mail
     * @param $file
     * @return mixed
     */
    public function envoyerMail($expediteur,$expediteur_mail,$destination,$destination_mail,$file,$route,$subject){
        if($file==null){
            Mail::send(
                $route,
                array('nom' => $destination),
                function ($message)
                use ($expediteur,$expediteur_mail,$destination,$destination_mail,$file,$subject) {
                    $message->from($expediteur_mail, $expediteur)
                        ->to($destination_mail)
                        ->subject($subject);
                });
        }
        else{
            Mail::send(
                $route,
                array('nom' => $destination),
                function ($message)
                use ($expediteur,$expediteur_mail,$destination,$destination_mail,$file,$subject) {
                    $message->attach($file)
                        ->from($expediteur_mail, $expediteur)
                        ->to($destination_mail)
                        ->subject($subject);
                });

        }

    }


}
