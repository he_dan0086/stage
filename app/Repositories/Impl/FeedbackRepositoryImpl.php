<?php

namespace App\Repositories\Impl;


use App\Feedback;
use App\Repositories\FeedbackRepository;

class FeedbackRepositoryImpl implements FeedbackRepository
{

    /**
     * retour la liste des feedback qui sont validés  avec son créateur
     * @return mixed
     */
    public function findByValideWithCreateur($valide)
    {
        return Feedback::whereValide($valide)->with('etudiant')->get();
    }

    /**
     * recherche le feedback par son id
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Feedback::whereId($id)->first();
    }

    /**
     * recherche le feedback par le createur
     * @param $createur
     * @return mixed
     */
    public function findByCreateur($createur)
    {
        return Feedback::whereCreatedBy($createur)
            ->with('etudiant')
            ->get();
    }

    /**
     * recherche les feedbacks où le id de son createur est dans la liste de id fournit
     *
     * @param $etudiant_id_liste
     * @return mixed
     */
    public function findWhereCreateurIn($etudiant_id_liste)
    {
        return Feedback::whereIn('created_by',$etudiant_id_liste)->with(['etudiant'])->orderby('entreprise')->get();
    }

    /**
     * retoue la liste de feedbacks
     * @return mixed
     */
    public function all()
    {
        return Feedback::with('etudiant')->get();
    }
}