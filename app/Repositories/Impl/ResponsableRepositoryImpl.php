<?php

namespace App\Repositories\Impl;


use App\Repositories\ResponsableRepository;
use App\Responsable;

class ResponsableRepositoryImpl implements ResponsableRepository
{

    /** cherche les responsables par le id de filiere
     * @param $id
     * @return mixed
     */
    public function findByFiliereId($id)
    {
       return Responsable::whereFiliereId($id)->get();
    }

    /**
     * get la liste de responsables avec le filiere
     * @return mixed
     */
    public function allWithFiliere()
    {
        return Responsable::with('filiere')->get();
    }

    /**
     * recherche le responsable par ID
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Responsable::whereId($id)->first();
    }
}