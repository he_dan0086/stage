<?php

namespace App\Repositories\Impl;


use App\Download;
use App\Repositories\TemplateRepository;
use App\Template;

class TemplateRepositoryImpl implements TemplateRepository
{

    /**
     * recheche le template par le langage
     * @param $lang
     * @return mixed
     */
    public function findDownloadByLang($lang)
    {
        return Download::whereLangage($lang)->first();
    }

    /**
     * retour la liste de templates;
     * @return mixed
     */
    public function allDownloads()
    {
        return Download::all();
    }

    /**
     * recherche la template par la filiere
     * @param $filiere
     * @return mixed
     */
    public function findTemplateByFiliere($filiere)
    {
        return Template::whereFiliere($filiere)->first();
    }
}