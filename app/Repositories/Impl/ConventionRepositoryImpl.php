<?php

namespace App\Repositories\Impl;


use App\Convention;
use App\Filiere;
use App\Repositories\ConventionRepository;

class ConventionRepositoryImpl implements ConventionRepository{


    /**
     * retour tous les conventions dans la DB
     * @return mixed
     */
    public function all()
    {
        return Convention::all();
    }

    /**
     * cherche une convention par id
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function findById($id)
    {
        return Convention::find($id);
    }

    /**
     * chercher les conventions par la filiere
     * @param $filiere
     * @return mixed
     */
    public function findByFiliere($filiere_id)
    {
        $filiere=Filiere::whereId($filiere_id)->first();
        return Convention::whereFiliere($filiere->filiere_nom)->get();
    }
}