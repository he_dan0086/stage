<?php  namespace App\Repositories;


interface OffreStageRepository {

    /**
     * Retourne toutes les offres de stages
     *
     * @return mixed
     */
    public function all();

    /**
     * Retourne true si l'offre est validée, false sinon
     *
     * @param $id
     * @return bool
     */
    public function isValide($id);



    /**
     * Retourne la liste du nombre de candidatures pour chaque offre.
     *
     * @return mixed
     */
    public function getNbCandidats();

    /**
     * cherche l'offre de stage par id
     * @param $id
     * @return mixed
     */
    public function findbyId($id);

    /**
     * cherche l'offre de satge par le createur
     * @param $createor
     * @return mixed
     */
    public function findByCreator($createor);

    /**
     * cherche l'offre de stage par le nom de l'entreprise
     * @param $entreprise_nom
     * @return mixed
     */
    public function findByEntrepriseNom($entreprise_nom);


    /**
     * ajouter la relation de l'offre de stage et du departement
     *
     * @param $departements
     * @param $offre_id
     * @return mixed
     */
    public function insertOffreDepartements($departements,$offre);

    /**
     * ajouter la relationde l'offre de stage et du niveau
     *
     * @param $niveaux
     * @param $offre_id
     *
     */
    public function insertOffreNiveaux($niveaux,$offre);


    /**
     * upload un fichier, fait copier dans le repertoire /public/upload/..
     * @param $file
     * @return mixed
     */
    public function uploadFile($file,$offre);


    /**
     * envoyer un email pour confirmer la publcaition de l'offre de stage
     *
     * @param $expediteur
     * @param $expediteur_mail
     * @param $destination
     * @param $destination_mail
     * @param $file
     * @return mixed
     */
    public function envoyerMail($expediteur,$expediteur_mail,$destination,$destination_mail,$file,$route,$subject);


}
