<?php

namespace App\Repositories;


interface FeedbackRepository
{
    /**
     * retour la liste des feedback qui sont validés  avec son créateur
     * @return mixed
     */
    public function findByValideWithCreateur($valide);

    /**
     * recherche le feedback par son id
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * recherche le feedback par le createur
     * @param $createur
     * @return mixed
     */
    public function findByCreateur($createur);

    /**
     * recherche les feedbacks où le id de son createur est dans la liste de id fournit
     *
     * @param $etudiant_id_liste
     * @return mixed
     */
    public function findWhereCreateurIn($etudiant_id_liste);

    /**
     * retoue la liste de feedbacks
     * @return mixed
     */
    public function all();

}