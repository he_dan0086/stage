<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Responsable extends Model {

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'responsables';
    protected $fillable = ['nom','prenom','filiere_id'];

    public function filiere(){
        return $this->belongsTo('App\Filiere','filiere_id');
    }

    public function user()
    {
        return $this->morphOne('App\User', 'user');
    }

}
