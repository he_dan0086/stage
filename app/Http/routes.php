<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::controllers(
    [
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]
);

/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                      ENTREPRISE                                        |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */


Route::group(['prefix' => 'entreprise','middleware' => 'auth.entreprise'], function () {

    Route::get('offre-stage/list',
        [
            'as'  => 'offres_list_login',
            'uses'=>'Entreprise\EntrepriseOffreStageController@index'
        ]
    );

    Route::get('offre-stage/show/{id_offre}',[
        'as' => 'afficher-offre',
        'uses'=>'Entreprise\EntrepriseOffreStageController@show'
    ]);

    Route::get('liste-etudiants',[
        'as' => 'etudiant_liste',
        'uses'=>'Entreprise\EntrepriseEtudiantController@getAllEtudiant'
    ]);
    Route::get('etudiant/show/{id}',[
        'as' => 'etudiant_show',
        'uses'=>'Entreprise\EntrepriseEtudiantController@show'
    ]);

    Route::get('profil',[
        'as' => 'entreprise_profil',
        'uses'=>'Entreprise\EntrepriseProfilController@profil'
    ]);

     Route::post('profil',[
         'as' => 'entreprise_profil_post',
         'uses'=>'Entreprise\EntrepriseProfilController@postProfil'
     ]);

});
Route::group(['prefix' => 'entreprise'], function () {

    Route::get('offre-stage/create',
        [
            'as' => 'offre-stage-entreprise',
            'uses'=>'Entreprise\EntrepriseOffreStageController@create'
        ]
    );

    Route::post('offre-stage/create',
        [
            'as'  => 'offre-create',
            'uses'=>'Entreprise\EntrepriseOffreStageController@store'
        ]
    );

    Route::get('offre-stage/{entreprise}/{code}/list',
        [
            'as'  => 'offres-list',
            'uses'=>'Entreprise\EntrepriseOffreStageController@showMyList'
        ]
    );

    Route::get('offre-stage/{id_offre}',[
        'as' => 'afficher-offre',
        'uses'=>'Entreprise\EntrepriseOffreStageController@show'
    ]);

    Route::get('/download',
        [
            'uses' => 'Entreprise\EntrepriseOffreStageController@getDownload'
        ]);

});

/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                      CAS Authentification                                           |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */


Route::get('cas',
    [
        'as' => 'cas_login',
        'uses' => 'Auth\AuthCASController@login'
    ]
);
Route::get('/cas/logout',
    [
        'as' => 'cas_logout',
        'uses' => 'Auth\AuthCASController@getLogout'
    ]
);




/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                     ETUDIANT                                                     |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */
//Route::group(['prefix' => 'login', 'middleware' => ['cas.auth']], function () {

Route::group(['prefix' => 'etudiant','middleware' => 'auth.etudiant'], function () {

    Route::get('views/{name}', function($name) {

        if (View::exists($name)) {
            return View::make($name);
        }
        return "Error 404";
    });

    Route::get('/',
        [
            'as' => 'racine-etudiant',
            'uses' => function () {
                return View::make('etudiant.layout');
            }
        ]
    );
    Route::get('recherche-stage',
        [
            'as' => 'recherche-stage',
            'uses' => 'Etudiant\EtudiantOffreStageController@index'
        ]
    );
    Route::get('offre-stage/{id}',
        [
            'as' => 'show-stage',
            'uses' => 'Etudiant\EtudiantOffreStageController@show'
        ]
    );

    Route::get('feedbacks',
        [
            'as' => 'feedback-etudiant',
            'uses' => 'Etudiant\EtudiantFeedBackController@index'
        ]
    );
    Route::get('feedback/{id}',
        [
            'as' => 'show-feedback-etudiant',
            'uses' => 'Etudiant\EtudiantFeedBackController@show'
        ]
    );
    Route::get('feedback-liste',
        [
            'as' => 'feedback-etudiant',
            'uses' => 'Etudiant\EtudiantFeedBackController@myList'
        ]
    );
    Route::get('feedback/{id}/edit',
        [
            'as' => 'edit-feedback-etudiant',
            'uses' => 'Etudiant\EtudiantFeedBackController@edit'
        ]
    );
    Route::post('feedback/{id}/edit',
        [
            'as' => 'post-edit-feedback-etudiant',
            'uses' => 'Etudiant\EtudiantFeedBackController@update'
        ]
    );
    Route::get('gestion-feedback/create',
        [
            'as' => 'creat-feedback',
            'uses' => 'Etudiant\EtudiantFeedBackController@create'
        ]
    );
    Route::post('gestion-feedback/create',
        [
            'as' => 'post-create-feedback',
            'uses' => 'Etudiant\EtudiantFeedBackController@store'
        ]
    );
    Route::delete('feedback/delete/{id}',
        [
            'as' => 'etudiant-delete-feedback',
            'uses' => 'Etudiant\EtudiantFeedBackController@delete'
        ]
    )->where('id', '[0-9]+');


    Route::get('cv',
        [
            'as' => 'cv-etudiant',
            'uses' => 'Etudiant\EtudiantCVController@getEtudiantCV'
        ]
    );
    Route::get('profil',
        [
            'as' => 'profil-etudiant',
            'uses' => 'Etudiant\EtudiantProfilController@profil'
        ]
    );
    Route::post('profil',
        [
            'as' => 'etudiant_profil_post',
            'uses' => 'Etudiant\EtudiantProfilController@postProfil'
        ]
    );

    Route::post('cv',
        [
            'as' => 'cv-etudiant',
            'uses' => 'Etudiant\EtudiantCVController@postEtudiantCV'
        ]
    );
    Route::get('convention',
        [
            'as' => 'convention_get',
            'uses' => 'Etudiant\EtudiantConventionController@create'
        ]
    );
    Route::get('convention/verifier',
        [
            'as' => 'convention_verifier',
            'uses' => 'Etudiant\EtudiantConventionController@showAllInput'
        ]
    );

    Route::post('convention',
        [
            'as' => 'convention_post',
            'uses' => 'Etudiant\EtudiantConventionController@store'
        ]
    );
    Route::get('experience-professionnel',
        [
            'as' => 'experience_professionel_get',
            'uses' => 'Etudiant\EtudiantConventionController@experienceCreate'
        ]
    );

    Route::post('experience-professionnel',
        [
            'as' => 'experience_professionel_post',
            'uses' => 'Etudiant\EtudiantConventionController@experienceStore'
        ]
    );
});


/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                      Authentification                                         |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */
Route::get('/',
    [
        'as' => 'index',
        'uses' => 'HomeController@pageIndex'
    ]
);
Route::get('/login',
    [
        'as' => 'login_get',
        'uses' => 'Auth\AuthController@getLogin'
    ]
);
Route::post('/login',
    [
        'as' => 'login_post',
        'uses' => 'Auth\AuthController@postLogin'
    ]
);
Route::get('/logout',
    [
        'as' => 'logout',
        'uses' => 'Auth\AuthController@getLogout'
    ]
);
Route::get('/inscription-etudiant',
    [
        'as' => 'inscription_etudiant',
        'uses' => 'Auth\AuthController@getEtudiantInscription'
    ]
);
Route::post('/inscription-etudiant',
    [
        'as' => 'inscription_entreprise_post',
        'uses' => 'Auth\AuthController@postEtudiantInscription'
    ]
);
Route::get('/inscription-entreprise',
    [
        'as' => 'inscription_entreprise',
        'uses' => 'Auth\AuthController@getEntrepriseInscription'
    ]
);
Route::post('/inscription-entreprise',
    [
        'as' => 'inscription_entreprise_post',
        'uses' => 'Auth\AuthController@postEntrepriseInscription'
    ]
);
Route::get('/inscription/active/{id}/{token}',
    [
        'as' => 'active_inscription',
        'uses' => 'Auth\AuthController@activerInscription'
    ]
);
Route::get('/accueil',
    [
        'as' => 'accueil',
        'uses' => 'Auth\AuthController@getAccueil'
    ]
);
/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                    RESPONSABLE                                                   |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */

Route::group(['prefix' => 'responsable', 'middleware' => 'auth.responsable'], function () {
    Route::get('accueil',
        [
            'as' => 'accueil-responsable',
            'uses' => function () {
                return redirect(route('responsable_profil_post'));
            }
        ]
    );


    Route::get('conventions',
        [
            'as' => 'responsable_gestion_convention_get',
            'uses' => 'Responsable\ResponsableConventionController@index'
        ]
    );
    Route::get('conventions/download/{id}/{lang}',
        [
            'as' => 'responsable_conv_show',
            'uses' => 'Responsable\ResponsableConventionController@afficher'
        ]);

    Route::post('conventions/{id}/edit/{lang}',
        [
            'as' => 'responsable_upload_conv',
            'uses' => 'Responsable\ResponsableConventionController@update'
        ]
    );

    Route::delete('conventions/delete/{id}/{en}',
        [
            'as' => 'responsable_delete_conv',
            'uses' =>'Responsable\ResponsableConventionController@destroy'
        ]);

    Route::get('templates',
        [
            'as' => 'responsable_template_get',
            'uses' => 'Responsable\ResponsableTemplateController@getTemplates'
        ]
    );

    Route::post('templates/{lang}',
        [
            'as' => 'responsable_upload_template_post',
            'uses' => 'Responsable\ResponsableTemplateController@postUploadConv'
        ]
    );
    Route::get('template/download/{lang}',
        [
            'as' => 'responsable_templ_download',
            'uses' => 'Responsable\ResponsableTemplateController@downloadTempl'
        ]);

    Route::get('feedbacks',
        [
            'as' => 'responsable_feedback_get',
            'uses' => 'Responsable\ResponsableFeedbackController@getFeedbacks'
        ]
    );
    Route::post('feedback/{id}',
        [
            'as' => 'valider-feedback-responsable',
            'uses' => 'Responsable\ResponsableFeedbackController@valider'
        ]
    );
    Route::delete('feedback/{id}',
        [
            'as' => 'responsable_delete_feedback',
            'uses' =>'Responsable\ResponsableFeedbackController@destroy'
        ]);

    Route::get('profil',
        [
            'as' => 'responsable_get_profil',
            'uses' => 'Responsable\ResponsableProfilController@index'
        ]
    );
    Route::post('profil',
        [
            'as' => 'responsable_profil_post',
            'uses' => 'Responsable\ResponsableProfilController@postProfil'
        ]
    );




});
/*
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 |                                                    ADMINISTRATEUR                                                   |
 |---------------------------------------------------------------------------------------------------------------------|
 |---------------------------------------------------------------------------------------------------------------------|
 */


Route::group(['prefix' => 'administrateur', 'middleware' => 'auth.administrateur'], function () {

    Route::get('accueil',
        [
            'as' => 'accueil-administrateur',
            'uses' => function () {
                return redirect(route('admin_identite_ecole_get'));
            }
        ]
    );

    /******************************
     * gestion des responsables
     ******************************/

    Route::get('responsables',
        [
            'as' => 'responsable_get',
            'uses' => 'Admin\AdminResponsableController@read'
        ]
    );

    Route::post('responsables',
        [
            'as' => 'responsable_post',
            'uses' => 'Admin\AdminResponsableController@create'
        ]
    );

    Route::post('responsables/update/{id}',
        [
            'as' => 'responsable_update',
            'uses' => 'Admin\AdminResponsableController@update'
        ]
    )->where('id', '[0-9]+');

    Route::get('responsables/delete/{id}',
        [
            'as' => 'responsable_delete',
            'uses' => 'Admin\AdminResponsableController@delete'
        ]
    )->where('id', '[0-9]+');

    /******************************
     * gestion des etudiants
     ******************************/

    Route::get('etudiants',
        [
            'as' => 'etudiant_get',
            'uses' => 'Admin\AdminEtudiantController@read'
        ]
    );
    Route::post('etudiants/update/{id}',
        [
            'as' => 'etudiant_update',
            'uses' => 'Admin\AdminEtudiantController@update'
        ]
    )->where('id', '[0-9]+');

    Route::delete('etudiants/delete/{id}',
        [
            'as' => 'etudiant_delete',
            'uses' => 'Admin\AdminEtudiantController@delete'
        ]
    )->where('id', '[0-9]+');

    /******************************
     * gestion des entreprises
     ******************************/

    Route::get('entreprises',
        [
            'as' => 'entreprise_get',
            'uses' => 'Admin\AdminEntrepriseController@read'
        ]
    );

    Route::post('entreprise/update/{id}',
        [
            'as' => 'entreprise_update',
            'uses' => 'Admin\AdminEntrepriseController@update'
        ]
    )->where('id', '[0-9]+');

    Route::delete('entreprise/delete/{id}',
        [
            'as' => 'entreprise_delete',
            'uses' => 'Admin\AdminEntrepriseController@delete'
        ]
    )->where('id', '[0-9]+');



    /**********************************************
     *    identifi de l'école'
     *********************************************/
    Route::get('identite-ecole',
        [
            'as' => 'admin_identite_ecole_get',
            'uses' => 'Admin\IdentiteEcoleController@getIdentite'
        ]
    );

    Route::post('identite-ecole/logo',
        [
            'as' => 'admin_identite_ecole_logo_post',
            'uses' => 'Admin\IdentiteEcoleController@postLogo'
        ]
    );

    Route::delete('identite-ecole/logo/delete',
        [
            'as' => 'admin_identite_ecole_logo_delete',
            'uses' => 'Admin\IdentiteEcoleController@deleteLogo'
        ]
    );

    Route::post('identite-ecole/nom',
        [
            'as' => 'admin_identite_ecole_nom_post',
            'uses' => 'Admin\IdentiteEcoleController@postNom'
        ]
    );

    Route::delete('identite-ecole/nom/delete',
        [
            'as' => 'admin_identite_ecole_nom_delete',
            'uses' => 'Admin\IdentiteEcoleController@deleteNom'
        ]
    );
    /**********************************************
     *    niveaux des etudes(quelle annee)
     *********************************************/
    Route::get('niveaux',
        [
            'as' => 'admin_niveaux_get',
            'uses' => 'Admin\AdminNiveauxController@read'
        ]
    );

    Route::post('niveaux',
        [
            'as' => 'admin_niveaux_post',
            'uses' => 'Admin\AdminNiveauxController@create'
        ]
    );

    Route::get('niveaux/delete/{id}',
        [
            'as' => 'admin_niveaux_delete',
            'uses' => 'Admin\AdminNiveauxController@delete'
        ]
    )->where('id', '[0-9]+');

    Route::post('niveaux/update/{id}',
        [
            'as' => 'admin_niveaux_update',
            'uses' => 'Admin\AdminNiveauxController@update'
        ]
    )->where('id', '[0-9]+');

    /**********************************************
     *    departements de l'école
     *********************************************/
    Route::get('departements',
        [
            'as' => 'admin_departement_get',
            'uses' => 'Admin\AdminDepartementController@read'
        ]
    );

    Route::post('departements',
        [
            'as' => 'admin_departe_post',
            'uses' => 'Admin\AdminDepartementController@create'
        ]
    );

    Route::post('departements/update/{id}',
        [
            'as' => 'admin_departe_update',
            'uses' => 'Admin\AdminDepartementController@update'
        ]
    )->where('id', '[0-9]+');

    Route::get('departements/delete/{id}',
        [
            'as' => 'admin_departe_delete',
            'uses' => 'Admin\AdminDepartementController@delete'
        ]
    )->where('id', '[0-9]+');
    /**********************************************
     *    Filieres de l'école
     *********************************************/
    Route::get('filieres',
        [
            'as' => 'admin_filiere_get',
            'uses' => 'Admin\AdminFiliereController@read'
        ]
    );

    Route::post('filieres',
        [
            'as' => 'admin_filiere_post',
            'uses' => 'Admin\AdminFiliereController@create'
        ]
    );

    Route::post('filieres/update/{id}',
        [
            'as' => 'admin_filiere_update',
            'uses' => 'Admin\AdminFiliereController@update'
        ]
    )->where('id', '[0-9]+');

    Route::get('filieres/delete/{id}',
        [
            'as' => 'admin_filiere_delete',
            'uses' => 'Admin\AdminFiliereController@delete'
        ]
    )->where('id', '[0-9]+');

    Route::get('upload_template',
        [
            'as' => 'admin_upload_template_get',
            'uses' => 'Admin\AdminTemplateController@getUploadConv'
        ]
    );

    Route::post('upload_template/{en}',
        [
            'as' => 'admin_upload_template_post',
            'uses' => 'Admin\AdminTemplateController@postUploadConv'
        ]
    );
    Route::get('template/download/{lang}',
        [
            'as' => 'admin_templ_download',
            'uses' => 'Admin\AdminTemplateController@downloadTempl'
        ]);



    Route::get('conventions/download/{id}/{lang}',
        [
            'as' => 'admin_conv_show',
            'uses' => 'Admin\AdminConventionController@afficher'
        ]);


    Route::get('conventions',
        [
            'as' => 'admin_gestion_convention_get',
            'uses' => 'Admin\AdminConventionController@index'
        ]
    );

    Route::post('conventions/{id}/edit/{lang}',
        [
            'as' => 'admin_upload_conv',
            'uses' => 'Admin\AdminConventionController@update'
        ]
    );

    Route::delete('conventions/delete/{id}/{en}',
        [
            'as' => 'admin_delete_conv',
            'uses' =>'Admin\AdminConventionController@destroy'
        ]);

    Route::get('stageouvrier',
        [
            'as' => 'admin_gestion_stageouvrier_get',
            'uses' => 'Admin\AdminStageOuvrierController@index'
        ]
    );
    Route::get('stageouvrier/download/{id}/{lang}',
        [
            'as' => 'admin_stage_ouvrier_show',
            'uses' => 'Admin\AdminStageOuvrierController@afficher'
        ]);
    Route::post('stageouvrier/{id}/edit/{lang}',
        [
            'as' => 'admin_upload_stage_ouvrier',
            'uses' => 'Admin\AdminStageOuvrierController@update'
        ]
    );
    Route::delete('stageouvrier/delete/{id}/{en}',
        [
            'as' => 'admin_delete_stage_ouvrier',
            'uses' =>'Admin\AdminStageOuvrierController@destroy'
        ]);

    Route::get('feedbacks',
        [
            'as' => 'admin_gestion_feedback_get',
            'uses' => 'Admin\AdminFeedbackController@index'
        ]);

    Route::post('feedback/{id}',
        [
            'as' => 'post-edit-feedback-admin',
            'uses' => 'Admin\AdminFeedbackController@update'
        ]
    );
    Route::delete('feedback/{id}',
        [
            'as' => 'admin_delete_feedback',
            'uses' =>'Admin\AdminFeedbackController@destroy'
        ]);




    /**********************************************
     *    gestion des offre de stage(CRUD)
     *********************************************/
    Route::resource('offre_stage', 'Admin\AdminOffreStageController');

    Route::get('offre_stage', [
        'as' => 'admin_gestion_offre_get',
        'uses' => 'Admin\AdminOffreStageController@index'
    ]);
    Route::post('offre_stage/file_upload/{id}', [
        'as' => 'admin_gestion_offre_file_post',
        'uses' => 'Admin\AdminOffreStageController@modifierFile'
    ]);

    Route::post('offre_stage/validation/{id}',
        [
            'as'   => 'validation_offrestage',
            'uses' => 'Admin\AdminOffreStageController@validerOffreStage'
        ]
    )->where('id', '[0-9]+');

});






