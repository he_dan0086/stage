<?php
namespace App\Http\Requests\Etudiant;


use Symfony\Component\HttpFoundation\Request;

class EtudiantConventionRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Filiere' =>'required',
            'GenreEtu' =>'required',
            'NomEtu' =>'required',
            'PrenomEtu' =>'required',
            'EmailEtu' =>'required',
            'DateNaissEtu' =>'required',
            'AdressEtu' =>'required',
            'VilleEtu' =>'required',
            'Assurance' =>'required',

            'NomEntreprise' =>'required',
            'AdrSiegeSocial' =>'required',
            'VilleSiegeSocial' =>'required',
            'Adresse1Stage' =>'required',
            'VilleStage' =>'required',
            'GenreDirEnt' =>'required',
            'NomDirEnt' =>'required',
            'PrenomDirEnt' =>'required',
            'EmailDirEnt' =>'required',
            'QualiteDirEnt' =>'required',

            'DateDebut' =>'required',
            'DateFin' =>'required',
            'DureeHebdo' =>'required',
            'MontantGrat' =>'required',

            'MontantGratEuros' =>'required',
            'TitreStage' =>'required',
            'DescrStage' =>'required',
            'GenreEncadreur' =>'required',
            'NomEncadreur' =>'required',
            'PrenomEncadreur' =>'required',
            'EmailEncadreur' =>'required',
            'FctEncadreur' =>'required',

            'GenreEnsResp' =>'required',
            'NomEnsResp' =>'required',
            'PrenomEnsResp' =>'required',
            'EmailEnsResp' =>'required',

            'NumSS' => 'required | numeric',
            'NumEtu' => 'required | numeric',
            'TelEtu' => 'required | numeric ',
            'CPEtu' => 'required | numeric | min:5',
            'AssuranceNo' => 'required | numeric',

            'CPSiegeSocial' => 'required | numeric | min:5',
            'CPStage' => 'required | numeric | min:5',
            'TelDirEnt' => 'required | numeric',

            'TelEncadreur' => 'required | numeric',
            'TelEnsResp' => 'required | numeric',
        ];
    }
}