<?php
namespace App\Http\Requests\Etudiant;


use Illuminate\Support\Facades\Request;

class EtudiantStageOuvrierRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'NumSS' => 'required | numeric',
            'NumEtu' => 'required | numeric',
            'CPSiegeSocial' => 'required | numeric | min:5',
        ];
    }
}