<?php namespace App\Http\Requests\Entreprise;

use App\Http\Requests\Request;

class OffreStageRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'niveau_0'=>'required',
            'departement_0' => 'required',

            'nom_entreprise' => 'required|max:60',
            'code_postal' => 'numeric | required|min:5',
            'ville' => 'required|max:60',
            'pays' => 'max:60',
            'email' => 'required|max:60',

            'titre'=>'required',
            'description' => 'max:255',
            'gratification' => 'numeric|max:1000000',
            'devise' => 'max:60',
            'ficher_detail' => 'required',
        ];

    }

}