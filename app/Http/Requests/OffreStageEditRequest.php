<?php namespace App\Http\Requests;


class OffreStageEditRequest extends Request {


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'niveau_0'=>'required',
            'departement_0' => 'required',

            'nom_entreprise' => 'required|max:60',
            'code_postal' => 'required|max:60',
            'ville' => 'required|max:60',
            'pays' => 'max:60',
            'email' => 'required|max:60',

            'description' => 'required|max:10000',
            'devise' => 'max:60',
            "competence_0" => 'required',
        ];

    }

}