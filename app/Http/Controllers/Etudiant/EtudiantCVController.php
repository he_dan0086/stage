<?php namespace App\Http\Controllers\Etudiant;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Etudiant\EtudiantCVRequest;
use App\Repositories\EtudiantRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class EtudiantCVController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | CV Etudiant Controller
    |--------------------------------------------------------------------------
    |
    | Ce controller permet la gestion de son CV (upload, edition)
    |
    | copyright 2015 AdopteUnStage
    */

    protected $etudiantRepository;

    public function __construct(EtudiantRepository $etudiantRepository)
    {
        $this->etudiantRepository=$etudiantRepository;

    }

    /**
     * obtenir le cv de l'etudiant
     *
     * @return \Illuminate\View\View
     */
    public function getEtudiantCV(){
        $cv = $this->etudiantRepository->findById(Auth::user()->user_id)->cv;
        return view('etudiant.cv', compact('cv'));
    }

    /**
     * update un cv de l'etudiant
     *
     * @param EtudiantCVRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEtudiantCV(EtudiantCVRequest $request){

        $etudiant =$this->etudiantRepository->findById(Auth::user()->user->id);
        if($etudiant->cv)
            File::delete($etudiant->cv);

        DB::transaction(function() use ($request, $etudiant) {
            $file = $request->file('cv');
            if ($file != null) {
                if($file->getClientOriginalExtension() == 'pdf' || $file->getClientOriginalExtension() == 'PDF'){
                    $pdf = 'cv.pdf';
                    $cv = 'uploads/etudiants/'.md5(Auth::user()->name);
                    $file->move($cv, $pdf);
                    $etudiant->cv = $cv.'/'.$pdf;
                }
            }
        });

        $etudiant->save();

        return redirect(route('cv-etudiant', compact('cv')));

    }

}
