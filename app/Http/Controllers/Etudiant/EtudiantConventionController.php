<?php

namespace App\Http\Controllers\Etudiant;

use App\Convention;
use App\Http\Controllers\Controller;
use App\Http\Requests\Etudiant\EtudiantConventionRequest;
use App\Http\Requests\Etudiant\EtudiantStageOuvrierRequest;
use App\Repositories\FiliereRepository;
use App\Repositories\MailRepository;
use App\Repositories\ResponsableRepository;
use App\Repositories\TemplateRepository;
use App\Repositories\UserRepository;
use App\Repositories\VariableRepository;
use App\StageOuvrier;
use App\Variable;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use PhpOffice\PhpWord\PhpWord;

/*
|--------------------------------------------------------------------------
| Etudiant Convention Controller
|--------------------------------------------------------------------------
|
| Ce controller permet etudiant de créer la convention et de valider une expérience de stage ouvrier
|
| copyright 2015 AdopteUnStage
|
*/
class EtudiantConventionController extends Controller
{

    protected $filiereRepository,$responsableRepository,
        $templateRepository,$mailRepository, $variableRepository,$userRepository;


    public function __construct(FiliereRepository $filiererepository,
                                ResponsableRepository $responsableRepository,
                                TemplateRepository $templateRepository,
                                MailRepository $mailRepository,
                                VariableRepository $variableRepository,
                                UserRepository $userRepository)
    {
        $this->filiereRepository = $filiererepository;
        $this->responsableRepository=$responsableRepository;
        $this->templateRepository=$templateRepository;
        $this->mailRepository=$mailRepository;
        $this->variableRepository = $variableRepository;
        $this->userRepository=$userRepository;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array['values']=$this->variableRepository->findByUserCurrent();
        $array['filieres']=$this->filiereRepository->all();
        return View::make('etudiant.create_convention')->with($array);
    }

    /**
     * retourner une vue de tous les inputs pour que l'utilisateur puisse vérifier les infos
     * @return mixed
     */
    public function showAllInput(){
        $infos=Input::all();
        $array['infos']=$infos;
        $valid=true;
        foreach ($infos as $info) {
            if(trim($info) == ''){
                $valid=false;
            }
        }
        $array['valide']=$valid;
        $array['filieres']=$this->filiereRepository->all();
        return View::make('etudiant.infos_convention')->with($array);
    }


    /**
     * créer une convention, jouter les champs dans la base de données
     * @return mixed
     */
    public function store(EtudiantConventionRequest $etudiantConventionRequest){
        if(isset($_POST['oui_button'])){

            $this->deleteInputVariables();

            $conv =new Convention();
            $conv->nom=Input::get('NomEtu');
            $conv->prenom=Input::get('PrenomEtu');
            $conv->filiere=Input::get('Filiere');

            $filiere=$this->filiereRepository->findByNom(Input::get('Filiere'));
            if($filiere != null){
                $responsables=$this->responsableRepository->findByFiliereId($filiere->id);
                $template=$this->templateRepository->findTemplateByFiliere($filiere->filiere_nom);
            }

            $files=array();

            if(Input::get('EstEtranger')=="Oui ") {
                $conv->estEtranger=true;
                if(isset($template) && $template->template_en != null){
                    $conv->convention_en = $this->createConvention(new EtudiantConventionRequest(),$template->template_en, 'en');
                    array_push($files,$conv->convention_en);
                }

                if(isset($template) && $template->template_biling != null){
                    $conv->convention_biling = $this->createConvention(new EtudiantConventionRequest(),$template->template_biling, 'biling');
                    array_push($files,$conv->convention_biling);
                }
            }
            $conv->estEtranger=false;
            if(isset($template) &&  $template->template_fr){
                $conv->convention_fr=$this->createConvention(new EtudiantConventionRequest(),$template->template_fr,'fr');
                array_push($files,$conv->convention_fr);
            }
            $conv->save();

            if($conv){
                //email pour responsable
                if(isset($responsables) && ($responsables-> first() != null))
                    foreach($responsables as $responsable){
                        $array['nom']=$responsable->nom.' '.$responsable->prenom;
                        $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                            $responsable->nom.' '.$responsable->prenom,$responsable->user->email,$files,
                            'emails.convention_responsable',$array,
                            '[Etablisation de convention sur AdopteUnStage]');
                    }

                return View::make('etudiant.reussite_convention');
            }

            return Redirect::back()->with('flash_error', 'Echoué de création de la convention!!!');
        }
        else if(isset($_POST['rengistrer_button'])){
            $inputs=Input::all();
            while ($value = current($inputs)) {
                if(trim($value) != '')
                {
                    $variable = new Variable();

                    $variable->updateOrCreate(
                        ['key' => Auth::user()->email.'+'.key($inputs), 'value' => $value]
                    );
                }
                next($inputs);
            }
            return redirect()->to(route('convention_get'))->with('flash_success','Vos Saisis sont bien enregistées, vous pouvez continuer vos saisis la prochaine fois!!');

        }

    }

    /**
     * return une vue sur la creation de l'experience de stage ouvrier
     * @return mixed
     */
    public function experienceCreate(){
        $filieres=$this->filiereRepository->all();
        return View::make('etudiant.create_experience_prof',compact('filieres'));
    }


    /** Erire dans BD d'un récord de StageOuvrier
     * @return mixed
     */
    public function experienceStore(EtudiantStageOuvrierRequest $etudiantStageOuvrierRequest){
        $stageouvrier=new StageOuvrier();
        $nom=Input::get('NomEtu');
        $prenom=Input::get('PrenomEtu');

        $stageouvrier->nom=$nom;
        $stageouvrier->prenom=$prenom;
        $stageouvrier->filiere=Input::get('Filiere');

        $file=Input::file('Doc_justificatif');
        $date=Carbon::now()->toDateTimeString();
        $template_ouvrier=$this->templateRepository->findDownloadByLang('ouvrier');

        $filiere=$this->filiereRepository->findByNom(Input::get('Filiere'));
        $responsables=$this->responsableRepository->findByFiliereId($filiere->id);


        if($file!=null) {
            if ($file->isValid()) {
                $exten = $file->getClientOriginalExtension();
                if ( $exten == 'doc' || $exten == 'docx' || $exten == 'odt'||$exten=='pdf'||$exten=='PDF') {
                    $name =  $nom.'_'.$prenom.'_doc_justicatif_stage_ouvrier_'.$date.'.'.$exten;
                    $path_local = 'stageouvriers/'.$stageouvrier->created_at.'/'.$prenom.$nom;
                    $file->move($path_local, $name);
                    $stageouvrier->doc_justicatif=$path_local.'/'.$name;
                    $stageouvrier->save();
                }
            }
        }
        if(Input::get('EstEtranger')=="Oui ") {
            $stageouvrier->estEtranger=true;
        }
        else
            $stageouvrier->estEtranger=false;
        $stageouvrier->infos_stage_ouvrier=$this->createConvention(new EtudiantConventionRequest(),$template_ouvrier->position,'ouvrier');
        $stageouvrier->save();

        $files=array();
        array_push($files,$stageouvrier->infos_stage_ouvrier);
        array_push($files,$stageouvrier->doc_justicatif);

        if($stageouvrier){
            //email pour responsable
            if($responsables-> first() != null)
                foreach($responsables as $responsable){
                    $array['nom']=$responsable->nom.' '.$responsable->prenom;
                    $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                        $responsable->nom.' '.$responsable->prenom,$responsable->user->email,$files,
                        'emails.convention_responsable',$array,
                        '[Etablisation de convention sur AdopteUnStage]');
                }
            return View::make('etudiant.reussite_stage_ouvrier');
        }
        return Redirect::back()->with('flash_error', 'Echoué de création de la convention!!!');
    }



    /**
     * creer un doc selon les inputs avec le template indiqué
     * remplacer tous les champs avec les inputs
     * @param $template
     * @param $lang
     * @return string
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    private function createConvention(EtudiantConventionRequest $etudiantConventionRequest,$template,$lang){
        $phpword=New PhpWord();
        $convention=$phpword->loadTemplate($template);
        $date=Carbon::now()->toDateTimeString();

        $genreRes=Input::get('GenreEnsResp');
        $genreEncad=Input::get('GenreEncadreur');
        $genreEtu=Input::get('GenreEtu');
        $genreDirEnt=Input::get('GenreDirEnt');

        $dateDebut=Input::get('DateDebut');
        $dateFin=Input::get('DateFin');

        $dateNaissEtu=Input::get('DateNaissEtu');
        if($lang!='fr'){
            //transfert les genre français vers l'anglais'
            $convention->setValue('GenreEnsResp_EN',$this->transfertFrToEnGenre($genreRes));
            $convention->setValue('GenreDirEnt_EN',$this->transfertFrToEnGenre($genreDirEnt));
            $convention->setValue('GenreEtu_EN',$this->transfertFrToEnGenre($genreEtu));
            $convention->setValue('GenreEncadreur_EN',$this->transfertFrToEnGenre($genreEncad));

            $convention->setValue('DateDebut_EN',$this->translateDate($dateDebut,'en'));
            $convention->setValue('DateFin_EN',$this->translateDate($dateFin,'en'));

            $convention->setValue('DateNaissEtu_EN',$this->translateDate($dateNaissEtu,'en'));
        }

        //Informations sur l'entreprise
        $convention->setValue('NomEntreprise',Input::get('NomEntreprise'));
        $convention->setValue('TelDirEnt',Input::get('TelDirEnt'));
        $adrSS=PHP_EOL.Input::get('AdrSiegeSocial') .PHP_EOL.  Input::get('cp_ville_SS') .PHP_EOL.  Input::get('pays_ss');
        $convention->setValue('AdrSiegeSocial',$adrSS);
        $convention->setValue('EmailDirEnt',Input::get('EmailDirEnt'));
        $convention->setValue('GenreDirEnt',$genreDirEnt);
        $convention->setValue('PrenomDirEnt',Input::get('PrenomDirEnt'));
        $convention->setValue('NomDirEnt',Input::get('NomDirEnt'));
        $convention->setValue('QualiteDirEnt',Input::get('QualiteDirEnt'));
        $adrStage=Input::get('Adresse1Stage')+Input::get('CPStage')+Input::get('VilleStage')+Input::get('pays');
        $convention->setValue('AdrStage',$adrStage);

        //informations sur l'étudiant
        $etudiant_nom=Input::get('NomEtu');
        $etudiant_prenom=Input::get('PrenomEtu');
        $convention->setValue('GenreEtu',$genreEtu);
        $convention->setValue('PrenomEtu',$etudiant_prenom);
        $convention->setValue('NomEtu',$etudiant_nom);
        $adressEtu=PHP_EOL.Input::get('AdressEtu') .PHP_EOL. Input::get('cp_ville_Etu') .PHP_EOL. Input::get('pays_etudiant');
        $convention->setValue('DomEtu',$adressEtu);
        $convention->setValue('TelEtu',Input::get('TelEtu'));
        $convention->setValue('DateNaissEtu',$dateNaissEtu);
        $convention->setValue('EmailEtu',Input::get('EmailEtu'));
        $convention->setValue('NumEtu',Input::get('NumEtu'));
        $convention->setValue('NumSS',Input::get('NumSS'));
        $convention->setValue('Filiere',Input::get('Filiere'));

        //stage
        $convention->setValue('TitreStage',Input::get('TitreStage'));
        $convention->setValue('DateDebut',$this->translateDate($dateDebut,'fr'));
        $convention->setValue('DateFin',$this->translateDate($dateFin,'fr'));
        $convention->setValue('DureeStageJour',$this->calculJoursEntreDates($dateFin,$dateDebut));
        $convention->setValue('DureeStageSemaine',intval($this->calculJoursEntreDates($dateFin,$dateDebut)/7));

        $convention->setValue('DureeHebdo',Input::get('DureeHebdo'));
        $convention->setValue('HoraireSpecif',Input::get('HoraireSpecif'));
        $convention->setValue('FerieEventuel',Input::get('FerieEventuel'));
        $convention->setValue('DescrStage',Input::get('DescrStage'));
        $convention->setValue('Assurance',Input::get('Assurance'));
        $convention->setValue('AssuranceNo',Input::get('AssuranceNo'));

        $convention->setValue('MontantGrat',Input::get('MontantGrat'));
        $convention->setValue('Devise',Input::get('Devise'));
        $convention->setValue('ModaliteGrat',Input::get('ModaliteGrat'));
        $convention->setValue('AvantagesGrat',Input::get('AvantagesGrat'));


        //informations sur le responsable
        $convention->setValue('GenreEnsResp',$genreRes);
        $convention->setValue('NomEnsResp',Input::get('NomEnsResp'));
        $convention->setValue('PrenomEnsResp',Input::get('PrenomEnsResp'));
        $convention->setValue('TelEnsResp',Input::get('TelEnsResp'));
        $convention->setValue('EmailEnsResp',Input::get('EmailEnsResp'));

        //informations sur le cadreur
        $convention->setValue('GenreEncadreur',$genreEncad);
        $convention->setValue('NomEncadreur',Input::get('NomEncadreur'));
        $convention->setValue('PrenomEncadreur',Input::get('PrenomEncadreur'));
        $convention->setValue('FctEncadreur',Input::get('FctEncadreur'));
        $convention->setValue('TelEncadreur',Input::get('TelEncadreur'));
        $convention->setValue('EmailEncadreur',Input::get('EmailEncadreur'));

        $convention->setValue('EstEtranger',Input::get('EstEtranger'));
        $convention->setValue('EstLabo',Input::get('EstLabo'));

        if($lang=='ouvrier'){
            $path='stageouvriers/'.$etudiant_prenom.$etudiant_nom;
            if (!file_exists($path)) {
                File::makeDirectory($path, 0777, true,true);
            }
            $file_convention=$path.'/'.$etudiant_nom.'_'.$etudiant_prenom.'_Stage_Ouvrier_'.$date.'.docx';
            $convention->saveAs($file_convention);
            return $file_convention;
        }

        $path='conventions/'.$etudiant_prenom.$etudiant_nom;
        if (!file_exists($path)) {
            File::makeDirectory($path, 0777, true,true);
        }
        $file_convention=$path.'/'.$etudiant_nom.'_'.$etudiant_prenom.'_Convention_'.$lang.'_'.$date.'.docx';
        $convention->saveAs($file_convention);
        return $file_convention;
    }



    /**
     * Trandure les genres francais en les genres anglais
     * @param $genreFr
     * @return string
     */
    private function transfertFrToEnGenre($genreFr){
        switch($genreFr){
            case 'M. ':
                return 'Mr.';
            case 'Mlle ':
                return 'Miss.';
            Case 'Mme ':
                return 'Ms.';
            default:
                return 'Mr.';
        }
    }

    /**
     * tranduire la date numérique en date normal, avec la langue diffrente
     * @param $dateFrNum
     * @param $lang
     * @return string
     */
    private function translateDate($dateFrNum,$lang)
    {
        if(trim($dateFrNum) != '') {
            $array = explode('/', $dateFrNum);
            $day = $array[0];
            $month = $array[1];
            $year = $array[2];
            if ($lang == 'fr') {
                switch ($month) {
                    case '01':
                        return $day . ' janvier ' . $year;
                    case '02':
                        return $day . ' février ' . $year;
                    case '03':
                        return $day . ' mars ' . $year;
                    case '04':
                        return $day . ' avril ' . $year;
                    case '05':
                        return $day . ' mai ' . $year;
                    case '06':
                        return $day . ' juin ' . $year;
                    case '07':
                        return $day . ' juillet ' . $year;
                    case '08':
                        return $day . ' août ' . $year;
                    case '09':
                        return $day . ' septembre ' . $year;
                    case '10':
                        return $day . ' octobre ' . $year;
                    case '11':
                        return $day . ' novembre ' . $year;
                    case '12':
                        return $day . ' décembre ' . $year;
                }
            } else {
                switch ($month) {
                    case '01':
                        return ' January ' . $day . 'th ' . $year;
                    case '02':
                        return ' February ' . $day . 'th ' . $year;
                    case '03':
                        return ' March ' . $day . 'th ' . $year;
                    case '04':
                        return ' April ' . $day . 'th ' . $year;
                    case '05':
                        return ' May ' . $day . 'th ' . $year;
                    case '06':
                        return ' June ' . $day . 'th ' . $year;
                    case '07':
                        return ' July ' . $day . 'th ' . $year;
                    case '08':
                        return ' August ' . $day . 'th ' . $year;
                    case '09':
                        return ' September ' . $day . 'th ' . $year;
                    case '10':
                        return ' October ' . $day . 'th ' . $year;
                    case '11':
                        return ' November ' . $day . 'th ' . $year;
                    case '12':
                        return ' December ' . $day . 'th ' . $year;
                }

            }
        }
    }

    /**
     * calculer le nombre de jours entre deux dates
     * @param $dateDebut
     * @param $dateFin
     * @return float
     */
    private function calculJoursEntreDates($dateDebut,$dateFin){
        $dateDebut_new= str_replace('/','.',$dateDebut);
        $dateFin_new=str_replace('/','.',$dateFin);
        $dureeSejour = (-1)*(strtotime($dateFin_new) - strtotime($dateDebut_new));
        return  $dureeSejour/86400+1;

    }

    /**
     * supprimer tous les variables de saisir
     */
    private function deleteInputVariables(){
        $variables=$this->variableRepository->findByUserCurrent();
        $user=$this->userRepository->getUserCurrect();
        foreach($variables as  $key=>$variable){
            Variable::whereKey($user->email.'+'.$key)->delete();
        }
    }
}