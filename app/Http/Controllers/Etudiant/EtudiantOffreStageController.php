<?php
namespace App\Http\Controllers\Etudiant;


use App\Competence;
use App\Departement;
use App\Http\Controllers\OffreStageController;
use App\Niveau;
use App\Repositories\OffreStageRepository;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Etudiant OffreStage Controller
|--------------------------------------------------------------------------
|
| Ce controller permet étudiant de rechercher des offres de stage
|
| copyright 2015 AdopteUnStage
*/
class EtudiantOffreStageController extends OffreStageController {

    protected $offreStageRepository;

    public function __construct(OffreStageRepository $offreStageRepository)
    {
        $this->offreStageRepository = $offreStageRepository;
    }

    /**
     * Retourne :
     *      -l'ensemble des stages(valide, non pourvu et avec competences)
     *      -l'ensemble des competences
     *      -l'ensemble des promotions avec specialites
     * le tout au format Json si requête Json sinon retourne accueil message erreur
     * @return View
     */
    public function index()
    {
        $array = parent::index();
        $array['competences']=Competence::all();
        $array['niveaux'] = Niveau::all();
        $array['departements'] = Departement::all();

        if (Request::wantsJson()) {
            return Response::json($array);
        } else {
            return  Redirect::refresh()->with('flash_error', 'Accès refusée!!!');
        }
    }


    /**
     * Afficher une offre de stage
     * @param $id
     */
    public function show($id)
    {
        $offre=$this->offreStageRepository->findbyId($id);
        $num = $offre->nombre_vu;
        $offre->nombre_vu=$num+1;
        $offre->save();

        $array = parent::show($id);
        $array['auteur']='etudiant';
        return View::make('etudiant.afficher_offre')->with($array);
    }

}