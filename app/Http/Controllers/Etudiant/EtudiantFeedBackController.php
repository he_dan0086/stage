<?php
namespace App\Http\Controllers\Etudiant;

use App\Feedback;
use App\Http\Controllers\Controller;
use App\Http\Requests\Etudiant\FeedBackEtudiantRequest;
use App\Repositories\EntrepriseRepository;
use App\Repositories\EtudiantRepository;
use App\Repositories\FeedbackRepository;
use App\Repositories\MailRepository;
use App\Repositories\ResponsableRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
/*
|--------------------------------------------------------------------------
| Etudiant Feedback Controller
|--------------------------------------------------------------------------
|
| Ce controller permet etudiant de gestion du feedback
|
| copyright 2015 AdopteUnStage
|
*/

class EtudiantFeedBackController extends Controller
{

    protected $userRepository, $etudiantRepository, $mailRepository,
        $responsableRepository, $entrepriseRepository ,$feedbackRepository ;

    /** constucteur avec l'inistalisation des respositories
     *
     * @param UserRepository $userRepository
     * @param EtudiantRepository $etudiantRepository
     * @param MailRepository $mailRepository
     * @param ResponsableRepository $responsableRepository
     * @param EntrepriseRepository $entrepriseRepository
     * @param FeedbackRepository $feedbackRepository
     */
    public function __construct(UserRepository $userRepository,
                                EtudiantRepository $etudiantRepository,
                                MailRepository $mailRepository,
                                ResponsableRepository $responsableRepository,
                                EntrepriseRepository $entrepriseRepository,
                                FeedbackRepository $feedbackRepository)
    {
        $this->userRepository=$userRepository;
        $this->etudiantRepository = $etudiantRepository;
        $this->mailRepository = $mailRepository;
        $this->responsableRepository = $responsableRepository;
        $this->entrepriseRepository = $entrepriseRepository;
        $this->feedbackRepository = $feedbackRepository;
    }


    /**
     * retour le page de angularJS pur rechercher un ffedback de l'étudiant
     *
     * @return mixed
     */
    public function index(){


        $array['feedbacks']=$this->feedbackRepository->findByValideWithCreateur(true);
        $array['entreprises']=$this->entrepriseRepository->all();

        if (Request::wantsJson()) {
            return Response::json($array);
        } else {
            return  Redirect::refresh()->with('flash_error', 'Accès refusée!!!');
        }
    }

    /**
     * afficher un feedback de l'étudiant
     *
     * @param $id
     * @return $this
     */
    public function show($id){
        $array['feedback']=$this->feedbackRepository->findById($id);

        $array['etudiant']=$this->etudiantRepository->findById($array['feedback']->created_by);
        $array['user'] = $this->userRepository->findByUserTypeEtId('App\Etudiant',$array['etudiant']->id);

        return view('etudiant.show_feedback')->with($array);
    }

    /**
     * liste tous les feedbacks crée par l'utilisateur authentifié
     *
     * @return \Illuminate\View\View
     */
    public function myList(){
        $feedbacks=$this->feedbackRepository->findByCreateur(Auth::user()->user->id);
        return view('etudiant.my_liste_feedbacks',compact('feedbacks'));
    }


    /**
     * retour la vu de modification du feedback
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id){
        $feedback = $this->feedbackRepository->findById($id);

        return view('etudiant.edit_feedback',compact('feedback'));
    }

    /**
     * poste la modifacation
     * @param $id
     * @return mixed
     */
    public function update($id){
        $inputs = Input::all();
        $feedback=$this->feedbackRepository->findById($id);
        if($feedback != null){
            $feedback->update([
                'entreprise' => $inputs['entreprise'],
                'descrption_entreprise' => $inputs['descrption_entreprise'],
                'contenu' => $inputs['contenu'],
                'recrutement_feedback' => $inputs['recrutement_feedback']
            ]);
            if(isset($inputs['isOuvert']))
                $feedback->isOuvert=true;
            else
                $feedback->isOuvert=false;
            $feedback->save();
            return Redirect::to(route('feedback-etudiant'))->with('flash_success', 'Les inofrmatins du feedback ont bien été modifiée');
        }
        return Redirect::refresh()->with('flash_error', 'Pas trouvé le feedback dans la base de donnée');
    }

    /**
     * retour la vue de la page de création du feedback
     * @return \Illuminate\View\View
     */
    public function create(){
        return view('etudiant.create_feedback');
    }

    /**
     * enregistrer le nouveau feedback
     *
     * @param FeedBackEtudiantRequest $feedBackEtudiantRequest
     * @return mixed
     */
    public function store(FeedBackEtudiantRequest $feedBackEtudiantRequest){
        $filiere_id=$this->userRepository->getRoleCurrent()->filiere_id;
        $responsables=$this->responsableRepository->findByFiliereId(($filiere_id));
        $array['etudiant']=$this->userRepository->getRoleCurrent();

        $inputs = Input::all();
        $feedback = new Feedback;
        if($feedback != null){
            if(isset($inputs['isOuvert'])){
                $feedback->isOuvert=true;
            }
            else
                $feedback->isOuvert=false;
            $feedback->entreprise=$inputs['entreprise'];
            $feedback->descrption_entreprise=$inputs['descrption_entreprise'];
            $feedback->contenu=$inputs['contenu'];
            $feedback->recrutement_feedback=$inputs['recrutement_feedback'];
            $feedback->created_by=$array['etudiant']->id;
            $feedback->valide=false;
            $feedback->save();
            $array['feedback']=$feedback;

            foreach($responsables as $responsable){
                $array['nom'] = $responsable->nom.' '.$responsable->prenom;
                $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                    $responsable->nom.' '.$responsable->prenom,$responsable->user->email,null,
                    'emails.feedback_responsable',$array,
                    '[Ajoute de feedback de l\'étudiant]');
            }

            return Redirect::to(route('feedback-etudiant'))->with('flash_success', 'Création du feedback succes');
        }
        return Redirect::refresh()->with('flash_error', 'Création du feedback échoué!!');
    }

    /**
     * suppression du feedback
     *
     * @param $id
     * @return mixed
     */
    public function delete($id){
        $feedback = $this->feedbackRepository->findById($id);
        if($feedback){
            $feedback->delete();
            return Redirect::to(route('feedback-etudiant'))->with('flash_success', 'suppression du feedback succes');
        }
        return Redirect::refresh()->with('flash_error', 'Suppréssion du feedback échoué!! pas trouvé le feedback correspondant!!');

    }

}