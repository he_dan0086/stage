<?php

namespace App\Http\Controllers\Etudiant;


use App\Http\Controllers\Controller;
use App\Repositories\EtudiantRepository;
use App\Repositories\FiliereRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


/*
|--------------------------------------------------------------------------
| Etudiant Profil Controller
|--------------------------------------------------------------------------
|
| Ce controller permet étudiant de voir et modifier les informations
|
| copyright 2015 AdopteUnStage
*/

class EtudiantProfilController extends Controller
{

    protected $filiereRepository,$etudiantRepository;

    public function __construct(FiliereRepository $filiereRepository,
                                EtudiantRepository $etudiantRepository)
    {
        $this->filiereRepository = $filiereRepository;
        $this->etudiantRepository=$etudiantRepository;
    }

    public function profil(){
        $filieres= $this->filiereRepository->all();
        $user = $this->etudiantRepository->findById(Auth::user()->user_id);
        $filiere_etu=$this->filiereRepository->findById($user->filiere_id);
        return view('etudiant.profil',compact('user','filiere_etu','filieres'));
    }

    public function postProfil(){
        $inputs=Input::all();
        $user = $this->etudiantRepository->findById(Auth::user()->user_id);
        $user->nom=$inputs['nom'];
        $user->prenom=$inputs['prenom'];
        $user->addresse=$inputs['adresse'];

        if(Input::get('Filiere')!=null)
            $user->filiere_id=$inputs['Filiere'];
        $user->telephone=$inputs['telephone'];
        $user->save();
        return Redirect::refresh()->with('flash_success', 'Les inofrmatins de l\'étudiant ont bien été modifiée');
    }
}