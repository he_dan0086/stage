<?php namespace App\Http\Controllers;


use Illuminate\Support\Facades\View;

class HomeController extends Controller{

    public function pageIndex(){
        return View::make('home');
    }
}