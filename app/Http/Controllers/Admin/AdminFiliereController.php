<?php

namespace App\Http\Controllers\Admin;


use App\Filiere;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


/*
|--------------------------------------------------------------------------
| Admin  Filiere  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des filieres(liste, ajouter, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminFiliereController extends Controller
{

    protected $filiere = null;


    public function __construct(Filiere $filiere)
    {
        $this->filiere = $filiere;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read()
    {
        $filieres =$this->filiere->all();
        return view('admin.filiere', compact('filieres'));
    }

    /**
     * création de filiere
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(){

        $filieres = Input::get('filieres');

        if($filieres != []){
            foreach($filieres as $n => $filiere){
                $f=new Filiere();
                $f->firstOrCreate(['filiere_nom' => $filiere]);
            }
            Session::flash('flash_success', 'Vos niveaux ont bien été créées');
        }
        else{
            Session::flash('flash_error', 'Aucun champs n\'a été envoyé');
        }
        return redirect(route('admin_filiere_get'));
    }

    /**
     * modification de filiere
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id){

        $filiere_update = Input::get('nom');
        $filiere = $this->filiere->find($id);

        if($filiere != null){
            $old_name = $filiere->filiere_nom;
            $filiere->update(['filiere_nom' => $filiere_update]);
            Session::flash('flash_success', 'Le departement "'.$old_name.'" a bien été modifiée');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucune departement ne correspond dans nos données.');
        }

        return redirect(route('admin_filiere_get'));

    }

    /**
     * suppression de filiere
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){

        $filiere = $this->filiere->find($id);

        if($filiere != null){
            $name = $filiere->filiere_nom;
            $this->filiere->destroy($id);
            Session::flash('flash_success', 'Le departement "'.$name.'" a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucune departement ne correspond dans nos données.');
        }

        return redirect(route('admin_filiere_get'));

    }

}