<?php namespace App\Http\Controllers\Admin;

use App\Etudiant;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\EtudiantRepository;
use App\Repositories\FiliereRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Admin  Etudiant  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des comptes d'etudiant (liste, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminEtudiantController extends Controller {

    protected $etudiant = null;
    protected $userRepository, $filiereRepository, $etudiantRepository;

    public function __construct(Etudiant $etudiant ,
                                UserRepository $userRepository,
                                FiliereRepository $filiereRepository,
                                EtudiantRepository $etudiantRepository)
    {
        $this->etudiant = $etudiant;
        $this->userRepository=$userRepository;
        $this->filiereRepository=$filiereRepository;
        $this->etudiantRepository = $etudiantRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read()
    {
        $filieres=$this->filiereRepository->all();
        $etudiants=$this->etudiantRepository->all();
        return view('admin.etudiants', compact('etudiants','filieres'));
    }


    /**
     * modification de l'etudiant
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id){
        $inputs=Input::all();
        $etudiant = $this->etudiant->find($id);
        $user=$this->userRepository->findByUserTypeEtId('App\Etudiant',$id);

        if($etudiant != null && $user != null){
            $etudiant->update([
                'nom' => $inputs['nom'],
                'prenom' => $inputs['prenom'],
                'filiere_id' => $inputs['Filiere']
            ]);
            $user->update([
                'email' => $inputs['email']
            ]);
            Session::flash('flash_success', 'L\'etudiant a bien été modifié en ['.$inputs['nom'].' '.$inputs['prenom'].' '.$inputs['email'].']');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun etudiant ne correspond dans nos données.');
        }

        $etudiants = $this->etudiant->all();
        return redirect(route('etudiant_get', compact('etudiants')));
    }

    /**
     * suppression de l'étudiant
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){

        $etudiant=$this->etudiant->find($id);
        $user=$this->userRepository->findByUserTypeEtId('App\Etudiant',$id);

        if($etudiant != null  && $user!=null){
            $name = $etudiant->nom.' '.$etudiant->prenom;
            if($etudiant->cv!=null)
                File::delete($etudiant->cv);
            $this->etudiant->destroy($id);
            User::destroy($user->id);
            Session::flash('flash_success', 'l\'etudiant "'.$name.'" a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun etudiant ne correspond dans nos données.');
        }

        $etudiants = $this->etudiant->all();
        return redirect(route('etudiant_get', compact('etudiants')));

    }

}
