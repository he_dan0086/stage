<?php namespace App\Http\Controllers\Admin;

use App\Departement;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


/*
|--------------------------------------------------------------------------
| Admin  Departement  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des departements (liste, ajouter, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/

class AdminDepartementController extends Controller {

    protected $departement = null;


    public function __construct(Departement $departement)
    {
        $this->departement = $departement;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read()
    {
        $departements = $this->departement->all();

        return view('admin.departement', compact('departements'));
    }

    /**
     * création de département
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(){

        $departements = Input::get('departements');

        if($departements != []){
            foreach($departements as $n => $departement){
                $this->departement->firstOrCreate(['nom' => $departement]);
            }
        }
        return redirect(route('admin_departement_get'));
    }

    /**
     * modification du département
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id){

        $nom = Input::get('nom');
        $departement = $this->departement->find($id);

        if($departement != null){
            $old_name = $departement->nom;
            $departement->update(['nom' => $nom]);
            Session::flash('flash_success', 'Le departement "'.$old_name.'" a bien été modifiée');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucune departement ne correspond dans nos données.');
        }

        return redirect(route('admin_departement_get'));
    }

    /**
     * suppression du département
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){

        $departement = $this->departement->find($id);

        if($departement != null){
            $name = $departement->nom;
            $this->departement->destroy($id);
            Session::flash('flash_success', 'Le departement "'.$name.'" a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucune departement ne correspond dans nos données.');
        }

        return redirect(route('admin_departement_get'));

    }
}
