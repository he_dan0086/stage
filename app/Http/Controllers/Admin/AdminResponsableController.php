<?php namespace App\Http\Controllers\Admin;

use App\Filiere;
use App\Repositories\FiliereRepository;
use App\Repositories\ResponsableRepository;
use App\Repositories\UserRepository;
use App\Responsable;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Admin  Responsable  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des responsables (liste, ajouter, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminResponsableController extends Controller {

    protected $responsable = null;
    protected $userRepository,$filiereRepository,$responsableRepository;

    public function __construct(Responsable $responsable,
                                UserRepository $userRepository,
                                FiliereRepository $filiereRepository,
                                ResponsableRepository $responsableRepository)
    {
        $this->responsable = $responsable;
        $this->userRepository=$userRepository;
        $this->filiereRepository = $filiereRepository;
        $this->responsableRepository = $responsableRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read()
    {
        $filieres=$this->filiereRepository->all();
        $responsables=$this->responsableRepository->allWithFiliere();

        return view('admin.responsables', compact('responsables','filieres'));
    }

    /**
     * création de responsables
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(){
        $filieres=Input::get('Filiere');
        $noms = Input::get('nom');
        $prenoms = Input::get('prenom');
        $emails = Input::get('email');
        $role = Role::whereNom('responsable')->first();

        if($noms != [] && $role!=null) {
            foreach ($noms as $n => $nom) {
                $dpt = new Responsable;
                $dpt->nom = $nom;
                $dpt->prenom = $prenoms[$n];
                $dpt->filiere_id=$filieres[$n];

                $user = new User;
                $user->email =$emails[$n];
                $user->password = Hash::make('azerty');
                $user->role_id = $role->id;
                $user->valide = 1;
                $user->user_type = 'App\Responsable';

                DB::transaction(function () use ($user, $dpt) {
                    $dpt->save();
                    $user->user_id = $dpt->id;
                    $user->save();
                });
                
            }
            Session::flash('flash_success', 'Vous avez bien créé les responsables');
        }
        else{
            Session::flash('flash_error', 'Aucun champs n\'a été envoyé');
        }
        $responsables= $this->responsable->all();
        return redirect(route('responsable_get', compact('responsables')));
    }

    /**
     * modification de responsable
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id){
        $inputs=Input::all();
        $responsable = $this->responsable->find($id);
        $user=$this->userRepository->findByUserTypeEtId('App\Responsable',$id);

        if($responsable != null  && $user!=null){
            $old_name = $responsable->nom.' '.$responsable->prenom;
            $responsable->update([
                'nom' => $inputs['nom'],
                'prenom' => $inputs['prenom'],
                'filiere_id' => $inputs['Filiere']
            ]);
            $user->update([
                'email' => $inputs['email']
            ]);

            Session::flash('flash_success', 'Le responsable ['.$old_name.'] a bien été renommée en ['.$inputs['nom'].' '.$inputs['prenom'].' '.$inputs['email'].']');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun responsable ne correspond dans nos données.');
        }

        $responsables = $this->responsable->all();
        return redirect(route('responsable_get', compact('responsables')));
    }

    /**
     * supression de responsable
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){

        $responsable = $this->responsable->find($id);
        $user= $this->userRepository->findByUserTypeEtId('App\Responsable',$id);

        if($responsable != null && $user != null){
            $this->responsable->destroy($id);
            User::destroy($user->id);
            Session::flash('flash_success', 'Le responsable a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun responsable ne correspond dans nos données.');
        }

        $responsables = $this->responsable->all();
        return redirect(route('responsable_get', compact('responsables')));

    }

}
