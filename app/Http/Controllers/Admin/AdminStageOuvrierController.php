<?php
namespace App\Http\Controllers\Admin;

use App\StageOuvrier;
use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;


/*
|--------------------------------------------------------------------------
| Admin StageOuvrier Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des stages ouvriers(liste, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminStageOuvrierController extends Controller {


    /**
     * Affiche la liste de l'ensemble des conventions de l'étudiant
     *
     * @return Response
     */
    public function index()
    {
        $stageouvriers=StageOuvrier::orderBy('created_at','desc')->orderBy('filiere')->get();
        return View::make('admin.liste_stage_ouvrier',compact('stageouvriers'));

    }

    /**
     * Afficher une conventions
     * @param $id
     * @param $lang
     * @return null
     */
    public function afficher($id,$lang)
    {
        $stageouvrier=StageOuvrier::find($id);
        switch($lang){
            case 'infos_stage_ouvrier':
                return Response::download($stageouvrier->infos_stage_ouvrier);
            case 'doc_justicatif':
                return Response::download($stageouvrier->doc_justicatif);
            default:
                return null;
        }
    }

    /**
     * Update conventions en BDD
     * @param  int  $id id de conventions
     *
     * @return Response
     */
    public function update($id,$lang)
    {
        $stage=StageOuvrier::find($id);
        $date=Carbon::now()->toDateTimeString();
        $file_upload=Input::file('nouveau_conv');
        //supprimer l'anncien.
        File::delete($stage->$lang);

        if($stage) {
            if ($file_upload != null) {
                if ($file_upload->isValid()) {
                    $exten = $file_upload->getClientOriginalExtension();
                    if ($exten == 'doc' || $exten == 'docx' || $exten == 'odt' || $exten == 'pdf' || $exten == 'PDF') {
                        $file = $stage->nom . '_' . $stage->prenom . '_' . $lang . '_' . $date . '.' . $exten;
                        $path_local = 'stageouvriers/'.$stage->prenom . $stage->nom . '/';
                        $file_upload->move($path_local, $file);
                        $stage->$lang = $path_local . $file;
                        $stage->save();
                        return Redirect::back()->with('flash_success', 'upload nouveau fichier ' . $lang . ' succes!!!');
                    }
                }
            }
        }
    }


    /**
     * Supprimer une conventions
     *
     * @param  int  $id id conventions
     * @return Response
     */
    public function destroy($id,$lang)
    {
        $stage=StageOuvrier::whereId($id)->first();
        if($stage!=NULL && $lang=='stageouvrier'){
            if($stage->infos_stage_ouvrier)
                File::delete($stage->infos_stage_ouvrier);
            if($stage->doc_justicatif)
                File::delete($stage->doc_justicatif);
            $stage->delete();
            return  Redirect::back()->with('flash_success', 'Supprimé Stage ouvrier succes!!!');
        }
        if($stage){
            File::delete($stage->$lang);
            $stage->$lang=null;
            $stage->save();
            return  Redirect::back()->with('flash_success', 'Supprimé stage_ouvrier'.$lang.' succes!!!');
        }
        return  Redirect::back()->with('flash_error', 'Supprimé stage_ouvrier'.$lang.' Echoué!!!');
    }
}