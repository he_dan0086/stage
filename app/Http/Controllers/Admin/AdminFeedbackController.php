<?php

namespace App\Http\Controllers\Admin;


use App\Repositories\FeedbackRepository;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AdminFeedbackController extends Controller
{
    protected $feedbackRepository;

    public function __construct(FeedbackRepository $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }


    public function index(){
        $feedbacks=$this->feedbackRepository->all();
        return view('admin.liste_feedback',compact('feedbacks'));
    }

    /**
     * poste la modifacation
     * @param $id
     * @return mixed
     */
    public function update($id){
        $inputs = Input::all();
        $feedback=$this->feedbackRepository->findById($id);
        if($feedback != null){
            $feedback->update([
                'entreprise' => $inputs['entreprise'],
                'descrption_entreprise' => $inputs['descrption_entreprise'],
                'contenu' => $inputs['contenu'],
                'recrutement_feedback' => $inputs['recrutement_feedback']
            ]);
            if(isset($inputs['isOuvert']))
                $feedback->isOuvert=true;
            else
                $feedback->isOuvert=false;
            $feedback->save();
            return Redirect::to(route('admin_gestion_feedback_get'))->with('flash_success', 'Les inofrmatins du feedback ont bien été modifiée');
        }
        return Redirect::refresh()->with('flash_error', 'Pas trouvé le feedback dans la base de donnée');
    }

    public function destroy($id){
        $feedback = $this->feedbackRepository->findById($id);
        if($feedback){
            $feedback->delete();
            return Redirect::to(route('admin_gestion_feedback_get'))->with('flash_success', 'suppression du feedback succes');
        }
        return Redirect::refresh()->with('flash_error', 'Suppréssion du feedback échoué!! pas trouvé le feedback correspondant!!');

    }

}