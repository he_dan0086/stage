<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Repositories\TemplateRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Admin  Template  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des templates de downloads (telecharger, upload)
|
|
| copyright 2015 AdopteUnStage
|
*/
class AdminTemplateController extends Controller
{
    protected $templateRepository;

    public function __construct(TemplateRepository $templateRepository )
    {
        $this->templateRepository = $templateRepository;

    }

    /**
     * get le page de modification de templates
     * @return mixed
     */
    public function getUploadConv(){
        $templates=$this->templateRepository->allDownloads();
        return View::make('admin.downloads',compact('templates'));
    }

    /**
     * upload un nouveau template selons le langage choisi
     * @param $lang
     * @return mixed
     */
    public function postUploadConv($lang){
        $download=$this->templateRepository->findDownloadByLang($lang);
        if($download->position != null)
            File::delete($download->position);
        $template=Input::file('conv_'.$lang);
        if($template!=null) {
            if ($template->isValid()) {
                $exten = $template->getClientOriginalExtension();
                if ( $exten == 'doc' || $exten == 'docx' || $exten == 'odt') {
                    if($lang=='ouvrier'){
                        $file =  'template_stage_ouvrier.'. $exten;
                        $path_local = 'templates/';
                        $template->move($path_local, $file);
                        $download->position= $path_local.'/'.$file;
                        $download->save();
                        return  Redirect::back()->with('flash_success', 'upload template du stage ouvrier succes!!!');
                    }
                    if($lang=='offre'){
                        $file =  'template_offre_stage.'. $exten;
                        $path_local = 'templates/';
                        $template->move($path_local, $file);
                        $download->position= $path_local.'/'.$file;
                        $download->save();
                        return  Redirect::back()->with('flash_success', 'upload template de l\'offre de stage succes!!!');
                    }
                }
            }
        }

        return  Redirect::back()->with('flash_error', 'upload template de la version '.$lang.' échoué!!!!!!');
    }

    /**
     * download un template selon le langage choisi
     * @param $lang
     * @return mixed
     */
    public function downloadTempl($lang){
        $template=$this->templateRepository->findDownloadByLang($lang);
        if($template != null)
            return Response::download($template->position);
    }

}