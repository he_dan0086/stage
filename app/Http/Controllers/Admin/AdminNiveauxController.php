<?php namespace App\Http\Controllers\Admin;

use App\Niveau;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Admin  Niveau  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des niveaux(liste, ajouter, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminNiveauxController extends Controller {

    protected $niveaux = null;

    public function __construct(Niveau $niveaux)
    {
        $this->niveaux = $niveaux;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function read()
	{
        $niveaux = $this->niveaux->all();
		return view('admin.niveaux', compact('niveaux'));
	}

    /**
     * création du nieavu
     * @param Input $input
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Input $input){
        $niveaux = $input->get('niveaux');

        if($niveaux != []) {
            foreach ($niveaux as $n => $niveau) {
                $dpt = new Niveau();
                $dpt->firstOrCreate(['nom' => Str::upper($niveau)]);
            }
            Session::flash('flash_success', 'Vos niveaux ont bien été créées');

        }
        else{
            Session::flash('flash_error', 'Aucun champs n\'a été envoyé');
        }

        $niveaux = $this->niveaux;
        return redirect(route('admin_niveaux_get', compact('niveaux')));
    }

    /***modification du niveau
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id){

        $nom = Input::get('nom');
        $niveau = $this->niveaux->find($id);

        if($niveau != null){
            $old_name = $niveau->nom;
            $niveau->update(['nom' => $nom]);
            Session::flash('flash_success', 'La promotion "'.$old_name.'" a bien été renommée en "'.$nom.'"');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun niveau ne correspond dans nos données.');
        }


        $niveaux = $this->niveaux->all();
        return redirect(route('admin_niveaux_get', compact('niveaux')));
    }

    /**
     * supression du niveau
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id){

        $niveau = $this->niveaux->find($id);

        if($niveau != null){
            $name = $niveau->nom;
            $this->niveaux->destroy($id);
            Session::flash('flash_success', 'La promotion "'.$name.'" a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun niveau ne correspond dans nos données.');
        }

        $niveaux = $this->niveaux->all();
        return redirect(route('admin_niveaux_get', compact('niveaux')));

    }

}
