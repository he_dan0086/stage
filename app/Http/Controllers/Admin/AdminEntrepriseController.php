<?php namespace App\Http\Controllers\Admin;

use App\Entreprise;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\EntrepriseRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class AdminEntrepriseController extends Controller {

    protected $entreprise = null;
    protected $userRepository,$entrepriseRepository;

    public function __construct(Entreprise $entreprise, UserRepository $userRepository,
                                EntrepriseRepository $entrepriseRepository)
    {
        $this->entreprise = $entreprise;
        $this->userRepository = $userRepository;
        $this->entrepriseRepository = $entrepriseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function read()
    {
        $entreprises=$this->entrepriseRepository->all();
        return view('admin.entreprises', compact('entreprises'));
    }


    public function update($id){
        $inputs=Input::all();
        $entreprise = $this->entreprise->find($id);
        $user=$this->userRepository->findByUserTypeEtId('App\Entreprise',$id);

        if($entreprise != null && $user != null){
            $entreprise->update([
                'nom' => $inputs['nom']
            ]);
            $user->update([
                'email' => $inputs['email']
            ]);
            Session::flash('flash_success', 'L\'entreprise a bien été modifié en ['.$inputs['nom'].' '.$inputs['email'].']');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun entreprise ne correspond dans nos données.');
        }

        $entreprises = $this->entreprise->all();
        return redirect(route('entreprise_get', compact('entreprises')));
    }

    public function delete($id){

        $entreprise=$this->entreprise->find($id);
        $user=$this->userRepository->findByUserTypeEtId('App\Entreprise',$id);

        if($entreprise != null && $user!=null){
            $name = $entreprise->nom;
            $this->entreprise->destroy($id);
            User::destroy($user->id);
            Session::flash('flash_success', 'l\'entreprise "'.$name.'" a été supprimée.');
        }
        else{
            Session::flash('flash_error', 'Une erreur s\'est produite, aucun entreprise ne correspond dans nos données.');
        }

        $entreprises = $this->entreprise->all();
        return redirect(route('entreprise_get', compact('entreprises')));

    }

}
