<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\OffreStageController;
use App\Http\Requests\OffreStageEditRequest;
use App\Repositories\CompetenceRepository;
use App\Repositories\MailRepository;
use App\Repositories\OffreStageRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


/*
|--------------------------------------------------------------------------
| Admin  OffreStage  Controller
|--------------------------------------------------------------------------
|
| Ce controller permet la gestion des offres de stages(liste, ajouter, upload, delete)
|
| copyright 2015 AdopteUnStage
|
*/
class AdminOffreStageController extends OffreStageController {
    protected $offreStageRepository, $mailRepository;

    public function __construct(OffreStageRepository $offreStageRepository,
                                MailRepository $mailRepository)
    {
        $this->offreStageRepository = $offreStageRepository;
        $this->mailRepository = $mailRepository;
    }


    /**
     * Affiche la liste de l'ensemble des offres de stages du moderateur
     *
     * @return Response
     */
    public function index()
    {
        $array = parent::index();
        return view('admin.liste_offre_stage')->with($array);
    }


    /**
     * Afficher une offre de stage
     * @param $id
     */
    public function show($id)
    {
        $array = parent::show($id);
        $array['auteur']='admin';
        return View::make('admin.afficher_offre')->with($array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $array = parent::edit($id);
        return View::make('admin.edition_offre_stage', $array);
    }


    /**
     * Update l'offre de stage en BDD
     * @param  int  $id id de l'offre de stage
     *
     * @return Response
     */
    public function update(OffreStageEditRequest $request,$id, CompetenceRepository $competence)
    {
        if(parent::update($request, $id, $competence)) {
            return Redirect::to('administrateur/offre_stage')->with('flash_success', 'Modification de l\'offre de stage effectuée!');
        } else {
            return Redirect::back()->withInput()->withErrors('Update une offre de stage échoué!');
        }
    }


    /**
     * Supprimer une offre de stage
     *
     * @param  int  $id id offre de stage
     * @return Response
     */
    public function destroy($id)
    {
        if(parent::destroy($id)) {
            return Redirect::to('administrateur/offre_stage')->with('flash_success', 'Suppression de l\'offre de stage effectuée!');
        } else {
            return Redirect::to('administrateur/offre_stage')->with('flash_error', 'Erreur c\'est produite l\'offre de stage n\'a pu être effacée!');
        }
    }

    /**
     *  valider une offre de stage et envoyer un mail à l'entrerpise
     * @param $id
     * @return mixed
     */
    public function validerOffreStage($id){
        $offre=$this->offreStageRepository->findbyId($id);
        if($offre!=null){
            $offre->valide=true;
            $offre->save();
            $array['nom']= $offre->entreprise_nom;
            $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                $offre->entreprise_nom,$offre->email,null,
                'emails.confirmation_validation_offre_stage',$array,
                '[Validation de l\'offre de stage sur AdopteUnStage]');
        }
        return Redirect::to('administrateur/offre_stage')->with('flash_success', 'Validation de l\'offre de stage ['.$offre->titre. '] effectuée!');

    }

    /**
     * modification de fichier detail de l'offre de stage
     * @param $id
     * @return mixed
     */
    public function modifierFile($id){
        $offre = $this->offreStageRepository->findbyId($id);
        if($offre->ficher_detail != null)
            File::delete($offre->ficher_detail);

        $file = Input::file('ficher_detail');
        if($file){
            $resultat=$this->offreStageRepository->uploadFile($file,$offre);
            if($resultat)
                return Redirect::back()->with('flash_success', 'File upload effectuée!');
        }
        return Redirect::back()->withInput()->with('flash_error', 'Erreur c\'est produite, File n\a pas pu être upload!! ');

    }
}