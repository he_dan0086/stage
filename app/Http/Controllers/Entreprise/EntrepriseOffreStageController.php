<?php namespace App\Http\Controllers\Entreprise;

use App\Administrateur;
use App\Http\Controllers\OffreStageController;
use App\Repositories\CompetenceRepository;
use App\Repositories\MailRepository;
use App\Repositories\OffreStageRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\OffreStageRequest;

/*
|--------------------------------------------------------------------------
| Entreprise OffreStage Controller
|--------------------------------------------------------------------------
|
| Ce controller permet entreprise de créer une offre de stage
| et voir la liste de son entreprise
|
| copyright 2015 AdopteUnStage
|
*/
class EntrepriseOffreStageController extends OffreStageController {


    //Repository du modèle des offres de stages
    protected $offreStageRepository, $mailRepository, $userRepository;

    public function __construct(OffreStageRepository $offreStageRepository,
                                MailRepository $mailRepository,
                                UserRepository $userRepository )
    {
        $this->offreStageRepository = $offreStageRepository;
        $this->mailRepository = $mailRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * retour la liste des offres de stage publié par l'entreprise
     * @return mixed
     */
    public function index(){
        $entreprise=$this->userRepository->getRoleCurrent()->id;
        $array['offres'] = $this->offreStageRepository->findByCreator($entreprise);
        $array['entreprise']=$this->userRepository->getRoleCurrent()->nom;
        $array['valide_info'] = $this->offreStageRepository->all()->lists('valide', 'id');
        $array['candidat_info']['nb'] = $this->offreStageRepository->getNbCandidats();
        return View::make('entreprise.liste_offre_stage')->with($array);
    }

    /**
     * afficher la liste des offres de stage pour une entreprise
     * @param $entreprise
     * @return mixed
     */
    public function showMyList($entreprise){

        $array['offres'] = $this->offreStageRepository->findByEntrepriseNom($entreprise);
        $array['entreprise']=$entreprise;
        $array['valide_info'] = $this->offreStageRepository->all()->lists('valide', 'id');
        $array['candidat_info']['nb'] = $this->offreStageRepository->getNbCandidats();
        return View::make('entreprise.liste_offre_stage')->with($array);
    }

    /**
     * Affichage d'une offre de stage
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $array = parent::show($id);
        $array['auteur']='entreprise';
        return View::make('entreprise.afficher_offre')->with($array);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $array = parent::create();
        return View::make('entreprise.creation_offre_stage')->with($array);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(OffreStageRequest $request,OffreStageRepository $offreStageRepository, CompetenceRepository $competence)
	{
        $offres=parent::store($request,$offreStageRepository,$competence);
        $admins=Administrateur::with('user')->get();
        if ($offres) {
            $entreprise=null;
            foreach($offres as $offre){
                $files=array();
                array_push($files,$offre->ficher_detail);

                $entreprise=$offre->nom_entreprise;
                //  envoyer un email à l'entreprise'
                $array['nom']=$entreprise;
                $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                    $entreprise,$offre->email,$files,
                    'emails.publication_mail_entreprise',$array,
                    '[Publication de l\'offre de stage sur AdopteUnStage]');

                //email pour admin
                if($admins->first() != null){
                    foreach($admins as $admin)
                        $array['nom']=$admin->nom.' '.$admin->prenom;
                        $this->mailRepository->envoyerMail('AdopteUnStage',env('MAIL_USERNAME'),
                            $admin->nom.' '.$admin->prenom, $admin->user->email, $files,
                            'emails.publication_mail_responsable',$array,
                            '[Nouvelle piublication sur AdopteUnStage]');
                }
            }
            return Redirect::to('entreprise/offre-stage/'.$entreprise.'/'.md5($entreprise).'/list')
                ->with('flash_success', 'Offre de stage publié!'.PHP_EOL. 'Vouz aller recevoir un email de confirmation
                 de la publication sur AdopteUnStage,' .PHP_EOL.'et vous allez aussi recevoir un email après la validation de l\'administrateur');
        } else {
            return Redirect::back()->withInput()->withErrors('La publication à échoué!');
        }
	}



    /**
     * télécharger un fichier de template
     * @return mixed
     */
    public function getDownload(){
        $path=  public_path()."/templates/template_offre_stage.doc";
        return Response::download($path);
    }
}