<?php

namespace App\Http\Controllers\Entreprise;


use App\Http\Controllers\Controller;
use App\Repositories\EtudiantRepository;
use App\Repositories\FiliereRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Entreprise Etudiant Controller
|--------------------------------------------------------------------------
|
| Ce controller permet entreprise de voir la liste des étudiants,
| et le détail de l'étudaint
|
| copyright 2015 AdopteUnStage
|
*/
class EntrepriseEtudiantController extends Controller
{
    protected $etudiantRepository,$filiereRepository, $userRepository;

    public function __construct(EtudiantRepository $etudiantRepository,
                                FiliereRepository $filiereRepository,
                                UserRepository $userRepository)
    {
        $this->etudiantRepository=$etudiantRepository;
        $this->filiereRepository=$filiereRepository;
        $this->userRepository=$userRepository;
    }

    /**
     * obtenir la liste des étudiants
     * @return \Illuminate\View\View
     */
    public function getAllEtudiant(){
        $etudiants=$this->etudiantRepository->allWithFiliereCV();
        return view('entreprise.liste_etudiant',compact('etudiants'));
    }


    /**
     * afficher les informations de l'étudiant et son cv
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id){
        $user=$this->userRepository->findByUserTypeEtId('App\Etudiant',$id);
        $filiere=$this->filiereRepository->findById($user->user->filiere_id);
        if($user){
            return view('entreprise.show_etudiant',compact('user','filiere'));
        }
        return Redirect::back()->with('flash_error','Pas trouvé l\'étudiant');
    }

}