<?php

namespace App\Http\Controllers\Entreprise;


use App\Entreprise;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Entreprise Profil Controller
|--------------------------------------------------------------------------
|
| ce controller permet une entreprise authentifiée de modifier son profil
|
| copyright 2015 AdopteUnStage
|
*/

class EntrepriseProfilController extends Controller
{


    protected $userRespository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRespository = $userRepository;
    }

    public function profil(){
        $entreprise=Entreprise::whereId(Auth::user()->user_id)->first();
        $user = Auth::user();
        return view('entreprise.profil',compact('user','entreprise'));
    }

    public function postProfil(){
        $inputs=Input::all();
        $entreprise=Entreprise::whereId(Auth::user()->user_id)->first();

        $entreprise->nom=$inputs['nom'];
        $entreprise->contact_person=$inputs['contact'];
        $entreprise->save();


        return Redirect::refresh()->with('flash_success', 'Les informations de l\'entreprise ont bien été modifiée');
    }

}