<?php namespace App\Http\Controllers;

use App\Competence;
use App\Departement;
use App\Http\Requests\OffreStageEditRequest;
use App\Niveau;
use App\Repositories\CompetenceRepository;
use App\Repositories\OffreStageRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use App\OffreStage;
use App\Http\Requests\OffreStageRequest;
use Illuminate\Support\Facades\Session;
/*
|--------------------------------------------------------------------------
| OffreStageController Controller
|--------------------------------------------------------------------------
|
| Ce controller est un parent de controller, il permet de la gestion des offre de stage
|
| copyright 2015 AdopteUnStage
*/

class OffreStageController extends Controller {

    protected $offreStageRepository,$competenceRepository;

    public function __construct(OffreStageRepository $offreStageRepository,
                                CompetenceRepository $competenceRepository)
    {
        $this->offreStageRepository = $offreStageRepository;
        $this->competenceRepository = $competenceRepository;
    }


    public function index()
    {
        $array['offres'] = $this->offreStageRepository->all();
        $array['valide_info'] = $this->offreStageRepository->all()->lists('valide', 'id');
        return $array;
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $array['competences'] = Competence::all();
        $array['departements']=Departement::all();
        $array['niveaux'] = Niveau::all();
        return $array;
	}

    /**
     * Afficher une offre de stage
     * @param $id
     */
    public function show($id)
    {
        $array['offre'] = $this->offreStageRepository->findbyId($id);
        $niveau_id=DB::table('niveau_offre_stage')->where('offre_stage_id',$id)->lists('niveau_id');
        $array['offre']['niveaux'] = Niveau::whereIn('id',$niveau_id)->get()->all();
        $departements_id=DB::table('departement_offre_stage')->where('offre_stage_id',$id)->lists('departement_id');
        $array['offre']['departements'] = Departement::whereIn('id',$departements_id)->get()->all();

        return $array;
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(OffreStageRequest $request,OffreStageRepository $offreStageRepository,CompetenceRepository $competenceRes)
	{
        if(Auth::check())
            $created_by=Auth::user()->user->id;
        else
            $created_by = null;
        $nom_entreprise = Input::get('nom_entreprise');
		$code_postal = Input::get('code_postal');
		$ville =Input::get('ville');
        $pays =Input::get('pays');
        $email = Input::get('email');

        /**informations dur le stage**/
        $titres=Input::get('titre');
        $descriptions = Input::get('description');
		$gratifications = Input::get('gratification');
        $devises=Input::get('devise');
        $files = Input::file('ficher_detail');

        $offres=array();
        if($descriptions != []){
            foreach($descriptions as $n => $description){
                $competences=Input::get('competence_'.$n);
                $niveaux=Input::get('niveau_'.$n);
                $departements=Input::get('departement_'.$n);

                $offre=new OffreStage();
                $offre->nom_entreprise = $nom_entreprise;
                $offre->code_postal = $code_postal;
                $offre->ville = $ville;
                $offre->pays = $pays;
                $offre->email = $email;
                $offre->created_by=$created_by;

                $offre->titre=$titres[$n];
                $offre->description = $descriptions[$n];
                $offre->gratification = $gratifications[$n];
                $offre->devise = $devises[$n];
                $offre->valide=false;
                $file = $files[$n];
                //upload un fichier .pdf ou doc/docx
                $result=$offreStageRepository->uploadFile($file,$offre);


                if($result){
                    /**departements**/
                    $offreStageRepository->insertOffreDepartements($departements,$offre);
                    /**niveaux**/
                    $offreStageRepository->insertOffreNiveaux($niveaux,$offre);
                    /**competences**/
                    $competenceRes->saveMultipe($competences, $offre);

                    array_push($offres,$offre);
                }
                else{
                    Session::flash('flash_error', 'Fichier transmis echoué');
                }
            }
        }
        return $offres;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$array['offre'] = OffreStage::where('offre_stages.id', $id)->with('competences')->with('niveaux')->with('departements')->first();

        $array['competences']['mine'] = $array['offre']->competences->lists('id');
        $array['niveaux']['mine'] = $array['offre']->niveaux->lists('id');
        $array['departements']['mine'] = $array['offre']->departements->lists('id');

        $array['competences']['all'] = $this->competenceRepository->all();
        $array['niveaux']['all'] = Niveau::all();
        $array['departements']['all'] = Departement::all();

        return $array;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(OffreStageEditRequest $request, $id, CompetenceRepository $competence)
	{
		$offre = $this->offreStageRepository->findbyId($id);
        /**informations sur lentreprise*/
        $offre->nom_entreprise = Input::get('nom_entreprise');
        $offre->code_postal = Input::get('code_postal');
        $offre->ville =Input::get('ville');
        $offre->pays =Input::get('pays');
        $offre->email = Input::get('email');

        /**informations dur le stage**/
        $offre->titre=Input::get('titre');
        $offre->description = Input::get('description');
        $offre->gratification = Input::get('gratification');
        $offre->devise=Input::get('devise');
        $resultat=$offre->save();

        $competence->saveMultipe(Input::get('competence_0'), $offre);
        $this->offreStageRepository->insertOffreDepartements(Input::get('departement_0'), $offre);
        $this->offreStageRepository->insertOffreNiveaux(Input::get('niveau_0'), $offre);

        return $resultat;
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$offre = $this->offreStageRepository->findbyId($id);
        if($offre!=null){
            if($offre->ficher_detail != null)
                File::delete($offre->ficher_detail);
            return ($offre->delete());
        }
        return false;

	}
}