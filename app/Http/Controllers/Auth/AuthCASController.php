<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Subfission\Cas\Facades\Cas;

class AuthCASController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | authentification Controller( pas utilisé pour l'instant)
    |--------------------------------------------------------------------------
    |
    | Ce controller permet de connecter avec le server CAS
    |
    | copyright 2015 AdopteUnStage
    |
    */


    /**
     * connecter avec le server CAS
     */
    public function login(){
        if(Cas::authenticate())
            return Redirect::to('login/');
    }

    /**
     * Déconnecter avec le server CAS
     */
    public function getLogout()
    {
        Cas::logout();
        return Redirect::route('index')->with('flash_notice', 'Vous avez été correctement déconnecté.');
    }
}
