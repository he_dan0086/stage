<?php

namespace App\Http\Controllers\Auth;


use App\Administrateur;
use App\Entreprise;
use App\Etudiant;
use App\Http\Controllers\Controller;
use App\Http\Requests\InscriptionEntrepriseRequest;
use App\Http\Requests\RegisterRequest;
use App\Repositories\FiliereRepository;
use App\Responsable;
use App\Role;
use App\Token;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;


/*
|--------------------------------------------------------------------------
| Auth Controller
|--------------------------------------------------------------------------
|
| ce controller permet de l'authentification
|
| copyright 2015 AdopteUnStage
|
*/
class AuthController  extends Controller{

    protected $filiereRepository;

    public function __construct(FiliereRepository $filiereRepository)
    {
        $this->filiereRepository=$filiereRepository;
    }
    /**
     * Affiche le formulaire d'inscription
     *
     * @return View
     */
    public function getLogin()
    {
        return View::make('auth.login');
    }
    /**
     * Affiche le formulaire d'inscription
     *
     * @return View
     */
    public function getEtudiantInscription()
    {
        $filieres=$this->filiereRepository->all();
        return View::make('auth.register',compact('filieres'));
    }

    /**
     * @param RegisterRequest $request
     * @return string
     */
    public function postEtudiantInscription( RegisterRequest $request)
    {

        $user_type = 'App\Etudiant';
        $etudiant = new Etudiant;
        $etudiant->nom=Input::get('nom');
        $etudiant->prenom=Input::get('prenom');
        $etudiant->telephone=Input::get('telephone');
        $etudiant->addresse=Input::get('adresse');
        $etudiant->filiere_id=Input::get('Filiere');

        $email=Input::get('email');
        $role = Role::whereNom('etudiant')->first();
        //false,compte non validé! il faut activer le compte
        $user=$this->inscrire($user_type, $etudiant, $role->id, false);

        $this->envoyerLienActive('emails.active_inscription_compte', $user->id,Input::get('nom').' '.Input::get('prenom'), $email);
        return View::make('auth.inscription_to_active',compact('email'));
    }

    public function activerInscription($id,$token){
        if (!$token)
            return view('auth.echec_token');
        $t = Token::whereToken($token)->first();
        if (!$t)
            return view('auth.echec_token');

        $user=User::find($id);
        if($user){
            $user->valide=true;
            $user->save();
        }
        Token::whereToken($t->token)->delete();
        return Redirect::route('login_get')->with('flash_success', 'Félicitation! vous avez réussit de vous inscrire sur AdopteUstage !! vous pour loggé sur AdopteUnSTage');
    }


    /**
     * Affiche le formulaire d'inscription
     * @return mixed
     */
    public function getEntrepriseInscription()
    {
        return View::make('auth.entreprise_inscription');
    }

    public function postEntrepriseInscription(InscriptionEntrepriseRequest $request)
    {
        $user_type = 'App\Entreprise';
        $entreprise = new Entreprise;
        $entreprise->nom = Input::get('nom_entreprise');
        $entreprise->contact_person= Input::get('nom_contact');
        $email=Input::get('email');
        $role = Role::whereNom('entreprise')->first();
        $user=$this->inscrire($user_type, $entreprise, $role->id, false);

        $this->envoyerLienActive('emails.active_inscription_compte', $user->id,Input::get('nom_entreprise'), Input::get('email'));

        return View::make('auth.inscription_to_active',compact('email'));
    }



    /**
     * @param $user_type
     * @param $type
     * @param $role
     * @param $isValide
     * @return mixed
     */
    private function inscrire($user_type, $type, $role, $isValide)
    {

        $user = new User;
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->role_id = $role;
        if ($isValide) $user->valide = 1;
        $user->user_type = $user_type;

        DB::transaction(function () use ($user, $type) {
            $type->save();
            $user->user_id = $type->id;
            $user->save();
        });
        return $user;
    }


    /**
     * Traitement du formulaire de login
     *
     * @return Redirect
     */
    public function postLogin()
    {
        $email = Input::get('email');
        $passe = Input::get('password');
        $souvenir=Input::get('souvenir');

        return $this->login($email,$passe,$souvenir);
    }


    private function login($email,$password,$souvenir)
    {
        if (Auth::attempt(array('email' => $email, 'password' => $password, 'valide' => 1), $souvenir)) {

            $role = Auth::user()->role;
            Session::put('role_nom', $role->nom);
            Session::put('role_autorisation', $role->id);

            return $this->getAccueil();

        } else {
            return Redirect::refresh()->with('flash_error', 'Pseudo/mot de passe non correct ou mail non validé !')->withInput();
        }
    }
    /**
     * Effectue le logout
     *
     * @return Redirect
     */
    public function getLogout()
    {
        Auth::logout();
        return Redirect::route('index')->with('flash_notice', 'Vous avez été correctement déconnecté.');
    }

    /**
     * @retour le page selon le type de l'utilisateur
     *
     * @return string
     */
    public function getAccueil()
    {

        if (Auth::user()->user_type == get_class(new Etudiant())) {
            return Redirect::to('etudiant#/recherche-stage')->with('flash_success', 'Bonjour ' . Auth::user()->user->nom .Auth::user()->user->prenom. ' !');
        } elseif (Auth::user()->user_type == get_class(new Administrateur())) {
            return Redirect::route('accueil-administrateur')->with('flash_success', 'Bonjour ' .  Auth::user()->user->nom .Auth::user()->user->prenom. ' !');
        } elseif (Auth::user()->user_type == get_class(new Entreprise())) {
            return Redirect::route('offre-stage-entreprise')->with('flash_success', 'Bonjour ' . Auth::user()->user->nom . ' !');
        }elseif (Auth::user()->user_type == get_class(new Responsable())) {
            return Redirect::route('accueil-responsable')->with('flash_success', 'Bonjour ' . Auth::user()->user->nom .' '.Auth::user()->user->prenom. ' !');
        }

        return "Error 404";

    }

    /**
     * enoyer le lien pour activer le compte
     *
     * @return Redirect
     */
    private function envoyerLienActive($vue, $user_id,$nom, $email ) {
        $token = new Token;
        $token->token = str_random(30);
        $token->save();

        $data = array( 'token' => $token, 'nom' => $nom,'email'=> $email,'id'=>$user_id );

        Mail::send($vue, $data, function ($message) use ($nom, $email) {
            $message->from('no-reply@adopteunstage.fr')
                ->to($email, $nom)
                ->subject('Inscription adopteunstage');
        });
    }

}