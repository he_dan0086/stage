<?php
namespace App\Http\Controllers\Responsable;


use App\Http\Controllers\Controller;
use App\Repositories\EtudiantRepository;
use App\Repositories\FeedbackRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Redirect;


/*
|--------------------------------------------------------------------------
| Responsable Feedback Controller
|--------------------------------------------------------------------------
|
| Ce controller permet responsable de gestion de ses feedback (afficher,valider, invalider,supprimer)
|
| copyright 2015 AdopteUnStage
*/

class ResponsableFeedbackController extends Controller
{

    protected $userRepository, $etudiantRepository,$feedbackRepository;

    /**
     * constructeur
     *
     * @param UserRepository $userRepository
     * @param EtudiantRepository $etudiantRepository
     * @param FeedbackRepository $feedbackRepository
     */
    public function __construct(UserRepository $userRepository,
                                EtudiantRepository $etudiantRepository ,
                                FeedbackRepository $feedbackRepository)
    {
        $this->userRepository = $userRepository;
        $this->etudiantRepository = $etudiantRepository;
        $this->feedbackRepository = $feedbackRepository;

    }

    /**
     * afficher tous les feedbacks de meme filiere avec le responsable authentifié
     *
     * @return \Illuminate\View\View
     */
    public function getFeedbacks(){
        $responsable = $this->userRepository->getRoleCurrent();
        $etudiants_id=$this->etudiantRepository->listEtudiantIdByFiliereId($responsable->filiere_id);
        $feedbacks=$this->feedbackRepository->findWhereCreateurIn($etudiants_id);

        return view('responsable.liste_feedback',compact('feedbacks','responsable'));
    }

    /**
     * valider un feedback de l'etudiant pour le rendre visible par publique
     *
     * @param $id
     * @return mixed
     */
    public function valider($id){
        $feedback= $this->feedbackRepository->findById($id);
        if($feedback){
            $feedback->valide=true;
            $feedback->save();
        }
        return Redirect::back()->with('flash_success', 'Validation du feedback success!!!');

    }


    /**
     * suppression un feeedback
     *
     * @param $id
     * @return mixed
     */
    public function destroy($id){
        $feedback = $this->feedbackRepository->findById($id);
        if($feedback){
            $feedback->delete();
            return Redirect::to(route('responsable_feedback_get'))->with('flash_success', 'suppression du feedback succes');
        }
        return Redirect::refresh()->with('flash_error', 'Suppréssion du feedback échoué!! pas trouvé le feedback correspondant!!');

    }

}