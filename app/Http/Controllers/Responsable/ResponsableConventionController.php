<?php
namespace App\Http\Controllers\Responsable;

use App\Http\Controllers\Controller;
use App\Repositories\ConventionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;


/*
|--------------------------------------------------------------------------
| Responsable Convention Controller
|--------------------------------------------------------------------------
|
| Ce controller permet responsable de voir et modifier les conventions de sa filiere
|
| copyright 2015 AdopteUnStage
*/

class ResponsableConventionController extends Controller
{

    protected $conventionRepository;

    public function __construct(ConventionRepository $conventionRepository)
    {
        $this->conventionRepository=$conventionRepository;

    }

    /**
     * Affiche la liste de l'ensemble des conventions de l'étudiant
     *
     * @return Response
     */
    public function index()
    {
        $filiere_id=Auth::user()->user->filiere_id;
        $conventions=$this->conventionRepository->findByFiliere($filiere_id);
        return View::make('responsable.liste_convention',compact('conventions'));

    }


    /**
     * Afficher une conventions(téléchercher le fichier)
     * @param $id
     */
    public function afficher($id,$lang)
    {
        $conv=$this->conventionRepository->findById($id);
        switch($lang){
            case 'en':
                return Response::download($conv->convention_en);
            case 'biling':
                return Response::download($conv->convention_biling);
            default:
                return Response::download($conv->convention_fr);
        }
    }

    /**
     * Update conventions(file) en BDD
     * @param $id int id de convention
     * @param $lang: en, fr, biling(language de convention)
     * @return mixed
     */
    public function update($id,$lang)
    {
        $conv=$this->conventionRepository->findById($id);
        $date=Carbon::now()->toDateString();
        $file_upload=Input::file('nouveau_conv');
        $conv_lang='convention_'.$lang;
        File::delete($conv->$conv_lang);

        if($file_upload!=null) {
            if ($file_upload->isValid()) {
                $exten = $file_upload->getClientOriginalExtension();
                if ( $exten == 'doc' || $exten == 'docx' || $exten == 'odt' ||$exten=='pdf' || $exten=='PDF') {
                    $file =  $conv->nom.'_'.$conv->prenom.'_'.$lang.'_'.$date.'.'.$exten;
                    $path_local = 'conventions/'.$conv->prenom.$conv->nom.'/';
                    $file_upload->move($path_local, $file);
                    $conv->$conv_lang=$path_local.$file;
                    $conv->save();
                    return  Redirect::back()->with('flash_success', 'upload template de la version '.$lang.' succes!!!');
                }
            }
        }
    }

    /** delete une convention
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function destroy($id,$lang)
    {
        $conv=$this->conventionRepository->findById($id);
        if($lang=='conv'){
            if($conv != null){
                if($conv->convention_en!=null)
                    File::delete($conv->convention_en);
                if($conv->convention_fr!=null)
                    File::delete($conv->convention_fr);
                if($conv->convention_biling!=null)
                    File::delete($conv->convention_biling);
                $conv->delete();
                return  Redirect::back()->with('flash_success', 'Supprimé convention succes!!!');
            }
            return  Redirect::back()->with('flash_error', 'Supprimé convention Echoué!!!');
        }

        if($conv){
            $conv_lang='convention_'.$lang;
            File::delete($conv->$conv_lang);
            $conv->$conv_lang=null;
            $conv->save();
            return  Redirect::back()->with('flash_success', 'Supprimé convention_'.$lang.' succes!!!');
        }
        return  Redirect::back()->with('flash_error', 'Supprimé convention_'.$lang.' Echoué!!!');

    }

}