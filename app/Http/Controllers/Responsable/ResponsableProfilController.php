<?php

namespace App\Http\Controllers\Responsable;


use App\Http\Controllers\Controller;
use App\Repositories\FiliereRepository;
use App\Repositories\ResponsableRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Responsable Profil Controller
|--------------------------------------------------------------------------
|
| Ce controller permet responsable de voir et modifier les informations
|
| copyright 2015 AdopteUnStage
*/

class ResponsableProfilController extends Controller
{
    protected $filiereRepository,$responsableRepository,$userRepository;

    public function __construct(FiliereRepository $filiereRepository,
                                ResponsableRepository $responsableRepository,
                                UserRepository $userRepository)
    {
        $this->filiereRepository = $filiereRepository;
        $this->responsableRepository=$responsableRepository;
        $this->userRepository=$userRepository;

    }

    /**
     * la vue de profil du responsable
     *
     * @return \Illuminate\View\View
     */
    public function index(){
        $user=$this->userRepository->getUserCurrect();
        $filiere_res=$this->filiereRepository->findById($user->user->filiere_id);
        $filieres=$this->filiereRepository->all();
        return view('responsable.profil',compact('filieres','user','filiere_res'));
    }

    /**
     * modifier les informations de responsable
     * @return mixed
     */
    public function postProfil(){
        $inputs=Input::all();
        $user = $this->responsableRepository->findById($this->userRepository->getUserCurrect()->user_id);
        $user->nom=$inputs['nom'];
        $user->prenom=$inputs['prenom'];

        if(Input::get('Filiere')!=null)
            $user->filiere_id=$inputs['Filiere'];
        $user->save();
        return Redirect::refresh()->with('flash_success', 'Les inofrmatins du responsable ont bien été modifiée');
    }

}