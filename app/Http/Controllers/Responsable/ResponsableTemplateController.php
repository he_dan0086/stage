<?php

namespace App\Http\Controllers\Responsable;


use App\Http\Controllers\Controller;
use App\Repositories\FiliereRepository;
use App\Repositories\TemplateRepository;
use App\Template;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;


/*
|--------------------------------------------------------------------------
| Responsable Template Controller
|--------------------------------------------------------------------------
|
| Ce controller permet responsable de gestion de ses templates de convention de sa filiere
|
| copyright 2015 AdopteUnStage
*/

class ResponsableTemplateController extends Controller
{

    protected $templateRepository, $filiereRepository;

    public function __construct(TemplateRepository $templateRepository,
                                FiliereRepository $filiereRepository)
    {
        $this->templateRepository=$templateRepository;
        $this->filiereRepository = $filiereRepository;

    }

    /**
     * afficher tous les templates existants
     * @return \Illuminate\View\View
     */
    public function getTemplates(){
        $filiere = $this->filiereRepository->findById(Auth::user()->user->filiere_id);
        if($filiere != null){
            $template=$this->templateRepository->findTemplateByFiliere($filiere->filiere_nom);
            return view('responsable.templates',compact('template'));
        }
    }

    /**
     * download un template
     *
     * @param $file
     * @return mixed
     */
    public function downloadTempl($lang){
        $filiere = $this->filiereRepository->findById(Auth::user()->user->filiere_id);
        if($filiere != null) {
            $template = $this->templateRepository->findTemplateByFiliere($filiere->filiere_nom);
            if($template != null){
                $file=$template->$lang;
                return Response::download($file);
            }

        }
    }


    /**
     * upload un nouveau template selons le langage choisi
     * @param $lang
     * @return mixed
     */
    public function postUploadConv($lang){
        $filiere = $this->filiereRepository->findById(Auth::user()->user->filiere_id);
        if($filiere != null){
            $template=$this->templateRepository->findTemplateByFiliere($filiere->filiere_nom);
            if($template != null && ($template->$lang != null))
                File::delete($template->$lang);
            else if($template == null){
                $template=new Template;
                $template->filiere=$filiere->filiere_nom;
            }
            $file=Input::file($lang);
            if($file!=null) {
                if ($file->isValid()) {
                    $exten = $file->getClientOriginalExtension();
                    if ( $exten == 'doc' || $exten == 'docx' || $exten == 'odt') {
                        $nom =  'convention_'.$lang.'.'. $exten;
                        $path_local = 'templates/'.$filiere->filiere_nom;
                        $file->move($path_local, $nom);
                        $template->$lang= $path_local.'/'.$nom;
                        $template->save();
                        return  Redirect::back()->with('flash_success', 'upload template succes!!!');
                    }
                }
            }
            return  Redirect::back()->with('flash_error', 'upload template de la version '.$lang.' échoué!!!!!!');
        }
    }


}