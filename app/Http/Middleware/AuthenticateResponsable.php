<?php

namespace App\Http\Middleware;


use App\Responsable;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class AuthenticateResponsable
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest())
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
//                return View::make('etudiant.page_non_autorise');
                return redirect()->back()->with('flash_error', 'Page non autorisée! Vous devez être un responsable!');
            }
        } else {
            if(!($this->auth->user()->user_type == get_class(new Responsable()))) {
                if ($request->ajax())
                {
                    return response('Unauthorized.', 401);
                }
                else
                {
                    return redirect()->back()->with('flash_error', 'Page non autorisée! Vous devez être un responsable!');
                }
            }
        }

        return $next($request);
    }

}
