<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Filiere extends Model
{
    protected $table = "filieres";


    public $timestamps = false;

    protected $fillable = array('filiere_nom');

    public function responsables(){
        return $this->hasMany('App\Responsable');
    }

    public function etudiants(){
        return $this->hasMany('App\Etudaint');
    }


}