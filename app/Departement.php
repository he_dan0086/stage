<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "departements";
    protected $fillable = ['nom'];

    public $timestamps = false;


    public function etudiants()
    {
        return $this->belongsToMany('App\Etudiant');
    }

    /**
     * Une promotions peut être liée à PLUSIEURS modérateurs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function moderateurs()
    {
        return $this->belongsToMany('App\Moderateur');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function offre_stages()
    {
        return $this->belongsToMany('App\OffreStage');
    }
}
