<?php namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider{


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\CompetenceRepository',
            'App\Repositories\Impl\CompetenceRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\ConventionRepository',
            'App\Repositories\Impl\ConventionRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\EtudiantRepository',
            'App\Repositories\Impl\EtudiantRepositoryImpl'
        );
        $this->app->bind(
            'App\Repositories\EntrepriseRepository',
            'App\Repositories\Impl\EntrepriseRepositoryImpl'
        );
        $this->app->bind(
            'App\Repositories\FeedbackRepository',
            'App\Repositories\Impl\FeedbackRepositoryImpl'
        );


        $this->app->bind(
            'App\Repositories\FiliereRepository',
            'App\Repositories\Impl\FiliereRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\MailRepository',
            'App\Repositories\Impl\MailRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\OffreStageRepository',
            'App\Repositories\Impl\OffreStageRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\ResponsableRepository',
            'App\Repositories\Impl\ResponsableRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\TemplateRepository',
            'App\Repositories\Impl\TemplateRepositoryImpl'
        );
        $this->app->bind(
            'App\Repositories\UserRepository',
            'App\Repositories\Impl\UserRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\VariableRepository',
            'App\Repositories\Impl\VariableRepositoryImpl'
        );



    }
}