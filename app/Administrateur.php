<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrateur extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = "administrateurs";

    public function user()
    {
        return $this->morphOne('App\User', 'user');
    }


}
