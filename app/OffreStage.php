<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OffreStage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "offre_stages";


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function niveaux()
    {
        return $this->belongsToMany('App\Niveau');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function departements()
    {
        return $this->belongsToMany('App\Departement');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function competences()
    {
        return $this->belongsToMany('App\Competence');
    }

    public function etudiants(){
        return $this->hasMany('App\Etudiant');
    }

}
