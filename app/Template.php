<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = "templates";

    protected $fillable = ['filiere','template_fr','template_en','template_biling'];

}