<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    protected $table="entreprises";

    protected $fillable = ['nom','contact_person'];

    public function user()
    {
        return $this->morphOne('App\User', 'user');
    }


}