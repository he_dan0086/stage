<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "niveaux";

    /**
     * @var array
     */
    protected $fillable = ['nom'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function etudiants()
    {
        return $this->belongsToMany('App\Etudiant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offre_stages()
    {
        return $this->belongsToMany('App\OffreStage');
    }

}
