<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class StageOuvrier extends Model
{
    protected $table = "stages_ouvriers";

    protected $fillable = ['nom','prenom','filiere','estEtranger','infos_stage_ouvrier','doc_justicatif'];


}