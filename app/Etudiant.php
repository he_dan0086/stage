<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table="etudiants";

    protected $fillable = ['nom', 'prenom', 'addresse','telephone','filiere_id','cv'];


    public function filiere()
    {
        return $this->belongsTo('App\Filiere','filiere_id');
    }

    /**
     * Un étudiant est associé à plusieurs feedbacks
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feedbacks()
    {
        return $this->hasMany('App\Feedback');
    }


    public function user()
    {
        return $this->morphOne('App\User', 'user');
    }

}
