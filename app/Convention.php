<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Convention extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "conventions";

    protected $fillable = ['nom','prenom','filiere','estEtranger','convention_en','convention_fr','convention_biling'];


}