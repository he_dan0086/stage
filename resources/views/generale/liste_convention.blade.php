<h3>Liste de conventions</h3>
@if($conventions->first() != null)
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <div class="table-responsive table black-font">
                    <table id="conventions_table" class="table table-bordred table-striped tablesorter">
                        <thead >
                        {{--<th><input type="checkbox" id="checkall" /></th>--}}
                        <th>Date<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>Filiere<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>Nom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>Prenom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>convention_en</th>
                        <th>convention_fr</th>
                        <th>convention_biling</th>
                        <th>actions</th>
                        </thead>

                        <tbody>
                        @foreach($conventions as $convention)
                            <tr>
                                {{--<th><input type="checkbox" id="checkthis" /></th>--}}
                                <td>{{$convention->created_at}}</td>
                                <td>{{$convention->filiere}}</td>
                                <td>{{$convention->nom}}</td>
                                <td>{{$convention->prenom}}</td>
                                <td>
                                    @if($convention->convention_en)
                                        <p data-placement="top" data-toggle="tooltip">
                                            @if(isset($role) && $role == 'admin')
                                                <a href="{{url("administrateur/conventions/download/".$convention->id."/en")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @elseif(isset($role) && $role =='responsable')
                                                <a href="{{url("responsable/conventions/download/".$convention->id."/en")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @endif
                                            <button data-target="#edit_{{ $convention->id }}_en" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"  >
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </button>
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{ $convention->id }}_en" >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </p>
                                    @else
                                        ----
                                    @endif
                                </td>
                                <td>
                                    @if($convention->convention_fr)
                                        <p data-placement="top" data-toggle="tooltip">
                                            @if(isset($role) && $role == 'admin')
                                                <a href="{{url("administrateur/conventions/download/".$convention->id."/fr")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @elseif(isset($role) &&  $role == 'responsable')
                                                <a href="{{url("responsable/conventions/download/".$convention->id."/fr")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @endif

                                            <button data-target="#edit_{{ $convention->id }}_fr" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" >
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </button>

                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$convention->id}}_fr" >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </p>
                                    @else
                                        ----
                                    @endif
                                </td>
                                <td>
                                    @if($convention->convention_biling)
                                        <p data-placement="top" data-toggle="tooltip">
                                            @if(isset($role) && $role == 'admin')
                                                <a href="{{url("administrateur/conventions/download/".$convention->id."/biling")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @elseif(isset($role) && $role == 'responsable')
                                                <a href="{{url("responsable/conventions/download/".$convention->id."/biling")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                            @endif

                                            <a href="#edit_{{ $convention->id }}_biling"  class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" >
                                                <span class="glyphicon glyphicon-pencil"></span>
                                            </a>

                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$convention->id}}_biling" >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </p>
                                    @else
                                        ----
                                    @endif
                                </td>
                                <td>
                                    <p data-placement="top" data-toggle="tooltip">
                                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$convention->id}}" >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </p>
                                </td>
                            </tr>




                            {{--=========================trois edit modal fade ================================--}}
                            <div class="modal fade" id="edit_{{ $convention->id }}_en" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.upload_conv', [
                                        'route' =>$route_update,
                                       'lang'  => 'en',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                            <div class="modal fade" id="edit_{{ $convention->id }}_fr" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.upload_conv', [
                                    'route' =>$route_update,
                                       'lang'  => 'fr',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <div class="modal fade" id="edit_{{ $convention->id }}_biling" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.upload_conv', [
                                    'route' =>$route_update,
                                       'lang'  => 'biling',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            {{--!!!=========================trois edit modal fade ================================--}}


                            {{--=========================trois delete modal fade ================================--}}
                            <div class="modal fade" id="delete_{{$convention->id}}_en" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.delete_conv', [
                                    'route' =>$route_delete,
                                       'lang'  => 'en',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <div class="modal fade" id="delete_{{$convention->id}}_fr" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.delete_conv', [
                                    'route' =>$route_delete,
                                       'lang'  => 'fr',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <div class="modal fade" id="delete_{{$convention->id}}_biling" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.delete_conv', [
                                    'route' =>$route_delete,
                                       'lang'  => 'biling',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                            <div class="modal fade" id="delete_{{$convention->id}}" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    @include('generale.delete_conv', [
                                    'route' =>$route_delete,
                                       'lang'  => 'conv',
                                       'id'  => $convention->id
                                     ])
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="clearfix"></div>
                    <ul class="pagination pull-right">
                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@else
    <h3 class="text-danger">Oups! Aucune convnetion établie sur AdopteUnStage pour l'instant!</h3>
@endif