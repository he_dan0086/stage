
<div class="offre">

    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Modifier Offre de stage</span>
        </div>

        <div class="historique__header__add">
            <a href="{{ URL(route('admin_gestion_offre_get')) }}" class="btn-float-action btn-green">Retour</a>
        </div>
    </div>
</div>

{!! Form::open(['route' => [$route, $offre->id], 'method' => 'put']) !!}
    <input name="_method" type="hidden" value="PUT">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="required-info center">
        <br/><i>* Les champs en rouge sont obligatoires</i><br/>
    </div>

    {{--============================Entreprse=================================--}}
    <div class="panel panel-success">
        <div class="panel-heading left">Entreprise</div>

        <br/>
        <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
            <input class="form-control" type="text" name="nom_entreprise" id="nom_entreprise" value="{{ $offre->nom_entreprise }}"required/>
            <div class="input-group-addon"><span>Nom de l'entreprise</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></div>
            <input class="form-control" type="text" name="code_postal" id="code_postal"  value="{{$offre->code_postal}}"required/>
            <div class="input-group-addon"><span>Code postal</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
            <input class="form-control" type="text" name="ville" id="ville" value="{{$offre->ville}}" required/>
            <div class="input-group-addon"><span>Ville</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
            <input class="form-control" type="text" name="pays" id="pays" value="{{$offre->pays}}" />
            <div class="input-group-addon"><span>Pays</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
            <input class="form-control" type="email" name="email" id="email" value="{{$offre->email}}" required/>
            <div class="input-group-addon"><span>E-mail de la personne à contacter pour confirmer la validation de l'offre de stage</span></div>
        </div>
        <br/>
    </div>


    {{--===============================stage==============================--}}
    <div class="stages">
        <div class="panel panel-success panel-stage-to-add">
            <div class="panel-heading left">Stage</div>
            <br/>

            {{--====niveau du stage=====--}}
            <h5 class="left" style="color:#080808;">- Niveau(<strong class="alert-success">plusieurs</strong>choix possible)
                <span class="colored__bullet__blue"></span>en choix</h5>
            <div class="btn-group" data-toggle="buttons">
                @foreach($niveaux['all'] as $niveau)
                    @if(in_array($niveau->id,$niveaux['mine']))
                        <label class="btn btn-green active">
                                <input class="checkbox" type="checkbox" name="niveau_0[]"  value="{{$niveau->id}} " checked/>
                                Stage {{$niveau->nom}}
                        </label>
                    @else
                        <label class="btn btn-green">
                            <input class="checkbox" type="checkbox" name="niveau_0[]"  value="{{$niveau->id}}"/>
                            Stage {{$niveau->nom}}
                        </label>
                    @endif
                @endforeach
            </div>
            {{--====End:niveau du stage=====--}}

            {{--=====departement ============--}}
            <h5 class="left" style="color: #080808;">- Département (<strong class="alert-success">plusieurs</strong>  choix possibles)
                <span class="colored__bullet__blue"></span>en choix</h5>
            <div class="btn-group" data-toggle="buttons">
                @foreach($departements['all'] as $departement)
                    @if(in_array($departement->id,$departements['mine']))
                    <label class="btn btn-green active">
                        <input class="checkbox" type="checkbox" name="departement_0[]"  value="{{$departement->id}}" checked/>
                        {{$departement->nom}}
                    </label>
                    @else
                        <label class="btn btn-green">
                            <input class="checkbox" type="checkbox" name="departement_0[]"  value="{{$departement->id}}"/>
                            {{$departement->nom}}
                        </label>
                    @endif
                @endforeach
            </div>
            {{--=====End:departement ============--}}

            <br/>
            <br/>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                <input class="form-control" type="text" name="titre" id="titre" value="{{$offre->titre}}" required/>
                <div class="input-group-addon"><span>Titre de l'offre de stage</span></div>
            </div>
            <br/>


            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></div>
                <textarea class="form-control" name="description" id="description"  cols="30" rows="5" required>{{$offre->description}}</textarea>
                <div class="input-group-addon"><span>Description succincte du stage</span></div>
            </div>
            <br/>

            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></div>
                <input class="form-control" type="text" name="gratification" id="gratification" value="{{$offre->gratification}}"/>
                <div class="input-group-addon"><span>Montant de la gratification (en monnaie locale)</span></div>

                <input class="form-control" type="text" name="devise" id="devise" value="{{$offre->devise}}"/>
                <div class="input-group-addon"><span>Devise</span></div>
            </div>
            <br/>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-tasks"></span></div>
                    <select class="select-multiple" multiple name="competence_0[]" id="competence" required>
                        @foreach($competences['all'] as $competence)

                            @if(in_array($competence->id, $competences['mine']))
                                <option selected value="{{ $competence->nom }}">{{ $competence->nom }}</option>
                            @else
                                <option value="{{ $competence->nom }}">{{ $competence->nom }}</option>
                            @endif
                        @endforeach
                    </select>
                    <div class="input-group-addon"><span>Compétences requises</span></div>
                </div>
            </div>
            <br/>
        </div>
    </div>

    <br/>
    <p class="button">
        {!! Form::submit('Enregistrer', ['class' => 'btn-form btn']) !!}
    </p>
{!! Form::close() !!}



{!! Form::open(['route' => ['admin_gestion_offre_file_post', $offre->id], 'method' => 'post', 'files'=>true, 'enctype'  => 'multipart/form-data']) !!}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="panel panel-success display_file">
    <div class="panel-heading left">Fichier Detail du stage</div>
    @if($offre->ficher_detail)
        <div class="profile__identity offre__block">
            <br/>
            <div class="offre__entreprise__content" style="text-align: center">
                <iframe class="frame_file" id="file" style="border:0px solid #666CCC" title="" src="{{ asset($offre->ficher_detail) }}"
                        frameborder="1" scrolling="auto" height="900" width="750" ></iframe>
            </div>
        </div>
        </br>
    @endif

    <div class="input-group form-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
        <input type="file" name="ficher_detail" id="ficher_detail" class="form-control"/>
        <div class="input-group-addon"><span>Description détaillée</span></div>

        {!! Form::submit('Envoyer', ['class' => 'btn-form btn']) !!}
    </div>
</div>
{!! Form::close() !!}

