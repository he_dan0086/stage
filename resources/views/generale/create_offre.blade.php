<form role="form" method="POST" action="{{ url(route($route)) }}" enctype="multipart/form-data"
      xmlns="http://www.w3.org/1999/html">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    {{--==========================informations warning=====================--}}
    <div class="alert alert-warning"><strong>Dépôt d'une offre de stage</strong><br/>
        Vous devez fournir un fichier (tout format) contenant la description détaillée de l'offre. Si besoin, vous pouvez utiliser le formulaire en ligne ci dessus.
        Vous recevrez un mail de confirmation si votre offre a été validée par le responsable des stages du cursus choisi.
    </div>
    {{--===========================Attentions==============================--}}
    <div class="alert alert-danger"><strong>Attention</strong><br/>
        le dépôt des offres de stage en MAM5, est fermé pour cette année.
        Pour ces promotions, nous vous invitons à déposer vos offres dès Septembre.

        <div class="required-info" >
            <strong>Les champs avec icon rouge(comme <span class="glyphicon glyphicon-home"></span>) sont obligatoires.</strong>
        </div>
    </div>

    {{--============================Entreprse=================================--}}
    <div class="panel panel-success">
        <div class="panel-heading left">Entreprise</div>
        <br/>
        <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
            <input class="form-control" type="text" name="nom_entreprise" id="nom_entreprise" placeholder="Nom de l'entreprise ou laboratoire"
                   value="{{old('nom_entreprise')}}" required/>
            <div class="input-group-addon"><span>Nom de l'entreprise</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></div>
            <input class="form-control" type="text" name="code_postal" id="code_postal" placeholder="Code postal" value="{{old('code_postal')}}" required/>
            <div class="input-group-addon"><span>Code postal</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
            <input class="form-control" type="text" name="ville" id="ville" placeholder="Ville" value="{{old('ville')}}" required/>
            <div class="input-group-addon"><span>Ville</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
            <input class="form-control" type="text" name="pays" id="pays" value="France" />
            <div class="input-group-addon"><span>Pays</span></div>
        </div>
        <br/>

        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
            <input class="form-control" type="email" name="email" id="email" placeholder="Email"  value="{{old('email')}}" required/>
            <div class="input-group-addon"><span>E-mail de la personne à contacter pour confirmer la validation de l'offre de stage</span></div>
        </div>
        <br/>
    </div>


    {{--===============================stage==============================--}}
    <div class="stages">
        <div class="panel panel-success panel-stage-to-add">
            <div class="panel-heading left">Stage
                <input id='stage1' class=" add-stage btn-green btn" type="button"
                       data-toggle="tooltip" data-placement="top" title="Ajouter un stage" value="+"/>
                <strong class="text-danger"> Ajouter un stage</strong>
            </div>
            <br/>

            {{--====niveau du stage=====--}}
            <h5 class="left" style="color:#080808;">- Niveau(<strong class="text-danger">plusieurs </strong>choix possible)
                (si sélectionné, en <span class="colored__bullet__blue"></span>)</h5>
            <div class="btn-group" data-toggle="buttons">
                @foreach($niveaux as $niveau)
                    <label class="btn btn-green">
                        <input class="checkbox" type="checkbox" name="niveau_0[]"  value="{{$niveau->id}}"/>
                        Stage {{$niveau->nom}}
                    </label>
                @endforeach
            </div>
            {{--====End:niveau du stage=====--}}

            {{--=====departement ============--}}
            <h5 class="left" style="color: #080808;">- Département (<strong class="text-danger">plusieurs </strong>  choix possibles)
                (si sélectionné, en <span class="colored__bullet__blue"></span>)</h5>
            <div class="btn-group" data-toggle="buttons">
                @foreach($departements as $departement)
                    <label class="btn btn-green">
                        <input class="checkbox" type="checkbox" name="departement_0[]"  value="{{$departement->id}}"/>
                        {{$departement->nom}}
                    </label>
                @endforeach
            </div>
            {{--=====End:departement ============--}}

            <br/>
            <br/>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                <input class="form-control" type="text" name="titre[0]" id="titre" placeholder="Titre"
                       value="{{old('titre[0]')}}"
                       required/>
                <div class="input-group-addon"><span>Titre de l'offre de stage</span></div>
            </div>
            <br/>


            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></div>
                <textarea class="form-control" name="description[0]" id="description"  cols="30" rows="5">{{old('description[0]')}}</textarea>
                <div class="input-group-addon"><span>Description succincte du stage (5 lignes maximum)</span></div>
            </div>
            <br/>

            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></div>
                <input class="form-control" type="number" min="0" name="gratification[0]" id="gratification"
                       value="{{old('gratification[0]')}}" placeholder="Gratification"/>
                <div class="input-group-addon"><span>Montant de la gratification (en monnaie locale)</span></div>

                <input class="form-control" type="text" name="devise[0]" id="devise" value="Euros"/>
                <div class="input-group-addon"><span>Devise</span></div>
            </div>
            <br/>

            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-wrench"></span></div>
                <select class="select-multiple " multiple name="competence_0[]" >
                    @foreach($competences as $competence)
                        <option value="{{ $competence->nom }}">{{ $competence->nom }}</option>
                    @endforeach
                </select>
                <div class="input-group-addon"><span>Compétences requises</span></div>
            </div>
            <br/>

            <div class="input-group form-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
                <input type="file" name="ficher_detail[0]" id="ficher_detail" class="form-control"   value="{{old('ficher_detail[0]')}}"  required/>
                <div class="input-group-addon"><span>Description détaillée</span></div>
            </div>
        </div>
    </div>
    <br/>
    <p class="button">
        <input type="submit" class="btn btn-form" value="Valider">
    </p>
</form>