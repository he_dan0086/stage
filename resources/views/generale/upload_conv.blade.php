{!! Form::open(['route' => [$route,$id,$lang], 'method'=>'post', 'files' => true, 'enctype'  => 'multipart/form-data']) !!}
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align black-font" id="Heading">Edit Your Detail</h4>
    </div>
    <div class="modal-body">
        <div class="form-group input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
            <input type="file" name="nouveau_conv" class="form-control" />
            <div class="input-group-addon">Upload un nouveau fichier</div>
        </div>
    </div>
    <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
    </div>
</div>
{!! Form::close() !!}