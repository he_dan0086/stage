<br/><br/>

<div class="offre">

    <div class="historique__header">
        <div class="historique__header__title">
            <a href="javascript:history.go(-1)" class="btn-float-action btn-green"　style="padding: 16px 18px;"><i class="glyphicon glyphicon-backward"></i></a>
            <span class="historique__header__title__span">Offre de stage</span>
        </div>
        @if(isset($auteur))
            @if($auteur=="etudiant")
                <div class="historique__header__add">
                    <span>Postuler</span>
                    <a class=" btn-float-action btn-green"  style="padding: 16px 18px;" href="{{URL('mailto:'.$offre->email.'?Subject=[Candidature%20des%20offres-stages]AdopteUnStage-Polytech')}}" ><i class="fa fa-rocket"></i></a>
                </div>
            @endif
            @if($auteur =="admin")
                @if($offre->valide ==false)
                    {!! Form::open(['route' =>['validation_offrestage', $offre->id], 'method' => 'post']) !!}
                        <div class="historique__header__add">
                            <span>Valider</span>
                            <button type="submit" class="btn-float-action btn btn-green" style="padding: 8px 10px;"><i class="fa fa-send"></i></button>
                        </div>
                    {!! Form::close() !!}
                @endif
            @endif
        @endif
    </div>
    <br/><br/>

{{--===========================type du stage==============================--}}
    <div class="offre__description offre__block">
        <div class="offre__description__header offre__block__header">
            <span class="block__header__title">Type du stage</span>
        </div>
        <div class="offre__block__row">
            <div class="offre__block__cell">
                <span class="titre">Niveau du stage : </span><br/>
                @foreach($offre->niveaux as $niveau)
                    <span class="colored__bullet__green"></span>
                    <span class="contenu"> {{ $niveau->nom }} </span><br>
                @endforeach

                <span class="titre">Departements : </span><br/>
                @foreach($offre->departements as $departement)
                    <span class="colored__bullet__green"></span>
                    <span class="contenu"> {{ $departement->nom }} </span><br>
                @endforeach
            </div>
        </div>
    </div>
    {{--===========================competences reauires==============================--}}
    @if($offre->competences->first() != null)
        <div class="offre__description offre__block">
            <div class="offre__description__header offre__block__header">
                <span class="block__header__title">Compétences requises</span>
            </div>
            <div class="offre__block__row">
                <div class="offre__block__cell">
                    <span class="contenu">
                        <ul>
                            @foreach($offre->competences as $competence)
                                <li>{{ $competence->nom }}</li>
                            @endforeach
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    @endif

    {{--===========================informations sur l'entreprise==============================--}}
    <div class="offre__infos offre__block">
        <div class="offre__infos__header offre__block__header">
            <span class="block__header__title">Informations de l'entreprise</span>
        </div>
        <div class="offre__block__row">

            <div class="offre__block__cell__left">
                <div class="offre__infos__contact">
                    <span class="titre">Nom de l'entreprise : </span>
                    <span class="contenu">{{ $offre->nom_entreprise }}</span><br/>

                    <span class="titre">Code postal: </span>
                    <span class="contenu">{{ $offre->code_postal }}</span><br/>

                    <span class="titre">Ville: </span>
                    <span class="contenu">{{ $offre->ville }}</span><br/>
                </div>
            </div>

            <div class="offre__block__cell__right">
                <div class="offre__infos__description">
                    <!-- PAYS-->
                    <span class="titre"> Pays :  </span>
                    @if($offre->pays)
                        <span class="contenu"> <i>{{ $offre->pays }}</i> </span><br/>
                    @else
                        <span class="contenu"><i>Non précisé</i></span><br/>
                    @endif

                    <!-- EMAIL -->
                    <span class="titre"> E-mail de la personne à contacter:  </span>
                    <span class="contenu"> <i>{{ $offre->email }}</i> </span><br/>
                </div>
            </div>
        </div>
    </div>
    {{--===========================End: informations sur l'entreprise==============================--}}


    {{--===========================informations sur le stage==============================--}}
    <div class="offre__description offre__block">
        <div class="offre__description__header offre__block__header">
            <span class="block__header__title">Stage</span>
        </div>
        <div class="offre__block__row">
            <div class="offre__block__cell">
                <span class="titre"> Description succincte du stage:  </span>
                <span class="contenu"> <i>{{ $offre->description }}</i> </span><br/>

                <span class="titre"> Gratification:  </span>
                @if($offre->gratification==0)
                    <span class="contenu"> <i> A discuter</i> </span><br/>
                @else
                    <span class="contenu"> <i>{{ $offre->gratification }} {{$offre->devise}}</i> </span><br/>
                @endif
            </div>
        </div>
    </div>

    <div class="profile__identity offre__block">
        <div class="offre__entreprise__header">
            <div class="offre__entreprise__header__title">
                <span class="offre__entreprise__header__name">Fichier Detail du stage</span>
            </div>
        </div>
        <br/>
        <div class="offre__entreprise__content" style="text-align: center">
            <iframe id="cv" style="border:0px solid #666CCC" title="" src="{{ asset($offre->ficher_detail) }}"
                    frameborder="1" scrolling="auto" height="900" width="721" ></iframe>
        </div>
    </div>
</div>
