@if($etudiants->first() != null)
<div class="historique__list">

    @foreach ($etudiants as $etudiant)

        <div class="historique__list__item">

            <div class="historique__list__item__row">
                <div class="historique__list__item__content">
                      <span class="historique__list__item__content__title">
                            Etudiant: {{$etudiant->nom}} {{$etudiant->prenom}}
                      </span>
                      <span class="historique__list__item__content__contact">
                          <div class="col-lg-10">
                             <span>
                                 <span class="colored__bullet__green"></span>
                                 <strong>Filiere:</strong> {{$etudiant->filiere->filiere_nom}}<br/>
                             </span>
                              <span>
                                 <span class="colored__bullet__red"></span>
                                 <strong>Adresse:</strong> {{$etudiant->addresse}}<br/>
                             </span>
                              <span>
                                 <span class="colored__bullet__yellow"></span>
                                 <strong>Telephone:</strong>{{$etudiant->telephone}}<br/>
                             </span>
                          </div>
                          <div class="col-lg-2">
                              @if(isset($schoolLogo))
                                  <img  src="{{ asset('uploads/ecole/'.($schoolLogo->value))}}" style="height: 50px;"/>
                              @endif
                          </div>
                      </span>
                </div>

                <div class="historique__list__item__control">

                    @include($boutons, ['etudiant' => $etudiant])

                </div>

            </div>

        </div>

    @endforeach
</div>
@else
    <h3 class="text-danger">Il n'y pas d'étudiant qui dépose son cv sur AdopteunStage</h3>
@endif