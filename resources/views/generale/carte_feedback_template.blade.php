@if($feedbacks->first()!=null)
    <div class="historique__list">
        @foreach ($feedbacks as $feedback)

            <div class="historique__list__item">
                <div class="historique__list__item__row">
                    <div class="historique__list__item__content">
                        <span class="historique__list__item__content__title">
                           Entreprise:  {{$feedback->entreprise}}
                          </span>
                            <span class="historique__list__item__content__contact">
                                <span class="colored__bullet__green"></span>
                                Description de l'entreprise:
                                <p class="short-text">{{$feedback->descrption_entreprise}}</p>

                                <span class="colored__bullet__red"></span>
                                Feedback:
                                <p class="short-text">{{$feedback->contenu}}</p>

                                <span class="colored__bullet__yellow"></span>
                                Recrutement:
                                <p class="short-text">{{$feedback->recrutement_feedback}}</p>
                            </span>

                        <div class="historique__list__state">
                            <div class="historique__list__state__section">
                                @if($feedback->valide==true)
                                    <span class="text-success"><strong>Feedback Validé</strong></span>
                                @else
                                    <span class="text-danger"><strong>Validation en cour</strong></span>

                                @endif

                                @if($feedback->isOuvert)
                                    <span>Par {{$feedback->etudiant->nom}} {{$feedback->etudiant->prenom}}</span>
                                @else
                                    <span>Anonyme</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="historique__list__item__control">
                        @include($boutons, ['feedback' => $feedback])
                    </div>

                    {{--========================= edit modal fade ================================--}}
                    <div class="modal fade" id="edit_{{ $feedback->id }}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- /.modal-content -->
                            @include('generale.edit_feedback',
                                 [
                                    'feedback' => $feedback
                                 ])
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    {{--!!!========================= edit modal fade ================================--}}



                    {{--========================= delete modal fade ================================--}}
                    <div class="modal fade" id="delete_{{$feedback->id}}" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- /.modal-content -->
                            {!! Form::open(['route' =>['responsable_delete_feedback', $feedback->id], 'method' => 'delete']) !!}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align black-font" id="Heading">Supprimer ce record dans la base de donnée</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                                </div>
                                <div class="modal-footer ">
                                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                </div>
            </div>
        @endforeach
    </div>
@else
    <h3 class="text-danger">Vous n'avez pas encore créé aucune feedback</h3>
@endif