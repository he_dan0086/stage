<br/><br/>

<div class="etudiant">

    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">
                <a href="javascript:history.go(-1)" class="btn-float-action btn-green"　style="padding: 16px 18px;"><i class="glyphicon glyphicon-backward"></i></a>
                Profil de {{ $user->user->nom }} {{$user->user->prenom}}
            </span>
        </div>
        <div class="historique__header__add">
            <span>Contacter</span>
            <a href="mailto:{{ $user->email }}" class="btn-float-action btn-green" style="padding: 16px 18px;">
                <i class="fa fa-envelope-o"></i>
            </a>
        </div>
    </div>

    <br/><br/>
    <div class="profile__identity offre__block">
        <div class="offre__entreprise__header">
            <div class="offre__entreprise__header__title">
                <span class="offre__entreprise__header__name">Qui suis-je ?</span>
            </div>
        </div>
        <br/>
        <div class="offre__entreprise__content">
            <ul>
                <li>
                    <span class="title">Adresse : </span>
                    <span class="contenu">{{$user->user->addresse}}</span>
                </li>
                <li>
                    <span class="title">Filiere : </span>
                    <span class="contenu">{{$filiere->filiere_nom}}</span>
                </li>
                <li>
                    <span class="title">Telephone : </span>
                    <span class="contenu">{{$user->user->telephone}}</span>
                </li>
            </ul>

        </div>
    </div>

    @if($user->user->cv != null)
        <div class="profile__identity offre__block">
            <div class="offre__entreprise__header">
                <div class="offre__entreprise__header__title">
                    <span class="offre__entreprise__header__name">Mon CV</span>
                </div>
            </div>
            <br/>
            <div class="offre__entreprise__content" style="text-align: center">
                <iframe id="cv" style="border:0px solid #666CCC" title="" src="{{ asset($user->user->cv) }}"
                        frameborder="1" scrolling="auto" height="1000" width="721" ></iframe>
            </div>
        </div>
    @endif



</div>
