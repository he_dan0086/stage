@if($offres->first()!=null)
<div class="historique__list">
    @foreach ($offres as $offre)

        <div class="historique__list__item">
            <div class="historique__list__item__row">
                <div class="historique__list__item__content">
                      <span class="historique__list__item__content__title">
                        {{$offre->nom_entreprise}} - {{$offre->titre}}
                      </span>
                        <span class="historique__list__item__content__contact">
                            <span class="glyphicon glyphicon-envelope"> {{ $offre->ville }}({{$offre->code_postal}})</span><br/>
                            <span class="glyphicon glyphicon-euro">
                                Gratification:
                                @if($offre->gratification==0)
                                  A décider
                                @else
                                    {{$offre->gratification}} {{$offre->devise}}
                                @endif
                            </span>
                        </span>
                      </span>

                    @if(isset($valide_info[$offre->id]))
                        <div class="historique__list__state">
                            <div class="historique__list__state__section" id="{{'valider-info-'.$offre->id}}">
                                @if($valide_info[$offre->id] == '1')
                                    <span class="colored__bullet__green"></span>
                                    <span>Offre validée</span>
                                @else
                                    <span class="colored__bullet__red"></span>
                                    <span>Validation en cours</span>
                                @endif
                            </div>
                            <div class="historique__list__state__section">
                                @if($offre->nombre_vu == 0 )
                                    <span class="colored__bullet__red"></span>
                                    <span>Aucun visiteur</span>
                                @else
                                    <span class="colored__bullet__green"></span>Visiteurs
                                    <span class="badge">{{$offre->nombre_vu}} </span>
                                @endif
                            </div>
                            <div class="historique__list__state__section">
                                <span class="colored__bullet__yellow"></span>
                                <span>{{ $offre->created_at }}</span>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="historique__list__item__control">
                    @include($boutons, ['offre' => $offre])
                </div>
            </div>
        </div>
    @endforeach
</div>
@else
    <h3 class="text-danger">Aucune offre de stage publiée sur AdopteUnStage</h3>
@endif