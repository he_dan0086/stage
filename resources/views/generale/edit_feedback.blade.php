@if(isset($route))
{!! Form::open(['route' => [$route, $feedback->id], 'method' => 'post',
     'xmlns' => 'http://www.w3.org/1999/html', 'class' => 'form-horizontal form']) !!}
@else
    {!! Form::open(['class' => 'form-horizontal form']) !!}
@endif

<div class="panel panel-info">
    <div class="panel-heading left">
        Feedback
    </div>
    <br/>

    <div class="text-info" >
        <div class="form-group sans-margin-bottom" >
            <label class="col-sm-4 control-label">Entreprise</label>
            <div class="col-sm-8 left">
                <input class=" col-lg-10 form-control-static" name="entreprise" value="{{$feedback->entreprise}}" >
            </div>
        </div>
        <div class="form-group sans-margin-bottom"  >
            <label class="col-sm-4 control-label">Description</label>
            <div class="col-sm-8 left">
                <textarea class=" col-lg-10 form-control-static" name="descrption_entreprise">{{$feedback->descrption_entreprise}}</textarea><br/>
            </div>
        </div>
        <div class="form-group sans-margin-bottom">
            <label class="col-sm-4 control-label">Feedback</label>
            <div class="col-sm-8 left">
                <textarea class=" col-lg-10 form-control-static "  name="contenu">{{$feedback->contenu}}</textarea><br/>
            </div>
        </div>
        <div class="form-group sans-margin-bottom">
            <label class="col-sm-4 control-label">Recrutement</label>
            <div class="col-sm-8 left">
                <textarea class=" col-lg-10 form-control-static "  name="recrutement_feedback">{{$feedback->recrutement_feedback}}</textarea><br/>
            </div>
        </div>
        @if(!isset($responsable))
            <div class="form-group">
                <div class="checkbox col-sm-10 control-label">
                    <label for="isOuvert">
                        @if($feedback->isOuvert)
                            <input type="checkbox" name="isOuvert" checked/>
                        @else
                            <input type="checkbox" name="isOuvert"/>
                        @endif
                        Autorisez d'afficher mes informations du contact
                    </label>
                </div>
            </div>
        @else
            <div class="form-group">
                <div class="checkbox col-sm-8 control-label">
                        @if($feedback->isOuvert == true)
                            <div class="text-success">
                                Etudiant Info Autorisé pour la publique<br/>
                                par {{$feedback->etudiant->nom}} {{$feedback->etudiant->prenom}}
                            </div>
                        @else
                        <div class="text-danger">Etudiant Info NON Autorisé pour la publique
                        <br/>par {{$feedback->etudiant->nom}} {{$feedback->etudiant->prenom}}
                        </div>
                        @endif
                </div>
            </div>

        @endif
    </div>

    <br/>
    <br/>
    @if(isset($route))
        <input type="submit" class="btn btn-success" value="ENREGISTRER">
    @else
        <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> Annuler</button>
    @endif
</div>

{!! Form::close() !!}

