@extends('template_panel')

@section('title')
    Panel Responsable
@endsection

@section('style_panel')
    <link rel="stylesheet" href="{{ asset('css/panel-admin.css') }}">
@endsection

@section('fil-ariane')
    <li><a href="{{ url(route('accueil-responsable')) }}"> Panel responsable </a></li>
@endsection

@section('menu')
    <li class="{{ Request::is('responsable/profil') ? 'active':'' }}">
        <a href="{{ url('responsable/profil') }}">PROFIL</a>
    </li>

    <li class="{{ Request::is('responsable/conventions') ? 'active':'' }}">
        <a href="{{ url('responsable/conventions') }}">GESTION DES CONVENTIONS</a>
    </li>

    <li class="{{ Request::is('responsable/templates') ? 'active':'' }}">
        <a href="{{ url('responsable/templates') }}">GESTION DES TEMPLATES</a>
    </li>

    <li class="{{ Request::is('responsable/feedbacks') ? 'active':'' }}">
        <a href="{{ url('responsable/feedbacks') }}">GESTION DES FEEDBACKS</a>
    </li>


@endsection


