@extends('responsable.layout')

@section('title')
    Templates
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Gesion des  templates </li>
@endsection

@section('content')


    <h3>Gesion des  templates</h3><br/>


    @include('generale.form_notification')
    @include('generale.flash_message')

    @if(isset($template) && ($template->template_fr != null))
        <a class="left btn btn-warning" href="{{ URL('responsable/template/download/template_fr') }}"><span class="glyphicon glyphicon-save"></span> Template Française</a>
    @elseif(!isset($template))
        <div class="text-danger"> <strong>Il y  a pas encore des templates dans votre filière, vous pouvez upload des templates!!</strong></div>
    @endif
    <br/>
    <br/>
    {!! Form::open(['route' => ['responsable_upload_template_post','template_fr'], 'method'=>'post','class' => 'form-inline', 'files' => true, 'enctype'  => 'multipart/form-data']) !!}
    <div class="input-group form-group left-bg">
        <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
        <input type="file" name="template_fr" class="form-control" required />
        <div class="input-group-addon"><span >Template Française</span></div>
    </div>

    <div class="form-group right-bg">
        {!! Form::submit('Envoyer', ['class' => 'btn-form right']) !!}
    </div>
    {!! Form::close() !!}
    <hr/>

    @if(isset($template) && ($template->template_en != null))
        <a class="left btn btn-warning" href="{{ URL('responsable/template/download/template_en') }}"><span class="glyphicon glyphicon-save"></span> Template Anglaise</a>
    @endif
    <br/>
    <br/>
    {!! Form::open(['route' => ['responsable_upload_template_post','template_en'], 'method'=>'post','class' => 'form-inline', 'files' => true, 'enctype'  => 'multipart/form-data']) !!}
    <div class="input-group form-group left-bg">
        <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
        <input type="file" name="template_en" class="form-control" required />
        <div class="input-group-addon"><span >Template Anglaise</span></div>
    </div>

    <div class="form-group right-bg">
        {!! Form::submit('Envoyer', ['class' => 'btn-form right']) !!}
    </div>
    {!! Form::close() !!}
    <hr/>

    @if(isset($template) && ($template->template_biling != null))
        <a class="left btn btn-warning" href="{{ URL('responsable/template/download/template_biling') }}"><span class="glyphicon glyphicon-save"></span> Template Bilingue</a>
    @endif
    <br/>
    <br/>
    {!! Form::open(['route' => ['responsable_upload_template_post','template_biling'], 'method'=>'post','class' => 'form-inline', 'files' => true, 'enctype'  => 'multipart/form-data']) !!}
    <div class="input-group form-group left-bg">
        <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
        <input type="file" name="template_biling" class="form-control" required />
        <div class="input-group-addon"><span >Template Bilingue</span></div>
    </div>

    <div class="form-group right-bg">
        {!! Form::submit('Envoyer', ['class' => 'btn-form right']) !!}
    </div>
    {!! Form::close() !!}
    <hr/>

@endsection

@section('script')

@endsection