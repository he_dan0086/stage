@extends('responsable.layout')

@section('title')
    Profil
@endsection

@section('fil-ariane')
    @parent
    <li class="active">MON PROFIL</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Profil</span>
        </div>
    </div>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    <form role="form" method="POST" action="{{url(route('responsable_profil_post'))}}" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />


        <div class="panel panel-info">
            <div class="panel-heading left">Responsable</div>

            <div class="text-info" >
                <div class="form-group sans-margin-bottom" >
                    <label class="col-sm-4 control-label">NOM</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="nom" value="{{$user->user->nom}}" >
                    </div>
                </div>
                <div class="form-group sans-margin-bottom"  >
                    <label class="col-sm-4 control-label">PRENOM</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="prenom" value="{{$user->user->prenom}}" >
                    </div>
                </div>
                <div class="form-group sans-margin-bottom"  >
                    <label class="col-sm-4 control-label">PRENOM</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="email" value="{{$user->email}}"  readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">FILIERE</label>
                    <div class="col-sm-8 left">
                        @if($filiere_res!=null)
                            <input class=" col-lg-10 form-control-static " value="{{$filiere_res->filiere_nom}}" readonly>
                        @else
                            <input class=" col-lg-10 form-control-static "  readonly>
                        @endif


                        <select class="col-lg-10 form-control-static" name="Filiere" id="Filiere" >
                            <option selected disabled> Sélectionner une filiere</option>
                            @foreach($filieres as $filiere)
                                <option value="{{ $filiere->id }}"> {{ $filiere->filiere_nom }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
            </div>
            <br/>
            <input type="submit" class="btn btn-success" value="ENREGISTRER">
        </div>
        <br/>

    </form>
@endsection


