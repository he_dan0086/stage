@extends('responsable.layout')

@section('title')
    Liste de feedbacks
@endsection

@section('fil-ariane')
    @parent
    <li class="active">GESTION DES FEEDBACKS</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Liste de feedbacks</span>
        </div>
    </div>
    <br/>
    @include('generale.carte_feedback_template', [
        'feedbacks'  => $feedbacks,
        'boutons' => 'responsable.boutons.gestion_feedback'
    ])
@endsection