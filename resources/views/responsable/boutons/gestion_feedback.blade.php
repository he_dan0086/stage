@if($feedback->valide == false)
    <div class="btn btn-form btn-green-valider">
        {!! Form::open(['route' =>['valider-feedback-responsable', $feedback->id], 'method' => 'post']) !!}
            <button type="submit"><span class="glyphicon glyphicon-ok"></span></button>
            <span class="historique__list__item__control__name">Valider</span>
        {!! Form::close() !!}
    </div>
@endif
<button class="btn btn-form btn-blue-view" data-title="Edit" data-toggle="modal" data-target="#edit_{{$feedback->id}}" >
    <span class="glyphicon glyphicon-fullscreen"></span>
    <span class="historique__list__item__control__name">Afficher</span>
</button>

<div class="btn btn-form btn-red-trash"  data-title="Delete" data-toggle="modal" data-target="#delete_{{$feedback->id}}">
    <span class="glyphicon glyphicon-trash"></span>
    <span class="historique__list__item__control__name">Effacer</span>
</div>
