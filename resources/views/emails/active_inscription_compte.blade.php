@extends('emails.layout')

@section('content')
    <h4>Cher client</h4>
    <br/>Bonjour,
    <br/><br/>Vous avez inscrit sur AdopteUnStage à l'heure: {{$token->created_at}} en utilisant l'adresse {{$email}}, cliquez le lien au-dessous pour activer le cpompte
    <a href="{{ url('/inscription/active') . '/'.$id.'/' . $token->token }}">{{ url('/inscription/active') . '/'.$id.'/' .  $token->token }}</a>
@endsection