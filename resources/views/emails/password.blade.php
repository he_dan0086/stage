@extends('emails.layout')

@section('content')

    <br/>
    <h4>Reset link</h4>
    <br/>Click here to reset your password: {{ url('password/reset/'.$token) }}
    <br/>


@endsection