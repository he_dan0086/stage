Bonjour Monsieur/Madame @if(isset($nom)){{ $nom }} @endif,<br/>

@yield('content')


<br/><br/>A très bientôt sur AdopteUnStage !
<br/>Cordialement

<br/><br/>--
<br/>L'équipe adopteunstage
