@extends('emails.layout')

@section('content')

    <br/>
    <h4>Feedback à valider</h4>
    <br/>Un des étudiants vient d'ajouter un feedback sur AdopteUnStage
    <br/>Vous pouvez la valider et contacterez l'étudiant s'il y des problèmes!
    <br/>
    <h3>Feedback</h3>
    <br/>Entreprise: {{$feedback->entreprise}}
    <br/>Description de l'entreprise: {{$feedback->descrption_entreprise}}
    <br/>Feedback: {{$feedback->contenu}}
    <br/>Recrutement: {{$feedback->recrutement_feedback}}
    <br/>Créé pas {{$etudiant->nom}} {{$etudiant->prenom}}



@endsection