@extends('etudiant.layout')

@section('title')
    Liste de feedbacks
@endsection

@section('fil-ariane')
    @parent
    <li class="active">GESTION DE MES FEEDBACKS</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Liste de feedbacks</span>
        </div>
        <div class="historique__header__add">
            <a href="{{ URL('etudiant/gestion-feedback/create') }}" class="btn-float-action btn-green">+</a>
        </div>
    </div>
    <br/>
    @include('generale.carte_feedback_template', [
        'feedbacks'  => $feedbacks,
        'boutons' => 'etudiant.boutons.gestion_feedback'
    ])
@endsection
