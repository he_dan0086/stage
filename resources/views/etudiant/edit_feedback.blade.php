@extends('etudiant.layout')

@section('title')
    Edit Feedback
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Edit Feedback</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">
                <a href="javascript:history.go(-1)" class="btn-float-action btn-green"　style="padding: 16px 18px;">
                    <i class="glyphicon glyphicon-backward"></i>
                </a>
                Edit Feedback</span>
        </div>
    </div>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    @include('generale.edit_feedback',
     [
        'route' =>'post-edit-feedback-etudiant',
        'feedback' => $feedback
     ])
@endsection


