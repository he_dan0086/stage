@extends('etudiant.layout')

@section('title')
    Etablir une convention de stage
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Etablir une convention de stage</li>
@endsection

@section('content')

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Etablir une convention de stage</span>
        </div>
    </div>

    <br/>
    <div class="alert alert-danger"><strong>Attention</strong></br>
        Veuillez vérifier que les informations suivantes sont correctes avant de valider définitivement vos données.
    </div>

    {{--==========================infos sur la convention=====================--}}
    <form role="form" method="POST" action="{{url(route('convention_post'))}}" xmlns="http://www.w3.org/1999/html" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        {{--============================Etudiant=================================--}}
        <div class="panel panel-info">
            <div class="panel-heading left">Etudiant</div>
            <input type="hidden" name="EstLabo" value="{{$infos['EstLabo']}}">
            <input type="hidden" name="EstEtranger" value="{{$infos['EstEtranger']}}">

            <div class="text-info" >
                <h3 class="left" style="padding-left: 15px;" name="Etu">{{$infos['GenreEtu']}} {{$infos['PrenomEtu']}} {{$infos['NomEtu']}}</h3>
                <input type="hidden" name="GenreEtu" value="{{$infos['GenreEtu']}}">
                <input type="hidden" name="PrenomEtu" value="{{$infos['PrenomEtu']}}">
                <input type="hidden" name="NomEtu" value="{{$infos['NomEtu']}}">
                <input type="hidden" name="CPEtu" value="{{$infos['CPEtu']}}">
                <input type="hidden" name="VilleEtu" value="{{$infos['VilleEtu']}}">

                <div class="form-group sans-margin-bottom" >
                    <label class="col-sm-4 control-label">Courriel</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="EmailEtu" value="{{$infos['EmailEtu']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom"  >
                    <label class="col-sm-4 control-label">Téléphone</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="TelEtu" value="{{$infos['TelEtu']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Adresse</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static "  name="AdressEtu" value="{{$infos['AdressEtu']}}" readonly><br/>
                        <input class=" col-lg-10 form-control-static " name="cp_ville_Etu" value="{{$infos['CPEtu']}} {{$infos['VilleEtu']}}" readonly><br/>
                        <input class=" col-lg-10 form-control-static "  name="pays_etudiant" value="{{$infos['pays_etudiant']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Filière</label>
                    <div class="col-sm-8 left">
                        @if(isset($infos['Filiere']))
                            <input class=" col-lg-10 form-control-static "  name="Filiere" value="{{$infos['Filiere']}}" readonly>
                        @else
                            <input class=" col-lg-10 form-control-static "  name="Filiere"  readonly>
                        @endif
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Date de naissance</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static " name="DateNaissEtu" value="{{$infos['DateNaissEtu']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Numéro d'étudiant</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static " name="NumEtu" value="{{$infos['NumEtu']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Numéro de sécurité sociale</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static "  name="NumSS" value="{{$infos['NumSS']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Assurance</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static "  value="{{$infos['Assurance']}} / {{$infos['AssuranceNo']}}" readonly>
                        <input type="hidden" name="Assurance" value="{{$infos['Assurance']}}">
                        <input type="hidden" name="AssuranceNo" value="{{$infos['AssuranceNo']}}">
                    </div>
                </div>
            </div>
        </div>
        <br/>
        {{--=END!!!!!!===========================Etudiant=================================--}}



        {{--============================Entreprise=================================--}}
        <div class="panel panel-success">
            <div class="panel-heading left">Entreprise</div>
            <div class="text-success" >
                <h3 class="left" style="padding-left: 15px;">{{$infos['NomEntreprise']}} </h3>
                <input type="hidden" name="NomEntreprise" value="{{$infos['NomEntreprise']}}">
                <input type="hidden" name="site_web" value="{{$infos['site_web']}}">
                <div class="form-group sans-margin-bottom" >
                    <label class="col-sm-4 control-label">Adresse siège social</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static "  name="AdrSiegeSocial" value="{{$infos['AdrSiegeSocial']}}" readonly>
                        <input type="hidden" name="CPSiegeSocial" value="{{$infos['CPSiegeSocial']}}">
                        <input type="hidden" name="VilleSiegeSocial" value="{{$infos['VilleSiegeSocial']}}">
                        <input class=" col-lg-10 form-control-static "  name="cp_ville_SS" value="{{$infos['CPSiegeSocial']}} {{$infos['VilleSiegeSocial']}}" readonly>
                        <input class=" col-lg-10 form-control-static"  name="pays_ss" value="{{$infos['pays_ss']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom"  >
                    <label class="col-sm-4 control-label">Adresse Stage</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static " name="Adresse1Stage" value="{{$infos['Adresse1Stage']}}" readonly>
                        <input class=" col-lg-10 form-control-static" name="cp_ville_stage" value="{{$infos['CPStage']}} {{$infos['VilleStage']}}" readonly>
                        <input type="hidden" name="CPStage" value="{{$infos['CPStage']}}">
                        <input type="hidden" name="VilleStage" value="{{$infos['VilleStage']}}">
                        <input  class=" col-lg-10 form-control-static" name="pays"  value="{{$infos['pays']}}" readonly>
                    </div>
                </div>
                <hr class="hr-style"/>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Directeur ({{$infos['QualiteDirEnt']}})</label>
                    <input type="hidden" name="QualiteDirEnt" value="{{$infos['QualiteDirEnt']}}">

                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"   value="{{$infos['GenreDirEnt']}} {{$infos['PrenomDirEnt']}} {{$infos['NomDirEnt']}}" readonly>
                        <input type="hidden" name="GenreDirEnt" value="{{$infos['GenreDirEnt']}}">
                        <input type="hidden" name="PrenomDirEnt" value="{{$infos['PrenomDirEnt']}}">
                        <input type="hidden" name="NomDirEnt" value="{{$infos['NomDirEnt']}}">
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Courriel</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="EmailDirEnt"  value="{{$infos['EmailDirEnt']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Téléphone</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="TelDirEnt" value="{{$infos['TelDirEnt']}}" readonly>
                    </div>
                </div>
            </div>
        </div>
        {{--=END!!!!!!===========================Entreprise=================================--}}


        {{--============================Stage=================================--}}
        <div class="panel panel-danger">
            <div class="panel-heading left">Stage</div>
            <div class="text-danger" >
                <h3 class="left" style="padding-left: 15px;">{{$infos['TitreStage']}} </h3>
                <input type="hidden" name="TitreStage" value="{{$infos['TitreStage']}}">

                <div class="form-group sans-margin-bottom" >
                    <label class="col-sm-4 control-label">Encadreur</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  value="{{$infos['GenreEncadreur']}} {{$infos['PrenomEncadreur']}} {{$infos['NomEncadreur']}}" readonly>
                        <input type="hidden" name="GenreEncadreur" value="{{$infos['GenreEncadreur']}}">
                        <input type="hidden" name="PrenomEncadreur" value="{{$infos['PrenomEncadreur']}}">
                        <input type="hidden" name="NomEncadreur" value="{{$infos['NomEncadreur']}}">
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Fonction</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="FctEncadreur" value="{{$infos['FctEncadreur']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Courriel</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="EmailEncadreur" value="{{$infos['EmailEncadreur']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Téléphone</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="TelEncadreur" value="{{$infos['TelEncadreur']}}"  readonly>
                    </div>
                </div>
                <hr class="hr-style"/>

                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Enseignant Responsable</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  value="{{$infos['GenreEnsResp']}} {{$infos['PrenomEnsResp']}} {{$infos['NomEnsResp']}}" readonly>
                        <input type="hidden" name="GenreEnsResp" value="{{$infos['GenreEnsResp']}}">
                        <input type="hidden" name="PrenomEnsResp" value="{{$infos['PrenomEnsResp']}}">
                        <input type="hidden" name="NomEnsResp" value="{{$infos['NomEnsResp']}}">
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Courriel</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="EmailEnsResp" value="{{$infos['EmailEnsResp']}}" readonly>
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Téléphone</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="TelEnsResp" value="{{$infos['TelEnsResp']}}" readonly>
                    </div>
                </div>
                <hr class="hr-style"/>

                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Date du Stage</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" value="de {{$infos['DateDebut']}} au {{$infos['DateFin']}}" readonly>
                        <input type="hidden" name="DateDebut" value="{{$infos['DateDebut']}}">
                        <input type="hidden" name="DateFin" value="{{$infos['DateFin']}}">
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Durée hebdomadaire</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="DureeHebdo" value="{{$infos['DureeHebdo']}} H " readonly>
                        <input type="hidden" name="DureeHebdo" value="{{$infos['DureeHebdo']}}">
                        <input type="hidden" name="HoraireSpecif" value="{{$infos['HoraireSpecif']}}">
                    </div>
                </div>
                <hr class="hr-style"/>


                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Gratification</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static"  name="ssss" value="{{$infos['MontantGrat']}}  {{$infos['Devise']}} == {{$infos['MontantGratEuros']}} Euros " readonly>
                        <input type="hidden" name="MontantGrat" value="{{$infos['MontantGrat']}}">
                        <input type="hidden" name="Devise" value="{{$infos['Devise']}}">
                        <input type="hidden" name="MontantGratEuros" value="{{$infos['MontantGratEuros']}}">
                        <input type="hidden" name="ModaliteGrat" value="{{$infos['ModaliteGrat']}}">
                        <input type="hidden" name="AvantagesGrat" value="{{$infos['AvantagesGrat']}}">
                        <input type="hidden" name="FerieEventuel" value="{{$infos['FerieEventuel']}}">
                        <input type="hidden" name="info_complem" value="{{$infos['info_complem']}}">
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Description du stage</label>
                    <div class="col-sm-8 left">
                        <textarea class=" col-lg-10 form-control-static"   name="DescrStage" readonly>{{$infos['DescrStage']}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        {{--=END!!!!!!===========================Stage=================================--}}

        @if(isset($valide) && $valide == true)
            <h4 class="text text-success">Vos Saisis sont compléts, vous pouvez valider votre convention</h4>
            <button type="submit" name="oui_button" class="btn btn-green" >
                Valider
            </button>
        @else
            <h4 class="text-danger"> Tous les champs affichés au-dessus sont obligatoires, et il maque des champs à remplir!! <br/>Les données ci-dessus sont elles correctes?</h4>
        @endif




        {{--retourner à la page précendant--}}
        <a href="javascript:history.go(-1)" class="btn btn-danger">Modifier</a>

        <input type="submit" name="rengistrer_button" class="btn btn-warning" value="Enregistrer"><br/><br/>

    </form>
@endsection


