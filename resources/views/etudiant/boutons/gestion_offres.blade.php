<div class="btn btn-form btn-blue-view">
    <a href="{{ URL('login/offre_stage/'.$offre->id) }}">
        <span class="glyphicon glyphicon-fullscreen"></span>
        <span class="historique__list__item__control__name">Afficher</span>
    </a>
</div>

<div class="btn btn-form btn-orange-edit">
    <a href="{{URL('mailto:'.$offre->email.'?Subject=[Candidature%20des%20offres-stages]AdopteUnStage-Polytech')}}">
        <span class="glyphicon glyphicon-pencil"></span>
        <span class="historique__list__item__control__name">Postuler</span>
    </a>
</div>
