
<div class="btn btn-form btn-orange-edit">
    <a href="{{URL('etudiant/feedback/'.$feedback->id.'/edit')}}">
        <span class="glyphicon glyphicon-edit"></span>
        <span class="historique__list__item__control__name">Modifier</span>
    </a>
</div>
<div class="btn btn-form btn-blue-view">
    <a href="{{URL('etudiant/feedback/'.$feedback->id)}}">
        <span class="glyphicon glyphicon-fullscreen"></span>
        <span class="historique__list__item__control__name">Afficher</span>
    </a>
</div>
<div class="btn btn-form btn-red-trash">
    {!! Form::open(['route' =>['etudiant-delete-feedback', $feedback->id], 'method' => 'delete']) !!}
    <button type="submit"> <span class="glyphicon glyphicon-trash"></span> </button>
    <span class="historique__list__item__control__name">Effacer</span>
    {!! Form::close() !!}
</div>