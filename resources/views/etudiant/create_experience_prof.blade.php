@extends('etudiant.layout')

@section('title')
    Valider une expérience professionnelle
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Valider une expérience professionnelle</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Stage ouvrier: Valider une expérience professionnelle</span>
        </div>
    </div>
    <br/>
    <div class="alert alert-success"><strong>Attention</strong></br>
        <br/>
        <div >Ce formulaire permet de valider une expérience professionnelle comme stage ouvrier.</div>
        Vous devez aller jusqu'au bout de la saisie pour que les informations que vous avez saisies soient mémorisées.</br>
        <div class="required-info" >
            <strong>Les champs avec icon rouge(comme <span class="glyphicon glyphicon-home"></span>) sont obligatoires.</strong>
        </div>
    </div>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

        {{--==========================infos sur le stage ouvrier=====================--}}
        {!! Form::open( ['route' => 'experience_professionel_post', 'method'=>'post','class' => 'form','files' => true, 'enctype'=>'multipart/form-data']) !!}

            {{--============================Etudiant=================================--}}
            <div class="panel panel-success">
                <div class="panel-heading left">Etudiant</div>
                <br/>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                    <select class="form-control" name="Filiere" id="Filiere" required="required">
                        <option selected disabled> Sélectionner une filiere</option>
                        @foreach($filieres as $filiere)
                            <option value="{{ $filiere->filiere_nom }}"> {{ $filiere->filiere_nom }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-addon"><span>Filiere du stage</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>
                    <select class="form-control" name="GenreEtu" id="GenreEtu" required>
                        <option value="M."> M.</option>
                        <option value="Mme">Mme</option>
                        <option value="Mlle">Mlle</option>
                    </select>
                    <input class="form-control" type="text" name="NomEtu" id="NomEtu" placeholder="Nom" required/>
                    <input class="form-control" type="text" name="PrenomEtu" id="PrenomEtu" placeholder="Prenom" required/>
                    <div class="input-group-addon"><span>Etudiant Nom et Prénom</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></div>
                    <input class="form-control" type="email" name="EmailEtu" id="EmailEtu" placeholder="Courriel" required/>
                    <div class="input-group-addon"><span>Courriel</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-grain"></i></div>
                    <input class="form-control" type="text" name="NumSS" id="NumSS" placeholder="Numéro de sécurité sociale" required/>
                    <div class="input-group-addon"><span>Numéro de sécurité sociale</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-equalizer"></i></div>
                    <input class="form-control" type="text" name="NumEtu" id="NumEtu" placeholder="Numéro d'étudiant" required/>
                    <div class="input-group-addon"><span>Numéro d'étudiant</span></div>
                </div>
                <br/>
            </div>
            {{--=END!!!!!!===========================Etudiant==========================--}}


            {{--============================Entreprise=================================--}}
            <div class="panel panel-success">
                <div class="panel-heading left">Entreprise</div>
                <br/>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                    <input class="form-control" type="text" name="NomEntreprise" id="NomEntreprise" placeholder="Nom de l'entreprise" required/>
                    <div class="input-group-addon"><span>Nom de l'entreprise</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                    <input class="form-control" type="text" name="AdrSiegeSocial" id="AdrSiegeSocial" placeholder="Adresse du Siège Social" required/>
                    <div class="input-group-addon"><span>Adresse du Siège Social</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                    <input class="form-control" type="text" name="CPSiegeSocial" id="CPSiegeSocial" placeholder="Code Postal" required/>
                    <div class="input-group-addon"><span>Code Postal</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                    <input class="form-control" type="text" name="VilleSiegeSocial" id="VilleSiegeSocial" placeholder="Ville" required/>
                    <div class="input-group-addon"><span>Ville</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                    <input class="form-control" type="text" name="pays_ss" id="pays_ss" value="France "/>
                    <div class="input-group-addon"><span>Pays</span></div>
                </div>
                <br/>
            </div>
            {{--END!!!!!!============================Entreprise=========================--}}



            {{--============================Stage========================================--}}
            <div class="panel panel-success">
                <div class="panel-heading left">Stage</div>
                <br/>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-subtitles"></span></div>
                    <input class="form-control" type="text" name="TitreStage" id="TitreStage" placeholder="Intitulé du stage" required/>
                    <div class="input-group-addon"><span>Intitulé du stage</span></div>
                </div>

                <br/>
                <div class="left required-info blue-font" >
                    <span class="red-font"> S'agit-il d'un stage en laboratoire de recherche : </span>
                    <label class="radio-inline" >
                        <input type="radio" name="EstLabo" id="EstLabo" value="Oui" >
                        Oui
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="EstLabo" id="EstLabo" value="Non" checked>
                        Non
                    </label>
                </div>

                <div class="left required-info blue-font">
                    <span class="red-font">Est-ce à l'étranger :  </span>
                    <label class="radio-inline" >
                        <input type="radio" name="EstEtranger" id="EstEtranger" value="Oui" >
                        Oui
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="EstEtranger" id="EstEtranger" value="Non" checked>
                        Non
                    </label>
                </div>

                <br/>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-dashboard"></span></div>
                    <input class="form-control" type="text" name="DateDebut" id="DateDebut" placeholder="Date de début de stage(DD/MM/YYYY)" required/>
                    <div class="input-group-addon"><span>Date de début de stage(DD/MM/YYYY)</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-dashboard"></span></div>
                    <input class="form-control" type="text" name="DateFin" id="DateFin" placeholder="Date de fin de stage(DD/MM/YYYY)" required/>
                    <div class="input-group-addon"><span>Date de fin de stage(DD/MM/YYYY)</span></div>
                </div>
                <br/>

                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
                    <input type="file" name="Doc_justificatif" class="form-control" required />
                    <div class="input-group-addon"><span >Document justificatif</span></div>
                </div>
            </div>
            {{--END!!!!============================Stage=================================--}}

            <div class="form-group right-bg">
                {!! Form::submit('Valider', ['class' => 'btn-success btn']) !!}
            </div>
        {!! Form::close() !!}
@endsection


