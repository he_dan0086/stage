@extends('etudiant.layout')


@section('fil-ariane')
    @parent
@endsection

@section('content')

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    {{--==============================Titre ================================--}}
    <div class="alert alert-danger"><h3><span class="glyphicon glyphicon-warning-sign"></span> Page Non Autorisé</h3></br>
        <h4>Pour voir toutes les offres de stage, vous devez être un étudiant et avoir loggé sur l'application AdopteUnStage</h4>
        <br/>
        <h4>En tant qu'étudiant,
            <p>vous pouvez voir la liste des offres de stage, deposer votre cv....</p></h4>
    </div>

    <a type="button" class="btn btn-green" href="{{url(route('login_get'))}}">Connectez vous</a>
    <a type="button" class="btn btn-warning" href="{{url(route('inscription_etudiant'))}}">S'inscrire</a>

@endsection

