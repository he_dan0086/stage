@extends('template_panel')

@section('title')
    Panel étudiant
@endsection

@section('style_panel')
    <link rel="stylesheet" href="{{ asset('css/panel.css') }}">
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/libraries/isteven-multi-select.css') }}">
@endsection

@section('fil-ariane')
    <li><a href="{{ url(route('racine-etudiant')) }}"> Panel étudiant </a></li>
@endsection

@section('content')
    <div  ng-app="EtudiantApp">
        <div ng-view>
            @yield('panel_content')
        </div>
    </div>
@endsection

@section('menu')

    <li class="{{ (Request::url() == url(route('racine-etudiant'))) ? 'active':'' }}">
        <a href="{{ url('etudiant#/recherche-stage') }}">RECHERCHE STAGE</a>
    </li>

    <li class="{{ (Request::url() == url(route('racine-etudiant'))) ? 'active':'' }}">
        <a href="{{ url('etudiant#/feedbacks') }}">RECHERCHE FEEDBACKS</a>
    </li>

    <li class="{{ Request::is('etudiant/profil*') ? 'active':'' }}">
        <a href="{{ url(route('profil-etudiant')) }}">MON PROFIL</a>
    </li>

    <li class="{{ Request::is('etudiant/feedback-liste*') ? 'active':'' }}">
        <a href="{{ url(route('feedback-etudiant')) }}">GESTION DE MES FEEDBACKS</a>
    </li>

    <li class="{{ Request::is('etudiant/cv*') ? 'active':'' }}">
        <a href="{{ url(route('cv-etudiant')) }}">DEPOSER CV</a>
    </li>

    <li class="{{ Request::is('etudiant/convention*') ? 'active':'' }}">
        <a href="{{ url(route('convention_get')) }}">CONVENTION DE STAGE</a>
    </li>

    <li class="{{ Request::is('etudiant/experience-professionnel*') ? 'active':'' }}">
        <a href="{{ url(route('experience_professionel_get')) }}">EXPERIENCE PROFESIONNELLE</a>
    </li>

@endsection

@section('script_js')
    <script src="{{ asset('js/angular/directives/dirPagination.js') }}"></script>
    <script src="{{ asset('js/angular/directives/isteven-multi-select.js') }}"></script>

    <script src="{{ asset('js/angular/etudiantApp.js') }}"></script>
    <script src="{{ asset('js/angular/services/rechercheFeedback.js') }}"></script>
    <script src="{{ asset('js/angular/controllers/rechercheFeedback.js') }}"></script>
    <script src="{{ asset('js/angular/filters/customEntrepriseFilter.js') }}"></script>


    <script src="{{ asset('js/angular/services/rechercheStage.js') }}"></script>
    <script src="{{ asset('js/angular/controllers/rechercheStage.js') }}"></script>
    <script src="{{ asset('js/angular/filters/customNiveauFilter.js') }}"></script>
    <script src="{{ asset('js/angular/filters/customDepartementFilter.js') }}"></script>
    <script src="{{ asset('js/angular/filters/customCompetenceFilter.js') }}"></script>
@endsection
