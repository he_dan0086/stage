@extends('etudiant.layout')

@section('title')
    Creation Feedback
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Creation Feedback</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Creation Feedback</span>
        </div>
    </div>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    {!! Form::open(['route' => ['post-create-feedback'], 'method' => 'post',
     'xmlns' => 'http://www.w3.org/1999/html', 'class' => 'form-horizontal']) !!}

    <div class="panel panel-success">
        <div class="panel-heading left">Feedback</div>
        <div class="required-info"> * Les champs obligatoires</div>

        <div class="text-success" >
            <div class="form-group sans-margin-bottom" >
                <label class="col-sm-4 control-label">Entreprise <span class="text-danger">*</span></label>
                <div class="col-sm-8 left">
                    <input class=" col-lg-10 form-control-static" name="entreprise" value="{{old('entreprise')}}" required>
                </div>
            </div>
            <div class="form-group sans-margin-bottom"  >
                <label class="col-sm-4 control-label">Description de l'entreprise</label>
                <div class="col-sm-8 left">
                        <textarea class=" col-lg-10 form-control-static" name="descrption_entreprise" required >{{old('descrption_entreprise')}}</textarea><br/>
                </div>
            </div>
            <div class="form-group sans-margin-bottom">
                <label class="col-sm-4 control-label">Feedback sur le stage</label>
                <div class="col-sm-8 left">
                    <textarea class=" col-lg-10 form-control-static "  name="contenu" required>{{old('contenu')}}</textarea><br/>
                </div>
            </div>
            <div class="form-group sans-margin-bottom">
                <label class="col-sm-4 control-label">Feedback sur le recrutement</label>
                <div class="col-sm-8 left">
                    <textarea class=" col-lg-10 form-control-static "  name="recrutement_feedback">{{old('recrutement_feedback')}}</textarea><br/>
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label for="isOuvert">
                        <input type="checkbox" name="isOuvert"/>
                        Autorisez d'afficher mes informations du contact
                    </label>
                </div>
            </div>
        </div>
        <br/>
        <input type="submit" class="btn btn-success" value="ENREGISTRER">
    </div>
    <br/>
    {!! Form::close() !!}
@endsection


