@extends('etudiant.layout')

@section('title')
    Convention
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Convention</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="alert alert-success">
        <h4><span class="glyphicon glyphicon-ok-circle"></span>Félicitation</h4>
        <br/>Vous avez réussit de valider une expérience professionnelle sur AdopteUnStage
        <br/>

@endsection