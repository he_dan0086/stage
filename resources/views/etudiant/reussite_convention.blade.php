@extends('etudiant.layout')

@section('title')
    Convention
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Convention</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="alert alert-success">
        <h4>Félicitation</h4>
        <br/>Vous avez réussit d'établir une convention sur AdopteUnStage
        <br/>Soyez patient pendant un période de temps, le responsable va vous contacter dès que la convention est prêt!!!
    <br/>

@endsection