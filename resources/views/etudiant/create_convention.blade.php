@extends('etudiant.layout')

@section('title')
    Etablir une convention de stage
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/convention.css') }}">
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Etablir une convention de stage</li>
@endsection

@section('content')
    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')


    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Etablir une convention de stage</span>
        </div>
    </div>
    <br/>
    <div class="alert alert-info"><strong>Attention</strong></br>
        Vous devez aller jusqu'au bout de la saisie pour que les informations que vous avez saisies soient mémorisées.</br>
        <div class="required-info" >
            <strong>Les champs avec icon rouge(comme <span class="glyphicon glyphicon-home"></span>) sont obligatoires.</strong>
        </div>
    </div>

<div>
    {{--==========================infos sur la convention=====================--}}
    <form role="form" method="GET" action="{{url(route('convention_verifier'))}}" enctype="multipart/form-data"
          xmlns="http://www.w3.org/1999/html">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <!-- Nav tabs -->
        <ul class="nav nav-pills nav-justified" role="tablist" style="font-size: 20px">
            <li role="presentation" class="active"><a href="#etudiant" aria-controls="etudiant" role="tab" data-toggle="tab">Etudiant</a></li>
            <li role="presentation"><a href="#entreprise" aria-controls="entreprise" role="tab" data-toggle="tab">Entreprise</a></li>
            <li role="presentation"><a href="#stage" aria-controls="stage" role="tab" data-toggle="tab">Stage</a></li>
            <li role="presentation"><a href="#responsables" aria-controls="responsables" role="tab" data-toggle="tab">Responsables</a></li>
            <li role="presentation"><a href="#info_comple" aria-controls="info_comple" role="tab" data-toggle="tab">Infos Complémentaires</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            {{--============================Etudiant=================================--}}
            <div role="tabpanel" class="tab-pane fade in active" id="etudiant">
                <div class="panel panel-success">
                    <br/>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-home"></span></div>
                        <select class="form-control" name="Filiere" id="Filiere" >
                            <option selected disabled> Sélectionner une filiere</option>
                            @foreach($filieres as $filiere)
                                <option value="{{ $filiere->filiere_nom }}"> {{ $filiere->filiere_nom }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-addon "><span>Filiere du stage</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-user"></i></div>
                        <select class="form-control" name="GenreEtu" id="GenreEtu">
                            <option value="M." @if(isset($values['GenreEtu']) && $values['GenreEtu'] == 'M.') selected @endif> M.</option>
                            <option value="Mme" @if(isset($values['GenreEtu']) && $values['GenreEtu'] == 'Mme') selected @endif>Mme</option>
                            <option value="Mlle" @if(isset($values['GenreEtu']) && $values['GenreEtu'] == 'Mlle') selected @endif>Mlle</option>
                        </select>
                        <input class="form-control" type="text" name="NomEtu" id="NomEtu" placeholder="Nom"
                        @if(isset($values['NomEtu']))value="{{$values['NomEtu']}}" @else value="{{old('NomEtu')}}" @endif/>

                        <input class="form-control" type="text" name="PrenomEtu" id="PrenomEtu" placeholder="Prenom"
                               @if(isset($values['PrenomEtu']))value="{{$values['PrenomEtu']}}" @else value="{{old('PrenomEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Etudiant Nom et Prénom</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-envelope"></i></div>
                        <input class="form-control" type="email" name="EmailEtu" id="EmailEtu" placeholder="Courriel"
                               @if(isset($values['EmailEtu']))value="{{$values['EmailEtu']}}" @else value="{{old('EmailEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Courriel</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-grain"></i></div>
                        <input class="form-control" type="text" name="NumSS" id="NumSS" placeholder="Numéro de sécurité sociale"
                               @if(isset($values['NumSS']))value="{{$values['NumSS']}}" @else value="{{old('NumSS')}}" @endif/>
                        <div class="input-group-addon"><span>Numéro de sécurité sociale</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-equalizer"></i></div>
                        <input class="form-control" type="text" name="NumEtu" id="NumEtu" placeholder="Numéro d'étudiant"
                               @if(isset($values['NumEtu']))value="{{$values['NumEtu']}}" @else value="{{old('NumEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Numéro d'étudiant</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-leaf"></i></div>
                        <input class="form-control" type="text" name="DateNaissEtu" id="DateNaissEtu" placeholder="Date de naissance (DD/MM/YYYY)"
                               @if(isset($values['DateNaissEtu']))value="{{$values['DateNaissEtu']}}" @else value="{{old('DateNaissEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Date de naissance (DD/MM/YYYY)</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="TelEtu" id="TelEtu" placeholder="Téléphone"
                               @if(isset($values['TelEtu']))value="{{$values['TelEtu']}}" @else value="{{old('TelEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Téléphone(inclut que les numéros</span></div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    </br>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="AdressEtu" id="AdressEtu" placeholder="Adresse domiciel"
                               @if(isset($values['AdressEtu']))value="{{$values['AdressEtu']}}" @else value="{{old('AdressEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Adresse domiciel</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="CPEtu" id="CPEtu" placeholder="Code Postal"
                               @if(isset($values['CPEtu']))value="{{$values['CPEtu']}}" @else value="{{old('CPEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Code Postal</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="VilleEtu" id="VilleEtu" placeholder="Ville"
                               @if(isset($values['VilleEtu']))value="{{$values['VilleEtu']}}" @else value="{{old('VilleEtu')}}" @endif/>
                        <div class="input-group-addon"><span>Ville</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="pays_etudiant" id="pays_etudiant"
                               @if(isset($values['pays_etudiant']))value="{{$values['pays_etudiant']}}" @else value="France " @endif/>
                        <div class="input-group-addon"><span>Pays</span></div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    </br>
                    <p class="left" style="color:#337AB7;">Pour votre stage, vous devez être couvert contre le risque "responsabilité civile"</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="Assurance" id="Assurance" placeholder="Company d'assurance"
                               @if(isset($values['Assurance']))value="{{$values['Assurance']}}" @else value="{{old('Assurance')}}" @endif/>
                        <div class="input-group-addon"><span>Company d'assurance</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="AssuranceNo" id="AssuranceNo" placeholder="Numéro de police"
                               @if(isset($values['AssuranceNo']))value="{{$values['AssuranceNo']}}" @else value="{{old('AssuranceNo')}}" @endif/>
                        <div class="input-group-addon"><span>Numéro de police</span></div>
                    </div>
                    <br/>
                </div>
            </div>

            {{--=END!!!!!!===========================Etudiant=================================--}}

            {{--============================Entreprise=================================--}}
            <div role="tabpanel" class="tab-pane fade" id="entreprise">
                <div class="panel panel-success">
                    <br/>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-home"></span></div>
                        <input class="form-control" type="text" name="NomEntreprise" id="NomEntreprise" placeholder="Nom de l'entreprise"
                               @if(isset($values['NomEntreprise']))value="{{$values['NomEntreprise']}}" @else value="{{old('NomEntreprise')}}" @endif/>
                        <div class="input-group-addon"><span>Nom de l'entreprise</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-share"></span></div>
                        <input class="form-control" type="text" name="site_web" id="site_web"   placeholder="Site Web"
                               @if(isset($values['site_web']))value="{{$values['site_web']}}" @else value="http:://" @endif/>
                        <div class="input-group-addon"><span>Site Web</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="AdrSiegeSocial" id="AdrSiegeSocial" placeholder="Adresse du Siège Social"
                               @if(isset($values['AdrSiegeSocial']))value="{{$values['AdrSiegeSocial']}}" @else value="{{old('AdrSiegeSocial')}}"@endif/>
                        <div class="input-group-addon"><span>Adresse du Siège Social</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="CPSiegeSocial" id="CPSiegeSocial" placeholder="Code Postal"
                               @if(isset($values['CPSiegeSocial']))value="{{$values['CPSiegeSocial']}}" @else value="{{old('CPSiegeSocial')}}"@endif/>
                        <div class="input-group-addon"><span>Code Postal</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="VilleSiegeSocial" id="VilleSiegeSocial" placeholder="Ville"
                               @if(isset($values['VilleSiegeSocial']))value="{{$values['VilleSiegeSocial']}}" @else value="{{old('VilleSiegeSocial')}}"@endif/>
                        <div class="input-group-addon"><span>Ville</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="pays_ss" id="pays_ss"
                               @if(isset($values['pays_ss']))value="{{$values['pays_ss']}}" @else value="France" @endif/>
                        <div class="input-group-addon"><span>Pays</span></div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    </br>
                    <p class="left" style="color:#337AB7;">Adresse du service de déroulement du stage</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="Adresse1Stage" id="Adresse1Stage" placeholder="Adresse du service dedéroulement du stage"
                               @if(isset($values['Adresse1Stage']))value="{{$values['Adresse1Stage']}}" @else value="{{old('Adresse1Stage')}}"@endif/>
                        <div class="input-group-addon"><span>Adresse du service de déroulement du stage</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="CPStage" id="CPStage" placeholder="Code Postal"
                               @if(isset($values['CPStage']))value="{{$values['CPStage']}}" @else value="{{old('CPStage')}}"@endif/>
                        <div class="input-group-addon"><span>Code Postal</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="VilleStage" id="VilleStage" placeholder="Ville"
                               @if(isset($values['VilleStage']))value="{{$values['VilleStage']}}" @else value="{{old('VilleStage')}}"@endif/>
                        <div class="input-group-addon"><span>Ville</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></div>
                        <input class="form-control" type="text" name="pays" id="pays"
                               @if(isset($values['pays']))value="{{$values['pays']}}" @else  value="France" @endif/>
                        <div class="input-group-addon"><span>Pays</span></div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    </br>
                    <p class="left" style="color:#337AB7;">Directeur / Représentant</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-user"></i></div>
                        <select class="form-control" name="GenreDirEnt" id="GenreDirEnt" >
                            <option value="M." @if(isset($values['GenreDirEnt']) && $values['GenreDirEnt'] == 'M.') selected @endif> M.</option>
                            <option value="Mme" @if(isset($values['GenreDirEnt']) && $values['GenreDirEnt'] == 'Mme') selected @endif>Mme</option>
                            <option value="Mlle" @if(isset($values['GenreDirEnt']) && $values['GenreDirEnt'] == 'Mlle') selected @endif>Mlle</option>
                        </select>
                        <input class="form-control" type="text" name="NomDirEnt" id="NomDirEnt" placeholder="Nom"
                               @if(isset($values['NomDirEnt']))value="{{$values['NomDirEnt']}}" @else value="{{old('NomDirEnt')}}"@endif/>
                        <input class="form-control" type="text" name="PrenomDirEnt" id="PrenomDirEnt" placeholder="Prenom"
                               @if(isset($values['PrenomDirEnt']))value="{{$values['PrenomDirEnt']}}" @else value="{{old('PrenomDirEnt')}}"@endif/>
                        <div class="input-group-addon"><span>Etudiant Nom et Prénom</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-envelope"></i></div>
                        <input class="form-control" type="email" name="EmailDirEnt" id="EmailDirEnt" placeholder="Courriel"
                               @if(isset($values['EmailDirEnt']))value="{{$values['EmailDirEnt']}}" @else value="{{old('EmailDirEnt')}}"@endif/>
                        <div class="input-group-addon"><span>Courriel</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="TelDirEnt" id="TelDirEnt" placeholder="Téléphone"
                               @if(isset($values['TelDirEnt']))value="{{$values['TelDirEnt']}}" @else value="{{old('TelDirEnt')}}"@endif/>
                        <div class="input-group-addon"><span>Téléphone</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="QualiteDirEnt" id="QualiteDirEnt" placeholder="Agissant en qualité de"
                               @if(isset($values['QualiteDirEnt']))value="{{$values['QualiteDirEnt']}}" @else value="{{old('QualiteDirEnt')}}"@endif/>
                        <div class="input-group-addon"><span>agissant en qualité de</span></div>
                    </div>
                    <br/>
                </div>
            </div>
            {{--END!!!!!!============================Entreprise=================================--}}


            {{--============================Stage=================================--}}
            <div role="tabpanel" class="tab-pane fade" id="stage">
                <div class="panel panel-success">
                    <br/>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-dashboard"></span></div>
                        <input class="form-control" type="text" name="DateDebut" id="DateDebut" placeholder="Date de début de stage(DD/MM/YYYY)"
                               @if(isset($values['DateDebut']))value="{{$values['DateDebut']}}" @else value="{{old('DateDebut')}}"@endif/>
                        <div class="input-group-addon"><span>Date de début de stage(DD/MM/YYYY)</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-dashboard"></span></div>
                        <input class="form-control" type="text" name="DateFin" id="DateFin" placeholder="Date de fin de stage(DD/MM/YYYY)"
                               @if(isset($values['DateFin']))value="{{$values['DateFin']}}" @else value="{{old('DateFin')}}"@endif/>
                        <div class="input-group-addon"><span>Date de fin de stage(DD/MM/YYYY)</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-time"></i></div>
                        <input class="form-control" type="number" min="0" name="DureeHebdo" id="DureeHebdo" placeholder="Durée hebdomadaire du stage"
                               @if(isset($values['DureeHebdo']))value="{{$values['DureeHebdo']}}" @else value="{{old('DureeHebdo')}}"@endif/>
                        <div class="input-group-addon"><span>(Heures) Durée hebdomadaire du stage</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></div>
                        <textarea class="form-control" name="HoraireSpecif" id="HoraireSpecif">@if(isset($values['HoraireSpecif'])) {{$values['HoraireSpecif']}} @else Horaire Sépciale @endif</textarea>
                        <div class="input-group-addon"><span>Horaires spéciales du stage qui sont <p>différents des horaires habituels de l'entreprise</p></span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></div>
                        <textarea class="form-control" name="FerieEventuel" id="FerieEventuel">@if(isset($values['FerieEventuel'])) {{$values['FerieEventuel']}} @else Ferié Eventuel @endif</textarea>
                        <div class="input-group-addon"><span>Cas échéant, la présence du stagiaire dans l'entreprise ou </span>
                                <p>l'organisme d'accueil la nuit, le dimanche ou un jour férié</p></div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    <br/>
                    <p class="left blue-font" >Pour un stage en France, lorsque la durée du stage est supérieure à deux mois consécutifs (ou non), celui-ci fait l'objet d'une gratification, et qui sera due à compter du premier jour du premier mois de stage et devra faire l'objet d'un versement mensuel.</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-euro"></span></div>
                        <input class="form-control" type="number" min="0" name="MontantGrat" id="MontantGrat" placeholder="Montant"
                               @if(isset($values['MontantGrat']))value="{{$values['MontantGrat']}}" @else value="{{old('MontantGrat')}}"@endif/>
                        <div class="input-group-addon"><span>Montant de la gratification</span></div>
                        <input class="form-control" type="text" name="Devise" id="Devise"
                               @if(isset($values['Devise']))value="{{$values['Devise']}}" @else value="Euros"  @endif/>
                        <div class="input-group-addon"><span>Devise</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-euro"></span></div>
                        <input class="form-control" type="number" name="MontantGratEuros" id="MontantGratEuros" placeholder="Soit à combien d'euros"
                               @if(isset($values['MontantGratEuros']))value="{{$values['MontantGratEuros']}}" @else value="{{old('MontantGratEuros')}}"@endif/>
                        <div class="input-group-addon"><span>Euros</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-shopping-cart"></i></div>
                        <input class="form-control" type="text" name="ModaliteGrat" id="ModaliteGrat" placeholder="Modalités de versement"
                               @if(isset($values['ModaliteGrat']))value="{{$values['ModaliteGrat']}}" @else value="Par Carte"@endif/>
                        <div class="input-group-addon"><span>Modalités de versement</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></div>
                        <textarea class="form-control" name="AvantagesGrat" id="AvantagesGrat">@if(isset($values['AvantagesGrat'])) {{$values['AvantagesGrat']}} @else Avantage @endif</textarea>
                        <div class="input-group-addon">
                            <span>Avantages offerts par l'entreprise
                                <p>(restauration, hébergement, remboursement de frais, ...)</p>
                            </span>
                        </div>
                    </div>
                    <br/>
                </div>

                <div class="panel panel-success">
                    <br/>
                    <div class="left required-info blue-font" >
                        <span class="red-font"> S'agit-il d'un stage en laboratoire de recherche : </span>
                        <label class="radio-inline" >
                            <input type="radio" name="EstLabo" id="EstLabo" value="Oui" >
                            Oui
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="EstLabo" id="EstLabo" value="Non" checked>
                            Non
                        </label>
                    </div>

                    <div class="left required-info blue-font">
                        <span class="red-font">Est-ce à l'étranger :  </span>
                        <label class="radio-inline" >
                            <input type="radio" name="EstEtranger" id="EstEtranger" value="Oui" >
                            Oui
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="EstEtranger" id="EstEtranger" value="Non" checked>
                            Non
                        </label>
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><span class="glyphicon glyphicon-subtitles"></span></div>
                        <input class="form-control" type="text" name="TitreStage" id="TitreStage" placeholder="Intitulé du stage"
                               @if(isset($values['TitreStage']))value="{{$values['TitreStage']}}" @else value="{{old('TitreStage')}}"@endif/>
                        <div class="input-group-addon"><span>Intitulé du stage</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-edit"></i></div>
                        <textarea class="form-control" name="DescrStage" id="DescrStage">@if(isset($values['DescrStage'])) {{$values['DescrStage']}} @else {{old('DescrStage')}} @endif</textarea>
                        <div class="input-group-addon">
                            <span>Description du subjet
                            </span>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
            {{--END!!!!============================Stage=================================--}}


            {{--============================Responsable=================================--}}
            <div role="tabpanel" class="tab-pane fade" id="responsables">
                <div class="panel panel-success">
                    </br>
                    <p class="left" style="color:#337AB7;">Encadrant dans l'entreprise</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-user"></i></div>
                        <select class="form-control" name="GenreEncadreur" id="GenreEncadreur" >
                            <option value="M." @if(isset($values['GenreEncadreur']) && $values['GenreEncadreur'] == 'M.') selected @endif> M.</option>
                            <option value="Mme" @if(isset($values['GenreEncadreur']) && $values['GenreEncadreur'] == 'Mme') selected @endif>Mme</option>
                            <option value="Mlle" @if(isset($values['GenreEncadreur']) && $values['GenreEncadreur'] == 'Mlle') selected @endif>Mlle</option>
                        </select>
                        <input class="form-control" type="text" name="NomEncadreur" id="NomEncadreur" placeholder="Nom"
                               @if(isset($values['NomEncadreur']))value="{{$values['NomEncadreur']}}" @else value="{{old('NomEncadreur')}}"@endif/>
                        <input class="form-control" type="text" name="PrenomEncadreur" id="PrenomEncadreur" placeholder="Prenom"
                               @if(isset($values['PrenomEncadreur']))value="{{$values['PrenomEncadreur']}}" @else value="{{old('PrenomEncadreur')}}"@endif/>
                        <div class="input-group-addon"><span>Etudiant Nom et Prénom</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-envelope"></i></div>
                        <input class="form-control" type="email" name="EmailEncadreur" id="EmailEncadreur" placeholder="Courriel"
                               @if(isset($values['EmailEncadreur']))value="{{$values['EmailEncadreur']}}" @else value="{{old('EmailEncadreur')}}"@endif/>
                        <div class="input-group-addon"><span>Courriel</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="TelEncadreur" id="TelEncadreur" placeholder="Téléphone"
                               @if(isset($values['TelEncadreur']))value="{{$values['TelEncadreur']}}" @else value="{{old('TelEncadreur')}}"@endif/>
                        <div class="input-group-addon"><span>Téléphone</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="FctEncadreur" id="FctEncadreur" placeholder="Fonction de l'encadreur dans l'entreprise"
                               @if(isset($values['FctEncadreur']))value="{{$values['FctEncadreur']}}" @else value="{{old('FctEncadreur')}}"@endif/>
                        <div class="input-group-addon"><span>Fonction de l'encadreur dans l'entreprise</span></div>
                    </div>
                    <br/>
                </div>
                <div class="panel panel-success">
                    </br>
                    <p class="left blue-font" >Tuteur enseignant</p>
                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-user"></i></div>
                        <select class="form-control" name="GenreEnsResp" id="GenreEnsResp">
                            <option value="M." @if(isset($values['GenreEnsResp']) && $values['GenreEnsResp'] == 'M.') selected @endif> M.</option>
                            <option value="Mme" @if(isset($values['GenreEnsResp']) && $values['GenreEnsResp'] == 'Mme') selected @endif>Mme</option>
                            <option value="Mlle" @if(isset($values['GenreEnsResp']) && $values['GenreEnsResp'] == 'Mlle') selected @endif>Mlle</option>
                        </select>
                        <input class="form-control" type="text" name="NomEnsResp" id="NomEnsResp" placeholder="Nom"
                               @if(isset($values['NomEnsResp']))value="{{$values['NomEnsResp']}}" @else value="{{old('NomEnsResp')}}"@endif/>
                        <input class="form-control" type="text" name="PrenomEnsResp" id="PrenomEnsResp" placeholder="Prenom"
                               @if(isset($values['PrenomEnsResp']))value="{{$values['PrenomEnsResp']}}" @else value="{{old('PrenomEnsResp')}}"@endif/>
                        <div class="input-group-addon"><span>Etudiant Nom et Prénom</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-envelope"></i></div>
                        <input class="form-control" type="email" name="EmailEnsResp" id="EmailEnsResp" placeholder="Courriel"
                               @if(isset($values['EmailEnsResp']))value="{{$values['EmailEnsResp']}}" @else value="{{old('EmailEnsResp')}}"@endif/>
                        <div class="input-group-addon"><span>Courriel</span></div>
                    </div>
                    <br/>

                    <div class="input-group">
                        <div class="input-group-addon required-champ"><i class="glyphicon glyphicon-phone"></i></div>
                        <input class="form-control" type="text" name="TelEnsResp" id="TelEnsResp" placeholder="Téléphone"
                               @if(isset($values['TelEnsResp']))value="{{$values['TelEnsResp']}}" @else value="{{old('TelEnsResp')}}"@endif/>
                        <div class="input-group-addon"><span>Téléphone</span></div>
                    </div>
                    <br/>
                </div>
            </div>
            {{--END!!!============================Responsable=================================--}}


            {{--============================Infos complementaire=================================--}}
            <div role="tabpanel" class="tab-pane" id="info_comple">
                <div class="panel panel-success">
                    </br>
                    <span class="left blue-font">Normalement vous ne devriez rien saisir ci-dessous. Toutefois, si vous avez des informations complémentaires à nous communiquer ou s'il y a une information que vous avez donnée qui est sujette à modification, veuillez l'indiquer ici </span>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-edit"></i></div>
                        <textarea class="form-control" name="info_complem" id="info_complem" style="height: 200px;">@if(isset($values['info_complem'])) {{$values['info_complem']}} @else Informations complétaires @endif</textarea>
                        <div class="input-group-addon">
                            <span>Informations complémentaires
                            </span>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
            {{--END!!!============================Infos complementaire=================================--}}
        </div>

        <input type="submit" class="btn btn-success" value="Suivant">
    </form>
</div>
@endsection


