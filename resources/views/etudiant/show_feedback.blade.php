@extends('etudiant.layout')

@section('title')
    Feedback
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/offre_stage.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Feedback</li>
@endsection

@section('content')

    @include('generale.flash_message')

    <br/><br/>

    <div class="etudiant">

        <div class="historique__header">
            <div class="historique__header__title">
                <span class="historique__header__title__span">
                    <a href="javascript:history.go(-1)" class="btn-float-action btn-green"　style="padding: 16px 18px;"><i class="glyphicon glyphicon-backward"></i></a>
                    Feedback
                    @if($feedback->isOuvert)
                        de {{ $etudiant->nom }} {{$etudiant->prenom}} en {{$etudiant->filiere->filiere_nom}}
                    @endif
                </span>
            </div>
            @if($feedback->isOuvert)
            <div class="historique__header__add">
                <span>Contacter</span>
                <a href="mailto:{{ $user->email }}" class="btn-float-action btn-green" style="padding: 16px 18px;">
                    <i class="fa fa-envelope-o"></i>
                </a>
            </div>
            @endif
        </div>

        <br/><br/>
        <div class="profile__identity offre__block">
            <div class="offre__entreprise__header">
                <div class="offre__entreprise__header__title">
                    <span class="offre__entreprise__header__name">Comment l'entreprise ?</span>
                </div>
            </div>
            <br/>
            <div class="offre__entreprise__content">
                <ul>
                    <li>
                        <span class="title">Entreprise : </span>
                        <span class="contenu">{{$feedback->entreprise}}</span>
                    </li>
                    <li>
                        <span class="title">Description de l'entreprise : </span>
                        <span class="contenu">{{$feedback->	descrption_entreprise}}</span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="profile__identity offre__block">
            <div class="offre__entreprise__header">
                <div class="offre__entreprise__header__title">
                    <span class="offre__entreprise__header__name">Comment le stage ?</span>
                </div>
            </div>
            <br/>
            <div class="offre__entreprise__content">
                <ul>
                    <li>
                        <span class="title">Feedback : </span>
                        <span class="contenu">{{$feedback->contenu}}</span>
                    </li>
                    <li>
                        <span class="title">Recrutement </span>
                        <span class="contenu">{{$feedback->recrutement_feedback}}</span>
                    </li>
                </ul>
            </div>
        </div>

        @if($feedback->isOuvert)
            <div class="profile__identity offre__block">
                <div class="offre__entreprise__header">
                    <div class="offre__entreprise__header__title">
                        <span class="offre__entreprise__header__name">Etudiant Information</span>
                    </div>
                </div>
                <br/>
                <div class="offre__entreprise__content">
                    <ul>
                        <li>
                            <span class="title">Filiere : </span>
                            <span class="contenu">{{$etudiant->filiere->filiere_nom}}</span>
                        </li>
                        <li>
                            <span class="title">Etudiant :</span>
                            <span class="contenu">{{$etudiant->nom}} {{$etudiant->prenom}}</span>
                        </li>
                        <li>
                            <span class="title">Telephone :</span>
                            <span class="contenu">{{$etudiant->telephone}} </span>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </div>
@endsection