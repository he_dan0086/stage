@extends('admin.layout')

@section('title')
    Liste de Etudiants
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Etudiants</li>
@endsection

@section('content')
    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <h3>Liste des étudiants</h3>
                <br/>


                @if($etudiants->first()!=null)
                <div class="table-responsive table black-font">
                    <table id="stages_ouvriers_table" class="table table-bordred table-striped tablesorter">
                        <thead>
                        <th>Filiere<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>Nom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>Prenom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>email<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                        <th>actions</th>
                        </thead>

                        <tbody>
                        @foreach($etudiants as $etudiant)
                            <tr>
                                @if(isset($etudiant->filiere->filiere_nom))
                                    <td data-sortable="true">{{$etudiant->filiere->filiere_nom}}</td>
                                @endif
                                <td data-sortable="true">{{$etudiant->nom}}</td>
                                <td data-sortable="true">{{$etudiant->prenom}}</td>
                                <td data-sortable="true">{{$etudiant->user->email}}</td>
                                <td data-sortable="true">
                                    <p data-placement="top" data-toggle="tooltip">
                                        <button data-target="#edit_{{ $etudiant->id }}" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"  >
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{ $etudiant->id }}" >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </p>
                                </td>
                            </tr>


                            {{--========================= edit modal fade ================================--}}
                            <div class="modal fade" id="edit_{{ $etudiant->id }}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    {!! Form::open(['route' => ['etudiant_update',$etudiant->id], 'method'=>'post']) !!}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                            <h4 class="modal-title custom_align black-font" id="Heading">Edit Informations de l'étudiant</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-star-empty"></span></div>
                                                <select class="form-control" name="Filiere" id="Filiere" required="required">
                                                    <option selected disabled> Sélectionner une filiere</option>
                                                    @foreach($filieres as $filiere)
                                                        <option value="{{ $filiere->id }}"> {{ $filiere->filiere_nom }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group-addon">Filiere</div>
                                            </div>
                                            <div class="form-group input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                                <input class="form-control" type="text" name="nom" placeholder="nom" value="{{$etudiant->nom}}" required/>
                                                <div class="input-group-addon">NOM</div>
                                            </div>
                                            <div class="form-group input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                                <input class="form-control" type="text" name="prenom" placeholder="prenom" value="{{$etudiant->prenom}}" required/>
                                                <div class="input-group-addon">PRENOM</div>
                                            </div>
                                            <div class="form-group input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                                <input class="form-control" type="email" name="email" placeholder="email" value="{{$etudiant->user->email}}" required/>
                                                <div class="input-group-addon">EMAIL</div>
                                            </div>
                                        </div>

                                        <div class="modal-footer ">
                                            <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            {{--!!!========================= edit modal fade ================================--}}



                            {{--========================= delete modal fade ================================--}}
                            <div class="modal fade" id="delete_{{$etudiant->id}}" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <!-- /.modal-content -->
                                    {!! Form::open(['route' =>['etudiant_delete',$etudiant->id ], 'method' => 'delete']) !!}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                            <h4 class="modal-title custom_align black-font" id="Heading">Supprimer ce record dans la base de donnée</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                                        </div>
                                        <div class="modal-footer ">
                                            <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="clearfix"></div>
                    <ul class="pagination pull-right">
                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    </ul>
                </div>
                    @else
                    <h3 class="text-danger"> Aucun étudiant inscrit sur AdopteUnStage</h3>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script_js')
    <script>
        $(document).ready(function(){
            $("#stages_ouvriers_table").tablesorter();
        });
    </script>

@endsection