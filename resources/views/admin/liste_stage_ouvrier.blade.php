@extends('admin.layout')

@section('title')
    Liste de Conventions
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Conventions</li>
@endsection

@section('content')
    @include('generale.form_notification')
    @include('generale.flash_message')


    <h3>Liste de conventions</h3>

    @if($stageouvriers->first() != null)
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <div class="table-responsive table black-font">
                    <table id="stages_ouvriers_table" class="table table-bordred table-striped tablesorter">
                        <thead >
                            {{--<th><input type="checkbox" id="checkall" /></th>--}}
                            <th>Date<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Filiere<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Nom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Prenom<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>stage_ouvrier_info</th>
                            <th> doc_justicatif</th>
                            <th>actions</th>
                        </thead>

                        <tbody>
                            @foreach($stageouvriers as $stageouvrier)
                                <tr>
                                    {{--<th><input type="checkbox" id="checkthis" /></th>--}}
                                    <td data-sortable="true">{{$stageouvrier->created_at}}</td>
                                    <td data-sortable="true">{{$stageouvrier->filiere}}</td>
                                    <td data-sortable="true">{{$stageouvrier->nom}}</td>
                                    <td data-sortable="true">{{$stageouvrier->prenom}}</td>
                                    <td data-sortable="true">
                                        @if($stageouvrier->infos_stage_ouvrier)
                                            <p data-placement="top" data-toggle="tooltip">
                                                <a href="{{url("administrateur/stageouvrier/download/".$stageouvrier->id."/infos_stage_ouvrier")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>
                                                <button data-target="#edit_{{ $stageouvrier->id }}_infos_stage_ouvrier" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"  >
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                </button>
                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{ $stageouvrier->id }}_infos_stage_ouvrier" >
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                            </p>
                                        @else
                                            ----
                                        @endif
                                    </td>
                                    <td data-sortable="true">
                                        @if($stageouvrier->doc_justicatif)
                                            <p data-placement="top" data-toggle="tooltip">
                                                <a href="{{url("administrateur/stageouvrier/download/".$stageouvrier->id."/doc_justicatif")}}" class="btn btn-success btn-xs">
                                                    <span class="glyphicon glyphicon-fullscreen"></span>
                                                </a>

                                                <button data-target="#edit_{{ $stageouvrier->id }}_doc_justicatif" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" >
                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                </button>

                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$stageouvrier->id}}_doc_justicatif" >
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                            </p>
                                        @else
                                            ----
                                        @endif
                                    </td>

                                    <td data-sortable="true">
                                        <p data-placement="top" data-toggle="tooltip">
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$stageouvrier->id}}" >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </p>
                                    </td>
                                </tr>




                                {{--========================= edit modal fade ================================--}}
                                <div class="modal fade" id="edit_{{ $stageouvrier->id }}_infos_stage_ouvrier" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.upload_conv', [
                                            'route' =>'admin_upload_stage_ouvrier',
                                           'lang'  => 'infos_stage_ouvrier',
                                           'id'  => $stageouvrier->id
                                         ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="edit_{{ $stageouvrier->id }}_doc_justicatif" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.upload_conv', [
                                        'route' =>'admin_upload_stage_ouvrier',
                                           'lang'  => 'doc_justicatif',
                                           'id'  => $stageouvrier->id
                                         ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                {{--!!!========================= edit modal fade ================================--}}



                                {{--========================= delete modal fade ================================--}}
                                <div class="modal fade" id="delete_{{$stageouvrier->id}}_infos_stage_ouvrier" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.delete_conv', [
                                           'route' =>'admin_delete_stage_ouvrier',
                                           'lang'  => 'infos_stage_ouvrier',
                                           'id'  => $stageouvrier->id
                                         ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <div class="modal fade" id="delete_{{$stageouvrier->id}}_doc_justicatif" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.delete_conv', [
                                            'route' =>'admin_delete_stage_ouvrier',
                                           'lang'  => 'doc_justicatif',
                                           'id'  => $stageouvrier->id
                                         ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>


                                <div class="modal fade" id="delete_{{$stageouvrier->id}}" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.delete_conv', [
                                        'route' =>'admin_delete_stage_ouvrier',
                                           'lang'  => 'stageouvrier',
                                           'id'  => $stageouvrier->id
                                         ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="clearfix"></div>
                    <ul class="pagination pull-right">
                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    @else
        <h3 class="text-danger">Oups! Il n'y aucun record de stage ouvrier pour l'instant!</h3>
    @endif
@endsection

@section('script_js')
    <script>
    $(document).ready(function(){
        $("#stages_ouvriers_table").tablesorter();
    });
    </script>

@endsection