@extends('admin.layout')

@section('title')
    Gestion des Responsables
@endsection

@section('style')
    <link rel="stylesheet" href="{{ url('css/crud_list.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Gestion de Responsables</li>
@endsection

@section('panel_content')

    @include('generale.form_notification')
    @include('generale.flash_message')

    @if($responsables->first() != null)
        <h3>Liste des responsables</h3><br/>

        <div class="crud-list">
            @foreach($responsables as $responsable)
                <div class="crud-list-item">
                    <div class="crud-list-left update-form-to-add">
                        <span style="color: #33A4B7;" aria-hidden="true">{{$responsable->filiere->filiere_nom}} </span>
                        <span class="crud-list-name">
                            <span class="espace-right"><strong>NOM:</strong> {{$responsable->nom}}</span>
                            <span class="espace-right"><strong>PRENOM:</strong> {{$responsable->prenom}}</span>
                            <span class="espace-right"><strong>Email:</strong> {{$responsable->user->email}}</span>
                        </span>
                    </div>

                    <div class="crud-list-right">
                        <a class="crud-list-update btn btn-warning add-resp-update-form"
                           update="{{ 'responsable_update-'.$responsable->id }}" id="{{$responsable->id}}">Modifier</a>
                        <a class="crud-list-delete btn btn-danger"
                           href="{{ route('responsable_delete', $responsable->user->id) }}">Supprimer</a>
                    </div>
                </div>

                <div id="{{'crud-resp-list-item-'.$responsable->id}}" class="crud-resp-list-item">
                    <form action="{{ route('responsable_update', $responsable->id) }}" method="post" class="form-inline">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="input-group">
                            <select class="form-control" name="Filiere" id="Filiere" required="required">
                                <option selected disabled> Sélectionner une filiere</option>
                                @foreach($filieres as $filiere)
                                    <option value="{{ $filiere->id }}"> {{ $filiere->filiere_nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group">
                            <input class="form-control" type="text" name="nom" placeholder="nom" value="{{$responsable->nom}}" required/>
                            <div class="input-group-addon">NOM</div>
                        </div>
                        <div class="input-group">
                            <input class="form-control" type="text" name="prenom" placeholder="prenom" value="{{$responsable->prenom}}" required/>
                            <div class="input-group-addon">PRENOM</div>
                        </div>
                        <div class="input-group">
                            <input class="form-control" type="text" name="email" placeholder="email" value="{{$responsable->user->email}}" required/>
                            <div class="input-group-addon">EMAIL</div>
                        </div>
                        <button class="btn btn-primary"  type="submit">Envoyer</button>
                    </form>
                </div>
            @endforeach
        </div>
        @else
        <h3 class="text-danger">Aucun record de responsables</h3>
    @endif

    <hr/>
    <br/>
    <br/>
    <h3>Ajouter un nouveau responsable</h3><br/>

    {!! Form::open(['route' => 'responsable_post', 'class' => 'form-inline']) !!}
    <div class="form-group">
        <div class="input-to-add">
            <div class="input-group">
                <select class="form-control" name="Filiere[0]" required="required">
                    <option selected disabled> Sélectionner une filiere</option>
                    @foreach($filieres as $filiere)
                        <option value="{{ $filiere->id }}"> {{ $filiere->filiere_nom }}</option>
                    @endforeach
                </select>
                <div class="input-group-addon">Filiere</div>
            </div>
            <br/>
            <br/>
            <div class="input-group">
                <div name="moderateur-to-ajout">
                    <input class="form-control" type="text" name="nom[0]" placeholder="nom"  required/>
                    <input class="form-control" type="text" name="prenom[0]" placeholder="prenom"  required/>
                    <input class="form-control" type="text" name="email[0]" placeholder="email"  required/>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <a class="btn btn-link" id="add-input">Ajouter un champs</a>
    {!! Form::submit('Envoyer', ['class' => 'btn-form']) !!}
    {!! Form::close() !!}
@endsection



@section('script_js')
    <script src="{{ url('js/add_input.js') }}"></script>
    <script src="{{ url('js/add_update_form.js') }}"></script>
@endsection
