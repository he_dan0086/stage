@extends('template_panel')

@section('title')
    Panel Administrateur
@endsection

@section('fil-ariane')
    <li><a href="{{ url(route('accueil-administrateur')) }}"> Panel administrateur </a></li>
@endsection

@section('style_panel')
    <link rel="stylesheet" href="{{ asset('css/panel-admin.css') }}">
@endsection

@section('content')
   @yield('panel_content')
@endsection

@section('menu')
   <li class="{{ Request::is('administrateur/identite-ecole*') ? 'active':'' }}">
       <a href="{{ url(route('admin_identite_ecole_get')) }}">PROFIL DE L'ECOLE</a>
   </li>

   <li class="{{ Request::is('administrateur/niveaux*') ? 'active':'' }}">
       <a href="{{ url(route('admin_niveaux_get')) }}">NIVEAUX</a>
   </li>

   <li class="{{ Request::is('administrateur/departements*') ? 'active':'' }}">
       <a href="{{ url(route('admin_departement_get')) }}">DEPARTEMENTS</a>
   </li>

   <li class="{{ Request::is('administrateur/filieres*') ? 'active':'' }}">
       <a href="{{ url(route('admin_filiere_get')) }}">FILIERES</a>
   </li>

   <li class="{{ Request::is('administrateur/responsables*') ? 'active':'' }}">
       <a href="{{ url(route('responsable_get')) }}">RESPONSABLES</a>
   </li>

   <li class="{{ Request::is('administrateur/etudiants*') ? 'active':'' }}">
       <a href="{{ url(route('etudiant_get')) }}">ETUDIANTS</a>
   </li>

   <li class="{{ Request::is('administrateur/entreprises*') ? 'active':'' }}">
       <a href="{{ url(route('entreprise_get')) }}">ENTREPRISES</a>
   </li>

   <li class="{{ Request::is('administrateur/feedbacks*') ? 'active':'' }}">
       <a href="{{ url(route('admin_gestion_feedback_get')) }}">FEEDBACKS</a>
   </li>

   <li class="{{ Request::is('administrateur/conventions*') ? 'active':'' }}">
       <a href="{{ url(route('admin_gestion_convention_get')) }}">CONVENTIONS</a>
   </li>

   <li class="{{ Request::is('administrateur/stageouvrier*') ? 'active':'' }}">
       <a href="{{ url(route('admin_gestion_stageouvrier_get')) }}">STAGES OUVRIERS</a>
   </li>

   <li class="{{ Request::is('administrateur/offre_stage*') ? 'active':'' }}">
       <a href="{{ url(route('admin_gestion_offre_get')) }}">OFFRES DE STAGES</a>
   </li>

   <li class="{{ Request::is('administrateur/upload_template*') ? 'active':'' }}">
       <a href="{{ url(route('admin_upload_template_get')) }}">TEMPLATES DE CONVENTION</a>
   </li>


@endsection