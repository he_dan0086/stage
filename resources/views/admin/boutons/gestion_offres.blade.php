<div class="btn btn-form btn-blue-view">
    <a href="{{ URL('administrateur/offre_stage/'.$offre->id) }}">
        <span class="glyphicon glyphicon-fullscreen"></span>
        <span class="historique__list__item__control__name">Afficher</span>
    </a>
</div>

<div class="btn btn-form btn-orange-edit">
    <a href="{{ URL('administrateur/offre_stage/'.$offre->id.'/edit') }}">
        <span class="glyphicon glyphicon-pencil"></span>
        <span class="historique__list__item__control__name">Editer</span>
    </a>
</div>

<div class="btn btn-form btn-red-trash">
    {!! Form::open(['route' =>['administrateur.offre_stage.destroy', $offre->id], 'method' => 'delete']) !!}
        <button type="submit"> <span class="glyphicon glyphicon-trash"></span> </button>
        <span class="historique__list__item__control__name">Effacer</span>
    {!! Form::close() !!}
</div>

@if(!($valide_info[$offre->id]))
    <div class="btn btn-form btn-green-valider" id="{{'valider-to-hide-'.$offre->id}}">
        {!! Form::open(['route' =>['validation_offrestage', $offre->id], 'method' => 'post','class'=>'btn_valider','id' => "{{'valider-'.$offre->id}}"]) !!}
            <button type="submit"> <span class="glyphicon glyphicon-ok"></span></button>
            <span class="historique__list__item__control__name">Valider</span>
        {!! Form::close() !!}
    </div>
@endif