@extends('admin.layout')

@section('title')
    Liste de offres de stages
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Offres Stages</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <h3 class="left">Liste des offres de stage</h3><br/>
    @include('generale.carte_offre_template', [
        'offres'  => $offres,
        'boutons' => 'admin.boutons.gestion_offres'
    ])

@endsection

@section('script_js')
    <script src="{{ url('js/valide_offre.js') }}"></script>
@endsection