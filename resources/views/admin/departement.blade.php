@extends('admin.layout')

@section('title')
    Gestion des Departements
@endsection

@section('style')
    <link rel="stylesheet" href="{{ url('css/crud_list.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Gestion des Departements</li>
@endsection

@section('panel_content')

    @include('generale.form_notification')
    @include('generale.flash_message')

    <h3>Liste des départements</h3><br/>

    @if($departements->first() != null)
        <div class="crud-list">
            @foreach($departements as $departement)
                <div class="crud-list-item">
                    <div class="crud-list-left update-form-to-add">
                        <span class="glyphicon glyphicon-tags crud-list-icon" aria-hidden="true"></span>
                        <span class="crud-list-name"> {{ $departement->nom }}</span>
                    </div>
                    <div class="crud-list-right">
                        <div class="update-form">
                            <form action="{{ route('admin_departe_update', $departement->id) }}" method="post" class="form-inline">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input class="form-control" type="text" name="nom" value="{{ $departement->nom }}" placeholder="Nouveau nom du departement" required/>
                                <button class="btn btn-primary" type="submit">Envoyer</button>
                            </form>
                        </div>
                        <a class="crud-list-update btn btn-warning add-update-form"
                           update="{{ 'admin_departe_update-'.$departement->id }}">Modifier</a>
                        <a class="crud-list-delete btn btn-danger"
                           href="{{ route('admin_departe_delete', $departement->id) }}">Supprimer</a>
                    </div>
                </div>
            @endforeach
        </div>
        <br/>
    @else
        <h3 class="text-danger">Aucun département défini sur AdopteUnStage</h3>
    @endif

    <h3>Ajouter un nouveau departement</h3><br/>

    {!! Form::open(['route' => 'admin_departe_post', 'class' => 'form', 'files' => true]) !!}
    <div class="form-group">
        <div class="input-to-add">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-tags"></span></div>
                {!! Form::text('departements[0]', null, ['class' => 'form-control', 'placeholder' => 'Departement à ajouter', 'min' => '2']) !!}
            </div>
        </div>
    </div>
    <br/>
    <a class="btn btn-link" id="add-input">Ajouter un champs</a>
    {!! Form::submit('Envoyer', ['class' => 'btn-form']) !!}
    {!! Form::close() !!}
@endsection



@section('script_js')
    <script src="{{ url('js/add_input.js') }}"></script>
    <script src="{{ url('js/add_update_form.js') }}"></script>
@endsection