@extends('admin.layout')

@section('title')
Liste de Conventions
@endsection

@section('fil-ariane')
@parent
<li class="active">Conventions</li>
@endsection

@section('content')
    @include('generale.form_notification')
    @include('generale.flash_message')

    @include('generale.liste_convention', [
        'role'  => 'admin',
        'route_update' =>'admin_upload_conv',
        'route_delete' =>'admin_delete_conv'
    ])




@endsection

@section('script_js')
    <script>
        $(document).ready(function(){
            $("#conventions_table").tablesorter();
        });
    </script>

@endsection