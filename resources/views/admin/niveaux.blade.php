@extends('admin.layout')

@section('title')
    Gestion des niveaux
@endsection

@section('style')
    <link rel="stylesheet" href="{{ url('css/crud_list.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Gestion des Niveaux</li>
@endsection

@section('panel_content')

    @include('generale.form_notification')
    @include('generale.flash_message')

    @if($niveaux->first() != null)
        <h3>Liste des niveaux des études actuelles</h3><br/>

        <div class="crud-list">
            @foreach($niveaux as $niveau)
                <div class="crud-list-item">
                    <div class="crud-list-left update-form-to-add">
                        <span class="glyphicon glyphicon-tags crud-list-icon" aria-hidden="true"></span>
                        <span class="crud-list-name"> {{ $niveau->nom }}</span>
                    </div>
                    <div class="crud-list-right">
                        <div class="update-form">
                            <form action="{{ route('admin_niveaux_update', $niveau->id) }}" method="post" class="form-inline">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input class="form-control" type="text" name="nom" value="{{$niveau->nom}} " placeholder="Nouveau nom du niveau" required/>
                                <button class="btn btn-primary" type="submit">Envoyer</button>
                            </form>
                        </div>
                        <a class="crud-list-update btn btn-warning add-update-form"
                           update="{{ 'admin_niveaux_update-'.$niveau->id }}">Modifier</a>
                        <a class="crud-list-delete btn btn-danger"
                           href="{{ route('admin_niveaux_delete', $niveau->id) }}">Supprimer</a>
                    </div>
                </div>
            @endforeach
        </div>
        <br/>
    @endif

    <h3>Ajouter un nouveau niveau</h3><br/>

    {!! Form::open(['route' => 'admin_niveaux_post', 'class' => 'form', 'files' => true]) !!}
    <div class="form-group">
        <div class="input-to-add">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-tags"></span></div>
                {!! Form::text('niveaux[0]', null, ['class' => 'form-control', 'placeholder' => 'Niveaux à ajouter', 'min' => '2']) !!}
            </div>
        </div>
    </div>
    <br/>
    <a class="btn btn-link" id="add-input">Ajouter un champs</a>
    {!! Form::submit('Envoyer', ['class' => 'btn-form']) !!}
    {!! Form::close() !!}
@endsection



@section('script_js')
    <script src="{{ url('js/add_input.js') }}"></script>
    <script src="{{ url('js/add_update_form.js') }}"></script>
@endsection