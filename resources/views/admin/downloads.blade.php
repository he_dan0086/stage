@extends('admin.layout')

@section('title')
    Templates
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Gesion des  templates </li>
@endsection

@section('content')


    <h3>Gesion des  templates</h3><br/>


    @include('generale.form_notification')
    @include('generale.flash_message')

    @if($templates->first()!=null)
        @foreach($templates as $template)
            <br/>
            <br/>
            <a class="left btn btn-warning" href="{{ URL('administrateur/template/download/'.$template->langage) }}"><span class="glyphicon glyphicon-save"></span> Template {{$template->langage}}</a>
            <br/>
            <br/>
            {!! Form::open(['route' => ['admin_upload_template_post',$template->langage], 'method'=>'post','class' => 'form-inline', 'files' => true, 'enctype'  => 'multipart/form-data']) !!}
            <div class="input-group form-group left-bg">
                <div class="input-group-addon"><span class="glyphicon glyphicon-cloud-upload"></span></div>
                <input type="file" name="{{'conv_'.$template->langage}}" class="form-control" required />
                <div class="input-group-addon"><span >Version {{$template->langage}}</span></div>
            </div>

            <div class="form-group right-bg">
                {!! Form::submit('Envoyer', ['class' => 'btn-form right']) !!}
            </div>
            {!! Form::close() !!}
            <hr/>
        @endforeach
    @endif

    <div class="required-info">
        <br/><i>* Les champs en rouge sont obligatoires</i><br/>
    </div>



@endsection

@section('script')

@endsection