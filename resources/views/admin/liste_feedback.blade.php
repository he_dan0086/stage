@extends('admin.layout')

@section('title')
    Liste de feedbacks
@endsection

@section('fil-ariane')
    @parent
    <li class="active">feedbacks</li>
@endsection

@section('content')
    @include('generale.form_notification')
    @include('generale.flash_message')


    <h3>Liste de feedbacks</h3>

    @if($feedbacks->first() != null)
        <div class="container">
            <div class="row">
                <div class="col-md-10 ">
                    <div class="table-responsive table black-font">
                        <table id="feedback_table" class="table table-bordred table-striped tablesorter">
                            <thead >
                            <th>Creer par<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Entreprise<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Description de l'entreprise<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Feedback<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>Recrutement<span class=" blue-font glyphicon glyphicon-chevron-down"></span></th>
                            <th>actions</th>
                            </thead>

                            <tbody>
                            @foreach($feedbacks as $feedback)
                                <tr>
                                    {{--<th><input type="checkbox" id="checkthis" /></th>--}}
                                    <td data-sortable="true">
                                        @if($feedback->isOuvert)
                                            {{$feedback->etudiant->nom}} {{$feedback->etudiant->prenom}}
                                        @else
                                            Anonyme
                                        @endif
                                    </td>
                                    <td data-sortable="true">{{$feedback->entreprise}}</td>
                                    <td data-sortable="true">{{$feedback->descrption_entreprise}}</td>
                                    <td data-sortable="true">{{$feedback->contenu}}</td>
                                    <td data-sortable="true">{{$feedback->recrutement_feedback}}</td>

                                    <td>
                                        <p data-placement="top" data-toggle="tooltip">
                                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_{{$feedback->id}}" >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                            <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit_{{$feedback->id}}" >
                                                <span class="glyphicon glyphicon-fullscreen"></span>
                                            </button>
                                        </p>
                                    </td>
                                </tr>




                                {{--========================= edit modal fade ================================--}}
                                <div class="modal fade" id="edit_{{ $feedback->id }}" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        @include('generale.edit_feedback',
                                             [
                                                'feedback' => $feedback
                                             ])
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                {{--!!!========================= edit modal fade ================================--}}



                                {{--========================= delete modal fade ================================--}}
                                <div class="modal fade" id="delete_{{$feedback->id}}" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <!-- /.modal-content -->
                                        {!! Form::open(['route' =>['admin_delete_feedback', $feedback->id], 'method' => 'delete']) !!}
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                                <h4 class="modal-title custom_align black-font" id="Heading">Supprimer ce record dans la base de donnée</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                                            </div>
                                            <div class="modal-footer ">
                                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="clearfix"></div>
                        <ul class="pagination pull-right">
                            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    @else
        <h3 class="text-danger">Oups! Il n'y aucun record de feedback pour l'instant!</h3>
    @endif
@endsection

@section('script_js')
    <script>
        $(document).ready(function(){
            $("#feedback_table").tablesorter();
        });
    </script>

@endsection