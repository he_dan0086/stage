@extends('template')

@section('title')
    AdopteUnStage @if(isset($schoolName)){{($schoolName->value)?' - '.$schoolName->value :''}}@endif
@endsection

@section('menu_li')
    <li><a href="{{ url(route('inscription_etudiant')) }}">Inscription Etudiant</a></li>
    <li><a href="{{ url(route('inscription_entreprise')) }}">Inscription Entreprise</a></li>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/Auth/login.css') }}">
@endsection



@section('content')

    @include('generale.form_notification')
    @include('generale.flash_message')
    <br/>

    <form role="form" action="{{ url(route('login_get')) }}">
        <p class=" login button">
            <input type="submit" class=" button btn btn-success btn-lg btn-block"  value="Etudiant avec identifiant unice">
            </input>
        </p>
        </br>
    </form>

    <form role="form"  action="{{ url('/entreprise/offre-stage/create') }}">
        <p class=" button">
            <input type="submit" class=" button btn btn-info  btn-lg btn-block" value="Entreprise (SANS COMPTE)"/>
        </p>
        <br/>
    </form>

    <form role="form"  action="{{ url(route('login_get')) }}">
        <p class=" button">
            <input type="submit" class=" button btn btn-danger  btn-lg btn-block" value="Authentification"/>
        </p>
        <br/>
    </form>




@endsection