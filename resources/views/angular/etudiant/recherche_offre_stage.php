<h2 xmlns="http://www.w3.org/1999/html">Recherche des offres de stage</h2><br/><br/>

<div class="input-group left">
    <div class="input-group-addon"><span class="glyphicon glyphicon-education"></span></div>

    <select ng-init="fields_niveau = niveaux[2]" class="form-control" ng-model="fields_niveau" ng-options="niveau.nom for niveau in niveaux">
        <option value="">Aucun filtre</option>
    </select>
    <div class="input-group-addon"><span>Niveaux du stage</span></div>

</div>
<div class="input-group right">
    <div class="input-group-addon"><span class="glyphicon glyphicon-education"></span></div>
    <select ng-init="fields_departement = departements[2]"  class="form-control" ng-model="fields_departement" ng-options="departement.nom for departement in departements">
        <option value="">Aucun filtre</option>
    </select>
    <div class="input-group-addon"><span>Departement corespondant</span></div>
</div>
<br>

<div
    isteven-multi-select
    input-model="competences"
    output-model="competenceSelection"
    button-label="nom"
    item-label="nom"
    tick-property="ticked">
</div>
<br>

<div class="historique__list">
    <div class="historique__list__item"
         dir-paginate="stage in stages
            | filter: fields_niveau.nom
            | filter: fields_departement.nom
            | customCompetenceFilter   : competenceSelection
            | itemsPerPage:10 ">

        <div class="historique__list__item__row">
            <div class="historique__list__item__content">
                <span class="historique__list__item__content__title">
                    {{ stage.nom_entreprise }} - {{stage.titre}}
                </span>
                <span class="historique__list__item__content__contact">
                    <div class="col-lg-6">
                        <span ng-if="stage.ville">
                            {{ stage.ville }} ({{stage.code_postal}} )
                        </span>
                         <span ng-if="stage.gratification!=0">
                             -- Gratification : {{ stage.gratification }} {{stage.devise}}
                        </span>
                        <br/>
                        <span ng-if="stage.email">
                            <strong>Email</strong> : {{ stage.email }}
                        </span>
                        </br>
                        <span><strong>Niveaux</strong>:</br>
                            <span ng-repeat="niveau in stage.niveaux">
                              <i>* {{ niveau.nom }}</i></br>
                            </span>
                        </span>
                        </br>
                    </div>

                    <div class="col-lg-6">
                        <span><strong>Competences</strong> :
                            <span ng-repeat="comp in stage.competences">
                                {{ comp.nom }}.
                            </span>
                        </span>
                        </br>

                        <span><strong>Departements</strong>:</br>
                            <span ng-repeat="dep in stage.departements">
                               <i> * {{ dep.nom }}</i></br>
                            </span>
                        </span>
                    </div>
                </span>
                </br>
            </div>

            <div class="historique__list__state">
                <div class="historique__list__state__section">
                    <span ng-if="stage.valide==1">
                        <span class="colored__bullet__green"></span>
                        <span>Offre validée</span>
                    </span>
                    <span ng-if="stage.valide==0">
                        <span class="colored__bullet__red"></span>
                        <span>Validation en cours</span>
                    </span>
                     <span ng-if="stage.nombre_vu==0">
                        <span class="colored__bullet__red"></span>
                        <span>Aucun visiteur</span>
                    </span>
                    <span ng-if="stage.nombre_vu!=0">
                        <span class="colored__bullet__green"></span>Visiteurs
                        <span class="badge">{{stage.nombre_vu}}</span>
                    </span>
                    <span class="colored__bullet__yellow"></span>
                    <span>{{ stage.created_at }}</span>

                </div>
            </div>
            <div class="historique__list__item__etudiant__control">
                <div class="btn btn-form btn-blue-view">
                    <a href="etudiant/offre-stage/{{ stage.id}}">
                        <span class="glyphicon glyphicon-fullscreen"></span>
                        <span class="historique__list__item__control__name">Afficher</span>
                    </a>
                </div>
                <div class="btn btn-form btn-red-trash">
                    <a ng-href="mailto:{{stage.email}}?Subject=[Candidature%20des%20offres-stages]AdopteUnStage-Polytech">
                        <span class="fa fa-rocket"></span>
                        <span class="historique__list__item__control__name">Postuler</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<dir-pagination-controls
    max-size="20"
    direction-links="true"
    boundary-links="true" >
</dir-pagination-controls>
