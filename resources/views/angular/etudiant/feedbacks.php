<div class="historique__header" xmlns="http://www.w3.org/1999/html">
    <div class="historique__header__title">
            <span class="historique__header__title__span">
                Recherche des feedbacks
            </span>
    </div>
</div>


<div class="input-group" >
    <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
    <input ng-model="entrep" id="search" class="form-control" placeholder="Entrez des mots clés pour la recherche">
    <div class="input-group-addon"><span>Recherche</span></div>
</div>
<br/>
<br/>
<br/>

<div class="historique__list">
    <div class="historique__list__item"
         dir-paginate="feedback in feedbacks
            | filter: entrep
            | itemsPerPage:10 ">

        <div class="historique__list__item__row">
            <div class="historique__list__item__content">
                <span class="historique__list__item__content__title">
                    <span ng-if="feedback.entreprise">
                             Entreprise: {{ feedback.entreprise }}
                    </span>
                </span>
                <div class="historique__list__item__content__contact">
                    <p ng-if="feedback.descrption_entreprise">
                        <span class="colored__bullet__green"></span>
                            <strong>Description de l'entreprise </strong>
                            {{ feedback.descrption_entreprise }}
                    </p>
                    <p ng-if="feedback.contenu">
                        <span class="colored__bullet__red"></span>
                        <strong>Feedbacks</strong>
                        {{ feedback.contenu }}
                    </p>
                    <p ng-if="feedback.recrutement_feedback">
                        <span class="colored__bullet__yellow"></span>
                        <strong>Feedback du recrutment</strong>
                        {{ feedback.recrutement_feedback }}
                    </p>
                    <p ng-if="feedback.isOuvert==true">
                        <span class="historique__list__state__section">
                            <span ng-if="feedback.created_by">
                                    created_by: {{ feedback.etudiant.nom }} {{ feedback.etudiant.prenom }}(infos ouvertes)
                            </span>
                        </span>
                    </p>
                    <p ng-if="feedback.isOuvert == false">
                        <span class="historique__list__state__section">
                            <span>
                                   Par Anonyme
                            </span>
                        </span>
                    </p>
                </div>
            </div>

            <div class="historique__list__item__etudiant__control">
                <div class="btn btn-form btn-blue-view">
                    <a href="etudiant/feedback/{{ feedback.id}}">
                        <span class="glyphicon glyphicon-fullscreen"></span>
                        <span class="historique__list__item__control__name">Afficher</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<dir-pagination-controls
        max-size="20"
        direction-links="true"
        boundary-links="true" >
</dir-pagination-controls>