@extends('template')

@section('title')
    AdopteUnStage @if(isset($schoolName)){{($schoolName->value)?' - '.$schoolName->value :''}}@endif
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/Auth/login.css') }}">
@endsection

@section('menu_li')
    <li><a href="{{ url(route('index')) }}">Home</a></li>
@endsection

@section('content')

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="alert alert-danger">
        <h3><span class="glyphicon glyphicon-remove-circle"></span>Expiration du lien</h3>
        <br/>Le lien d'activer le compte est expiré, vous devez vous réinscrire sur AdopteUnStage
    </div>

    <a class="btn btn-primary" type="button" href="{{url(route('inscription_etudiant'))}}">Inscription</a>




@endsection
