@extends('template')

@section('title')
    AdopteUnStage @if(isset($schoolName)){{($schoolName->value)?' - '.$schoolName->value :''}}@endif
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/Auth/login.css') }}">
@endsection

@section('menu_li')
    <li><a href="{{ url(route('index')) }}">Home</a></li>
@endsection

@section('content')

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="alert alert-success">
        <h3><span class="glyphicon glyphicon-ok-circle"></span>Dernière étape de l'inscription</h3>
        <br/>Nous avez envoyé un mail pour activer votre compte à l'adresse <strong> {{$email}}</strong>
        <br/>Pour compléter votre inscription, Cliquez le lien dans le mail!

        <br/><br/><strong>Après l'inscription, vour pouez prifiter des fonctionalité:</strong>
        <br/>voir la liste des offres de stage publiées..........
    </div>

    <a class="btn btn-primary" type="button" href="{{url(route('login_get'))}}">Entrer dans la boîte de mail</a>




@endsection
