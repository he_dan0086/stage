@extends('template')

@section('title')
    AdopteUnStage @if(isset($schoolName)){{($schoolName->value)?' - '.$schoolName->value :''}}@endif
@endsection


@section('menu_li')
    <li><a href="{{ url(route('index')) }}">Home</a></li>
@endsection


@section('content')
        <div class="alert alert-success" >
            <strong><span class="glyphicon glyphicon-home"></span> Tous les champs sont obligatoires.</strong>
        </div>

    	<div id="wrapper">
	    	<div id="login" class="animate form">
	    		@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

                    <h3> Inscription Etudiant</h3><br/>


                    <form  role="form" method="POST" action="{{ Request::url() }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input class="form-control" type="text" id="usernom" name="nom" placeholder="Votre nom" value="{{ old('nom') }}" required/>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input class="form-control" type="text" id="userprenom" name="prenom" placeholder="Votre prenom" value="{{ old('prenom') }}" required/>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                            <input class="form-control" type="text" name="telephone" placeholder="Votre Telephone" value="{{ old('telephone') }}" required/>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-globe"></span></div>
                            <textarea class="form-control" type="text" name="adresse" placeholder="Votre Adresse complét"  required>{{ old('adresse') }}</textarea>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-globe"></span></div>
                            <select class="form-control" name="Filiere" id="Filiere" required="required" >
                                <option selected disabled> Sélectionner une filiere</option>
                                @foreach($filieres as $filiere)
                                    <option value="{{ $filiere->id }}"> {{ $filiere->filiere_nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                            <input class="form-control" type="email" id="emailsignup" name="email" placeholder="Adresse email" value="{{ old('email') }}" required/>
                            <div class="input-group-addon">Celui-ci va être votre login nom</div>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                            <input class="form-control" id="passwordsignup" name="password" required="required" type="password" placeholder="Mot de passe" required/>
                        </div>
                        <br/>

                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                            <input class="form-control" id="passwordsignup_confirm" name="password_confirmation" required="required" type="password" placeholder="Confirmation du mot de passe" required/>
                        </div>
                        <br/>


	                <p class="signin button"> 
						<input class="btn-form" type="submit" value="S'inscrire"/>
					</p>
				</form>
			</div>
		</div>

@endsection
