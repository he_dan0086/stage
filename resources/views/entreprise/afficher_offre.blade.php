@extends('entreprise.layout')

@section('title')
    {{ $offre->title }}
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/offre_stage.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">{{ $offre->title }}</li>
@endsection

@section('content')


    @include('generale.flash_message')

    @include('generale.vue_offre')

@endsection
