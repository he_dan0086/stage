@extends('template_panel')

@section('title')
    Panel Entreprise
@endsection

@section('style_panel')
    <link rel="stylesheet" href="{{ asset('css/panel-entreprise.css') }}">
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/libraries/isteven-multi-select.css') }}">
@endsection

@section('fil-ariane')
    <li><a href="{{ url(route('offre-stage-entreprise')) }}"> Panel entreprise </a></li>
@endsection

@section('menu')
    <li class="{{ Request::is('entreprise/offre-stage/create') ? 'active':'' }}">
        <a href="{{ url('entreprise/offre-stage/create') }}">AJOUTER UNE OFFRE DE STAGE</a>
    </li>

    <li class="{{ Request::is('entreprise/profil') ? 'active':'' }}">
        <a href="{{ url(route('entreprise_profil')) }}">MON PROFIL</a>
    </li>


    <li class="{{ Request::is('entreprise/offre-stage/list') ? 'active':'' }}">
        <a href="{{ url(route('offres_list_login')) }}">MES LISTES DES OFFRES</a>
    </li>

    <li class="{{ Request::is('entreprise/liste-etudiants*') ? 'active':'' }}">
        <a href="{{ url(route('etudiant_liste')) }}">LISTE DES ETUDIANTS</a>
    </li>

@endsection


