@extends('entreprise.layout')

@section('title')
    Ajouter nouvelle offre de stage
@endsection

@section('style')
    <link rel="stylesheet" href="{{asset('css/libraries/select2.css')}}"/>
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Ajouter une offre de stage</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Ajouter nouvelle Offre Stage</span>
        </div>
        <div class="historique__header right">
            {{--signer une probleme au responsable Helen: helen@polytech.unice.fr--}}
            <a href="mailto:helen@polytech.unice.fr?Subject=Problème%20offres-stages.polytech">Signer problème</a>
            <a href="{{url('entreprise/download')}}">Télécharger un modèle d'offres de stage</a>
        </div>
    </div>
    <br/>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    {{--==========================template pour creer une offre de satge=====================--}}
    @include('generale.create_offre',
        [
            'route' => 'offre-create',
            'competences' => $competences
        ]
    )

@endsection


@section('script_js')
    <script src="{{asset('js/libraries/select2.js')}}"></script>
    <script src="{{asset('js/competences_offre.js')}}"></script>
    <script src="{{ url('js/add_input.js') }}"></script>
@endsection