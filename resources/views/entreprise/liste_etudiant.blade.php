@extends('entreprise.layout')

@section('title')
    Liste des étudiants
@endsection

@section('fil-ariane')
    @parent
    <li class="active">Etudiants</li>
@endsection

@section('content')
    <br/>

    @include('generale.form_notification')
    @include('generale.flash_message')

    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Liste des étudiants</span>
        </div>
    </div>
    <br/>

    @include('generale.carte_user_template', [
        'etudiants'  => $etudiants,
        'boutons' => 'entreprise.boutons.gestion_etudiant'
    ])

@endsection