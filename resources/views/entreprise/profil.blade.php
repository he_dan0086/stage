@extends('entreprise.layout')

@section('title')
    Profil
@endsection

@section('fil-ariane')
    @parent
    <li class="active">MON PROFIL</li>
@endsection

@section('content')

    {{--==============================Titre ================================--}}
    <div class="historique__header">
        <div class="historique__header__title">
            <span class="historique__header__title__span">Profil</span>
        </div>
    </div>

    {{--=========================messages error ou notification==================--}}
    @include('generale.form_notification')
    @include('generale.flash_message')

    <form role="form" method="POST" action="{{url(route('entreprise_profil_post'))}}" xmlns="http://www.w3.org/1999/html" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="panel panel-info">
            <div class="panel-heading left">Entreprise</div>

            <div class="text-info" >
                <div class="form-group sans-margin-bottom" >
                    <label class="col-sm-4 control-label">NOM DE L'ENTREPRISE</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="nom" value="{{$entreprise->nom}}" >
                    </div>
                </div>
                <div class="form-group sans-margin-bottom"  >
                    <label class="col-sm-4 control-label">Personne de contact</label>
                    <div class="col-sm-8 left">
                        <input class=" col-lg-10 form-control-static" name="contact" value="{{$entreprise->contact_person}}" >
                    </div>
                </div>
                <div class="form-group sans-margin-bottom">
                    <label class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-8 left">
                        <input type="email" class=" col-lg-10 form-control-static " name="email" value="{{$user->email}}" readonly>
                    </div>
                </div>
            </div>
            <br/>
            <input type="submit" class="btn btn-success" value="ENREGISTRER">
        </div>
        <br/>
    </form>
@endsection


