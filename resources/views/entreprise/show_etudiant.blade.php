@extends('entreprise.layout')

@section('title')
    {{ $user->user->nom }} {{ $user->user->prenom }}
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/offre_stage.css') }}"/>
@endsection

@section('fil-ariane')
    @parent
    <li><a href="{{ url('/entreprise/liste-etudiants') }}">Liste des étudiants</a></li>
    <li class="active">{{$user->user->nom }}</li>
@endsection

@section('content')

    @include('generale.flash_message')
    @include('generale.vue_etudiant')


@endsection