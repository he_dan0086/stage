<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('departements', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nom');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

//        Schema::drop('moderateur_promotion');
        Schema::drop('departements');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
