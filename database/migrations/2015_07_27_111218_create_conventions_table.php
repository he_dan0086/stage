<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConventionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('conventions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('prenom');
            $table->string('nom');
            $table->string('filiere');
            $table->boolean('estEtranger');
            $table->string('convention_fr')->nullable();
            $table->string('convention_en')->nullable();
            $table->string('convention_biling')->nullable();
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('conventions');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
