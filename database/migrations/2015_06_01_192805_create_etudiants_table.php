<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtudiantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('etudiants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nom');
			$table->string('prenom');
			$table->string('addresse');
			$table->integer('filiere_id')->unsigned();
			$table->string('telephone');
			$table->timestamps();

			$table->string('cv')->nullable();

			$table->foreign('filiere_id')->references('id')->on('filieres')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('etudiants');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
