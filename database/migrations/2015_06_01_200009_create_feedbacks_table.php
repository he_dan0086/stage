<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feedbacks', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('entreprise');
			$table->text('descrption_entreprise')->nullable();
            $table->text('contenu');
            $table->text('recrutement_feedback');
			$table->integer('created_by')->unsigned();
            $table->boolean('isOuvert');
			$table->boolean('valide');

			$table->timestamps();

			$table->foreign('created_by')->references('id')->on('etudiants')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('feedbacks');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }

}
