<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsableTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('responsables', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nom');
            $table->string('prenom');
            $table->integer('filiere_id')->unsigned();

            $table->foreign('filiere_id')->references('id')->on('filieres')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('responsables');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
