<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffreStagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offre_stages', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('nom_entreprise');
            $table->string('code_postal');
            $table->string('ville');
            $table->string('pays')->nullable();
            $table->string('email');

            $table->string('titre');
            $table->text('description', 10000);
            $table->integer('gratification')->nullable();
            $table->string('devise')->nullable();
            $table->string('ficher_detail');
            $table->boolean('valide');
            $table->integer('nombre_vu')->unsigned();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('stagiaire_id')->unsigned()->nullable();
			$table->timestamps();

            $table->foreign('created_by')->references('id')->on('entreprises')->onDelete('cascade');
            $table->foreign('stagiaire_id')->references('id')->on('etudiants')->onDelete('set null');
		});

        Schema::create('competence_offre_stage', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('competence_id')->unsigned();
            $table->foreign('competence_id')->references('id')->on('competences')->onDelete('cascade');

            $table->integer('offre_stage_id')->unsigned();
            $table->foreign('offre_stage_id')->references('id')->on('offre_stages')->onDelete('cascade');
        });

        Schema::create('departement_offre_stage', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('departement_id')->unsigned();
            $table->foreign('departement_id')->references('id')->on('departements')->onDelete('cascade');

            $table->integer('offre_stage_id')->unsigned();
            $table->foreign('offre_stage_id')->references('id')->on('offre_stages')->onDelete('cascade');
        });

        Schema::create('niveau_offre_stage', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('niveau_id')->unsigned();
            $table->foreign('niveau_id')->references('id')->on('niveaux')->onDelete('cascade');

            $table->integer('offre_stage_id')->unsigned();
            $table->foreign('offre_stage_id')->references('id')->on('offre_stages')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('niveau_offre_stage');
        Schema::drop('departement_offre_stage');
        Schema::drop('competence_offre_stage');
        Schema::drop('offre_stages');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
