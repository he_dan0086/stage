<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesOuvriersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('stages_ouvriers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('prenom');
            $table->string('nom');
            $table->string('filiere');
            $table->boolean('estEtranger');
            $table->string('infos_stage_ouvrier')->nullable();
            $table->string('doc_justicatif')->nullable();
            $table->timestamps();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::drop('stages_ouvriers');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
