<?php


use App\Template;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('templates')->delete();
        Template::create(array(
            'filiere' => 'SI4',
            'template_fr' => 'templates/template_convention_fr.docx',
            'template_en' => 'templates/template_convention_en.docx',
            'template_biling' =>'templates/template_convention_biling.docx' ));
    }

}