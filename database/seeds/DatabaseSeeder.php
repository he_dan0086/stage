<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Model::unguard();

        //Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('LangueTableSeeder');
        $this->command->info('Langue table seeded!');

        $this->call('RoleTableSeeder');
        $this->command->info('Role table seeded!');

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        $this->call('FiliereTableSeeder');
        $this->command->info('filiere table seeded!');

        $this->call('EntrepriseTableSeeder');
        $this->command->info('Entreprise table seeded!');

        $this->call('EtudiantTableSeeder');
        $this->command->info('Etudiant table seeded!');

        $this->call('AdministrateurTableSeeder');
        $this->command->info('Administrateur table seeded!');

        $this->call('ResponsableTableSeeder');
        $this->command->info('Responsable table seeded!');

        $this->call('CompetenceTableSeeder');
        $this->command->info('Competence table seeded!');

        $this->call('DepartementTableSeeder');
        $this->command->info('Departement table seeded!');

        $this->call('NiveauxTableSeeder');
        $this->command->info('niveaux table seeded!');

        $this->call('OffreStageTableSeeder');
        $this->command->info('OffreStage table seeded!');

        $this->call('DownloadTableSeeder');
        $this->command->info('Download table seeded!');

        $this->call('TemplateTableSeeder');
        $this->command->info('Template table seeded!');

        $this->call('FeedbackTableSeeder');
        $this->command->info('Feedback table seeded!');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}