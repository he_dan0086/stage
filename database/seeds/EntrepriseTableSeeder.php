<?php


use App\Entreprise;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrepriseTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('entreprises')->delete();

        Entreprise::create([
            'id'=>'1',
            'nom'=>'air france',
            'contact_person'=>'HE Dan'
        ]);

    }

}