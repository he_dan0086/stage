<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Administrateur;

class AdministrateurTableSeeder extends Seeder {

    public function run()
    {
        DB::table('administrateurs')->delete();
        Administrateur::create([
            'id'=>'1',
            'nom'=>'HE',
            'prenom'=>'Dan'
        ]);

        Administrateur::create([
            'id'=>'2',
            'nom'=>'Gallesio',
            'prenom'=>'Erick'
        ]);

        Administrateur::create([
            'id'=>'3',
            'nom'=>'Tonin',
            'prenom'=>'Cyril'
        ]);

        Administrateur::create([
            'id'=>'4',
            'nom'=>'Litovsky',
            'prenom'=>'Igor'
        ]);
    }
}
