<?php


use App\Download;
use App\Template;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DownloadTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('downloads')->delete();
        Download::create(array('langage' => 'offre', 'position' => 'templates/template_offre_stage.doc'));
        Download::create(array('langage' => 'ouvrier', 'position' => 'templates/template_stage_ouvrier.docx'));

    }

}