<?php


use App\Niveau;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NiveauxTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('niveaux')->delete();
        Niveau::create(['nom' => 'ingénieur (5ème année) ou Master 2']);
        Niveau::create(['nom' => 'technicien (4ème année)']);
        Niveau::create(['nom' => 'ouvrier (3ème année et Peip)']);
    }
}