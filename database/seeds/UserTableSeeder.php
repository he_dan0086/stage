<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
class UserTableSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();

        $roles = Role::orderBy('id')->get(array('id'));

        User::create(array( 'email' => 'he.dan0086@gmail.com', 'password' => Hash::make('azerty'),
            'role_id' => $roles[0]->id, 'valide' => 1, 'user_type' => 'App\Administrateur', 'user_id' => '1'
        ));
        User::create(array( 'email' => 'erick.gallesio@unice.fr', 'password' => Hash::make('azerty'),
            'role_id' => $roles[0]->id, 'valide' => 1, 'user_type' => 'App\Administrateur', 'user_id' => '2'
        ));
        User::create(array( 'email' => 'cyril.tonin@polytech.unice.fr', 'password' => Hash::make('azerty'),
            'role_id' => $roles[0]->id, 'valide' => 1, 'user_type' => 'App\Administrateur', 'user_id' => '3'
        ));
        User::create(array( 'email' => 'igor.litovsky@polytech.unice.fr', 'password' => Hash::make('azerty'),
            'role_id' => $roles[0]->id, 'valide' => 1, 'user_type' => 'App\Administrateur', 'user_id' => '4'
        ));



        User::create(array( 'email' => 'he.dan0086@yahoo.com', 'password' => Hash::make('azerty'),
            'role_id' => $roles[1]->id, 'valide' => 1, 'user_type' => 'App\Responsable', 'user_id' => '1'
        ));
        User::create(array( 'email' => 'he.dan@etu.unice.fr', 'password' => Hash::make('azerty'),
            'role_id' => $roles[2]->id, 'valide' => 1, 'user_type' => 'App\Etudiant', 'user_id' => '1'
        ));
        User::create(array( 'email' => 'dan_emilie@yahoo.com', 'password' => Hash::make('azerty'),
            'role_id' => $roles[3]->id, 'valide' => 1, 'user_type' => 'App\Entreprise', 'user_id' => '1'
        ));

    }
}
