<?php

use App\Filiere;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Responsable;

class ResponsableTableSeeder extends Seeder {

    public function run(){
        DB::table('responsables')->delete();

        $filieres = Filiere::orderBy('id')->get(array('id'));
        Responsable::create([
            'id' => 1,
            'nom' =>'Eric',
            'prenom'=> 'Galisio',
            'filiere_id'=>$filieres[2]->id
        ]);
    }
}
