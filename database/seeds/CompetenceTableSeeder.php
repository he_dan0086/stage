<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Competence;

class CompetenceTableSeeder extends Seeder {
    public function run()
    {
        DB::table('competences')->delete();

        Competence::create(array('nom' => 'PHP'));
        Competence::create(array('nom' => 'LARAVEL'));
        Competence::create(array('nom' => 'ANGULARJS'));
        Competence::create(array('nom' => 'HTML/XML'));
        Competence::create(array('nom' => 'ECLIPSE'));
        Competence::create(array('nom' => 'VISUAL STUDIO'));
        Competence::create(array('nom' => 'SYMFONY'));
        Competence::create(array('nom' => 'JAVASCRIPT'));
        Competence::create(array('nom' => 'JAVA'));
        Competence::create(array('nom' => 'C/C++'));
    }
}
