<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\OffreStage;

class OffreStageTableSeeder extends Seeder {

    public function run()
    {
        DB::table('offre_stages')->delete();

        OffreStage::create([

                'nom_entreprise' => 'Bleep Communication',
                'code_postal' => '07787',
                'ville' => 'Nice',
                'pays' => 'France',
                'email' => 'francoi@exmple.com',
                'valide' => '0',

                'titre' =>'Développé en WordPress et Php chez Bleep communication',
                'description' => 'NauticNews.com est le premier magazine dinformation nautique bilingue (FR, GB),
                en ligne depuis 2004. Développé en WordPress et Php, il intègre plusieurs bases de données.
                Le but du stage est de préparer/réaliser la migration de son contenu vers la dernière version
                de WordPress, en intégrant de nouvelles offres (nouveau design, nouveau système de publicités,
                 boutique en ligne, version mobile, etc.).',
                'gratification' => '500',
                'devise' => 'euros',
        ]);
        OffreStage::create([

            'nom_entreprise' => 'Siemens Regional Companies',
            'code_postal' => '07787',
            'ville' => 'Nice',
            'pays' => 'France',
            'email' => 'francoi@exmple.com',
            'valide' => '1',

            'titre' =>'testez les logiciels développement chez Siemens Regional Companies',
            'description' => 'Au sein de SIEMENS Postal Parcel Airport Logistics, vous êtes intégré à l
            équipe projet en charge de la maintenance des logiciels applicatifs automatisant le
             système de tri-bagages (TBE) basé aux Terminaux 2E/2F à laéroport de Roissy,
             ainsi que de la réalisation des évolutions (tests et développement) de ce même système.
             Dans ce cadre, vous participez aux projets de déploiement logiciels : * Vous testez les logiciels dévelo...',
            'gratification' => '500',
            'devise' => 'euros',
        ]);

    }
}