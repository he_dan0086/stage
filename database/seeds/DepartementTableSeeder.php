<?php

use App\Departement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartementTableSeeder extends Seeder{

    public function run(){
        DB::table('departements')->delete();
        Departement::create(['nom' => 'Sciences Informatiques (SI)']);
        Departement::create(['nom' => 'Electronique (ELEC)']);
        Departement::create(['nom' => 'Math. Appliquées et Modélisation (MAM) ']);
        Departement::create(['nom' => 'IMAFA']);
        Departement::create(['nom' => 'Génie Biologique (BIO)']);
        Departement::create(['nom' => 'Génie de l‘eau (EAU)']);
        Departement::create(['nom' => 'Bâtiment (BAT)']);
    }
}