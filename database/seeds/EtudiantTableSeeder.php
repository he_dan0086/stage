<?php

use App\Filiere;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Etudiant;

class EtudiantTableSeeder extends Seeder {

    public function run()
    {

        DB::table('etudiants')->delete();
        $filieres = Filiere::orderBy('id')->get(array('id'));

        Etudiant::create([
            'id'=>'1',
            'nom'=>'HE',
            'prenom'=>'Dan',
            'addresse'=>'2255 Route des dolines',
            'filiere_id'=> $filieres[2]->id,
            'telephone'=>'0668692755'
        ]);

    }
}
