<?php

use App\Filiere;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FiliereTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('filieres')->delete();
        Filiere::create(['filiere_nom' => 'SI3']);
        Filiere::create(['filiere_nom' => 'SI4']);
        Filiere::create(['filiere_nom' => 'SI5']);
        Filiere::create(['filiere_nom' => 'MAM3']);
        Filiere::create(['filiere_nom' => 'MAM4']);
        Filiere::create(['filiere_nom' => 'MAM5']);
        Filiere::create(['filiere_nom' => 'BAT3']);
        Filiere::create(['filiere_nom' => 'BAT4']);
        Filiere::create(['filiere_nom' => 'BAT5']);
        Filiere::create(['filiere_nom' => 'ELEC3']);
        Filiere::create(['filiere_nom' => 'ELEC4']);
        Filiere::create(['filiere_nom' => 'ELEC5']);
        Filiere::create(['filiere_nom' => 'GB3']);
        Filiere::create(['filiere_nom' => 'GB4']);
        Filiere::create(['filiere_nom' => 'GB5']);
        Filiere::create(['filiere_nom' => 'BIO3']);
        Filiere::create(['filiere_nom' => 'BIO4']);
        Filiere::create(['filiere_nom' => 'BIO5']);
        Filiere::create(['filiere_nom' => 'PEIP 1']);
        Filiere::create(['filiere_nom' => 'PEIP 2']);
        Filiere::create(['filiere_nom' => 'EUROAQUAE M1']);
        Filiere::create(['filiere_nom' => 'EUROAQUAE M2']);
        Filiere::create(['filiere_nom' => 'HYDRO M1']);
        Filiere::create(['filiere_nom' => 'HYDRO M2']);
        Filiere::create(['filiere_nom' => 'MASTER2 SSTIM-IMAGES']);
        Filiere::create(['filiere_nom' => 'MASTER2 SSTIM-SIGNAL']);
        Filiere::create(['filiere_nom' => 'MASTER IFI']);
        Filiere::create(['filiere_nom' => 'MASTER IMAFA']);
        Filiere::create(['filiere_nom' => 'MASTER MAPI MAJE1']);
        Filiere::create(['filiere_nom' => 'MASTER MAPI MAJE2']);
        Filiere::create(['filiere_nom' => 'MASTER MAPI MAT2']);

    }

}