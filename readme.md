## Installation du projet :

- Prérequis disposer PHP en console avec une version supérieur ou égale à 5.4 V.

- Enlevez le .example au fichier .env.example et le completer (choix Base de Donnees, email de l'application et mode Debug a false)
Ensuite il suffit d'executer la commande :  php artisan migrate:refresh
pour installer la Base de Donnees

## [Optionnel]
Et si vous souhaitez peupler la Base de Données vous pouvez executer la commande : php artisan db:seed
Ainsi, tous les compte creee auront pour mot de passe "azerty".

## l'administrateur  par défault :
- "he.dan0086@gmail.com" en email & "azerty" en mot de passe.
- "igor.litovsky@polytech.unice.fr" en email & "azerty" en mot de passe.
- "erick.gallesio@unice.fr" en email & "azerty" en mot de passe.
- "cyril.tonin@polytech.unice.fr" en email & "azerty" en mot de passe.

## Constructure du projet(MVC)
## Model:
- tous les .php dans le repertoire app/, dedans les relations entre les models sont définies.


## View:
- resources/views/, il gère toutes les vues;


## Controller:
- Respertoir app/HTTP/Controllers;
            app/HTTP/Middleware, il gère l'authentification de l'applcaition
            app/HTTP/Requests, il gère les requests des vues(eg: champs obligatoire, numeric, etc.)


- app/routes.php: il définit toute adresse url.
- app/Respositories: il permet d'accéder à la base de donnée.



## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
