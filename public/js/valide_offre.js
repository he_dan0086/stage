$(document).ready(function(event){

    $id=$(event.target).attr('id');

    $('#valider-'+$id).click(function(){
        $('.valider-to-hide-'+$id).hide();
        $('#valider-info-'+$id).replaceWith(
            "<span class=\"colored__bullet__green\"></span>"+
            "<span>Offre validée</span>");
    });
});