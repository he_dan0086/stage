$(document).ready(function(){
    $(".select-multiple").select2({
        tags: true
    })

    $(".select-multiple + span + .input-group-addon")
        .css('width','20% !important')
        .css('background-color','#eee')
        .css('border-color','#ccc');

    $(".select-multiple + .select2-container--default .select2-selection--multiple")
        .css('border-radius','0px');
});