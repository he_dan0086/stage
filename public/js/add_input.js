$(document).ready(function(){
    $('#add-input').click(function(){

        $(".input-to-add:last").find('.form-control').attr('required','required')
            .prev('.input-group-addon').addClass('required');
        var c = $(".input-to-add:last").clone();

        $(c).find('.form-control').each(function(){

            var name = $(this).attr('name'),
                value = name.split('['),
                value_next = value[1].split(']');
            name = value[0]+'['+(parseInt(value_next[0])+1)+']';

            $(this).attr('name',name).val('');
        });
        $(".input-to-add:last").parent().append('<br/>').append(c);
    });

    $('.add-stage').live('click',(function(){
        var c = $(".panel-stage-to-add:last").clone();
        $(".add-stage").hide();
        $('.text-danger').hide();
        c.find(".select2").remove();
        c.find('label').removeClass('active');

        $(c).find('.select-multiple').each(function(){
            var name = $(this).attr('name'),
                value = name.split('['),
                value_next = value[0].split('_');
            name = value_next[0]+'_'+(parseInt(value_next[1])+1)+'[]';
            $(this).attr('name',name).val('');

            $(this).select2({
                'width':'100%' ,
                tags: true
            });
        });

        $(c).find('.checkbox').each(function(){
            var name = $(this).attr('name'),
                value = name.split('['),
                value_next = value[0].split('_');
            name = value_next[0]+'_'+(parseInt(value_next[1])+1)+'[]';
            $(this).attr('name',name);
        });

        $(c).find('.form-control').each(function(){
            var name = $(this).attr('name'),
                value = name.split('['),
                value_next = value[1].split(']');
            name = value[0]+'['+(parseInt(value_next[0])+1)+']';
            $(this).attr('name',name).val('');
        });

        $(".panel-stage-to-add:last").parent().append('<br/>').append(c);
    }));
});
