app.factory('Feedbacks',function($http, $q) {
    var factory = {
        feedb : false,

        getFeedbacks : function(){
            var deferred = $q.defer();

            $http.get(window.location.href.replace('#',''))
                .success(function(data, status) {
                    factory.feedb = data;
                    deferred.resolve(factory.feedb);
                }).error(function(data, status) {
                    deferred.reject('Erreur requete Ajax');
                });
            return deferred.promise;
        }
    };
    return factory;
});