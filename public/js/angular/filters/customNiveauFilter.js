app.filter('customNiveauFilter', function() {
    return function(item) {
        if(item.niveaux == 'undefined') {
            return null;
        }
        return item.niveaux;
    };
});