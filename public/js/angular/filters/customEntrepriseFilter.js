app.filter('customEntrepriseFilter', function() {
    return function(item) {
        if(item.entreprise == 'undefined') {
            return null;
        }
        return item.entreprise;
    };
});