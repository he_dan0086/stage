app.filter('customDepartementFilter', function() {
    return function(item) {
        if(item.departements == 'undefined') {
            return null;
        }
        return item.departements;
    };
});