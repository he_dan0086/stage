app.controller('RechercheFeedbackCtrl', ['$scope','$routeParams','Feedbacks',function($scope, $routeParams, Feedbacks){

    $scope.getFeedbacks = Feedbacks.getFeedbacks().then(function(feedback){

        $scope.feedbacks = feedback.feedbacks;

    }, function(msg){
        alert(msg);
    });

}]);