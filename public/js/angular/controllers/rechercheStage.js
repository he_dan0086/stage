app.controller('RechercheStageCtrl', function($scope, $routeParams, Stages){

    $scope.test = Stages.getStages().then(function(stages){

        $scope.niveaux = stages.niveaux;
        $scope.departements = stages.departements;
        $scope.stages = stages.offres;
        $scope.competences = stages.competences;

    }, function(msg){
        alert(msg);
    });

});