var app = angular.module('EtudiantApp', ['ngRoute', 'angularUtils.directives.dirPagination', 'isteven-multi-select']);

app.config(function($routeProvider){

    $routeProvider
        .when('/recherche-stage',
        {
            templateUrl : 'etudiant/views/angular.etudiant.recherche_offre_stage',
            controller: 'RechercheStageCtrl'
        })
        .when('/feedbacks',
        {
            templateUrl : 'etudiant/views/angular.etudiant.feedbacks',
            controller: 'RechercheFeedbackCtrl'
        })
        .otherwise({redirectTo: '/'});
});